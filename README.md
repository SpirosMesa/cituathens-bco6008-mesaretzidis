# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This project was created in order to fullfil the requirements of the module BCO 6008 - Object Oriented Systems 2. The application,
was left partially incomplete, due to the fact the learning goals, were achieved, before the projects completion.

### Project Learning Outcomes
Serialization, DB connections, Vogella PDF library, JUnit, UML, Javadoc, File I/O, Design Patterns, MySQL scripts(Tables, Triggers, Functions). 

### How do I get set up? ###

Simply import the code into your preferred IDE, or simply build through the console. Please check the "external libraries section".

### External Libraries

In order to complete the project, 3 Java libraries were used. JUnit, for testing, the Weka library for classification, as well as IBM's Watson 
library for psychometric testing.

### Project architecture

The project's architecture, has been deliberately made complex, using an unnecessary number of design patterns. This action was taken in order
to maximize the learning outcomes, at the cost of simplicity and maintainability.

### Who do I talk to? ###

For any inquires you can contact me at spirosmesa@yahoo.gr