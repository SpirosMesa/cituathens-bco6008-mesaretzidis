CREATE TABLE client (
  id     int(10) NOT NULL, 
  fn     varchar(255) NOT NULL, 
  ln     varchar(255) NOT NULL, 
  active bit(1) DEFAULT 1 NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE `values` (
  id                   int(10) NOT NULL, 
  self_transcendenceid int(10) NOT NULL, 
  conservation_id      int(10) NOT NULL, 
  hedonism_id          int(10) NOT NULL, 
  self_enhacement_id   int(10) NOT NULL, 
  open_to_chnage_id    int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_5_agreeableness (
  id                   int(10) NOT NULL AUTO_INCREMENT, 
  percentile           decimal(1, 12) NOT NULL, 
  raw_score            decimal(1, 12) NOT NULL, 
  facet_altruism_id    int(11) NOT NULL, 
  facet_cooperation_id int(10) NOT NULL, 
  facet_modesty_id     int(10) NOT NULL, 
  facet_morality_id    int(10) NOT NULL, 
  facet_sympathy_id    int(10) NOT NULL, 
  facet_trust_id       int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE bit_5_conscientiousness (
  id                            int(11) NOT NULL AUTO_INCREMENT, 
  facet_achievement_striving_id int(11) NOT NULL, 
  facet_cautiousness_id         int(11) NOT NULL, 
  facet_dutifulness_id          int(11) NOT NULL, 
  facet_orderliness_id          int(11) NOT NULL, 
  facet_self_discipline_id      int(11) NOT NULL, 
  facet_self_efficacy_id        int(11) NOT NULL, 
  percentile                    decimal(1, 12) NOT NULL, 
  raw_score                     decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_5_extraversion (
  id                           int(10) NOT NULL AUTO_INCREMENT, 
  facet_excitement_seeeking_id int(10) NOT NULL, 
  facet_activity_level_id      int(11) NOT NULL, 
  facet_assertiveness_id       int(11) NOT NULL, 
  facet_cheerfulness_id        int(11) NOT NULL, 
  facet_friendliness_id        int(11) NOT NULL, 
  facet_gregariousness_id      int(10) NOT NULL, 
  percentile                   decimal(1, 12) NOT NULL, 
  raw_score                    decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_5_emotional_range (
  id                     int(10) NOT NULL AUTO_INCREMENT, 
  percentile             decimal(1, 12) NOT NULL, 
  raw_score              decimal(1, 12) NOT NULL, 
  facet_anger_id         int(10) NOT NULL, 
  facet_immoderation_id  int(10) NOT NULL, 
  facet_anxiety_id       int(10) NOT NULL, 
  facet_depression_id    int(10) NOT NULL, 
  facet_vulnerability_id int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_5_openness (
  id                           int(10) NOT NULL AUTO_INCREMENT, 
  facet_adventurousness_id     int(11) NOT NULL, 
  facet_articstic_interests_id int(11) NOT NULL, 
  facet_emotionality_id        int(11) NOT NULL, 
  facet_imagination_id         int(11) NOT NULL, 
  facet_intellect_id           int(11) NOT NULL, 
  facet_liberalism_id          int(11) NOT NULL, 
  percentile                   decimal(1, 12) NOT NULL, 
  raw_score                    decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_adventurousness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_articstic_interests (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_emotionality (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_imagination (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_intellect (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_liberalism (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_achievement_striving (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE Entity (
  );
CREATE TABLE facet_cautiousness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_dutifulness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_orderliness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_self_discipline (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_self_efficacy (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_activity_level (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  real NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_assertiveness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_cheerfulness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_excitement_seeking (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_friendliness (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  percentile decimal(1, 12) NOT NULL, 
  raw_score  decimal(1, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE facet_gregariousness (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile real NOT NULL, 
  raw_score  real NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_5_conscientiousness (
  id int(11) NOT NULL AUTO_INCREMENT, 
  PRIMARY KEY (id));
CREATE TABLE client_texts (
  id                int(10) NOT NULL, 
  client_id         int(10) NOT NULL, 
  watson_resutls_id int(10), 
  weka_results_id   int(10), 
  PRIMARY KEY (id));
CREATE TABLE watson_resutls (
  id          int(10) NOT NULL, 
  tree        int(10) NOT NULL, 
  big_five_id int(10) NOT NULL UNIQUE, 
  needs_id    int(10) NOT NULL, 
  values_id   int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE weka_results (
  id   int(10) NOT NULL, 
  tree varchar(255) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE big_five (
  id                   int(10) NOT NULL, 
  agreeableness_id     int(10) NOT NULL UNIQUE, 
  conscientiousness_id int(10) NOT NULL UNIQUE, 
  extraversion_id      int(10) NOT NULL UNIQUE, 
  emotional_range_id   int(10) NOT NULL UNIQUE, 
  opennessid           int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE agreeableness (
  id             int(10) NOT NULL, 
  percentile     decimal(13, 12) NOT NULL, 
  raw_score      decimal(13, 12) NOT NULL, 
  altruismid     int(10) NOT NULL UNIQUE, 
  cooperation_id int(10) NOT NULL, 
  modestyid      int(10) NOT NULL, 
  moralityid     int(10) NOT NULL, 
  sympathy_id    int(10) NOT NULL, 
  trust_id       int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE altruism (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE cooperation (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE modesty (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE morality (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE sympathy (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE trust (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE achievement_striving (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE cautiousness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE conscientiousness (
  id                      int(10) NOT NULL AUTO_INCREMENT, 
  percentile              decimal(13, 12) NOT NULL, 
  `raw_ score`            decimal(13, 12) NOT NULL, 
  self_efficacy_id        int(10) NOT NULL, 
  self_discipline_id      int(10) NOT NULL, 
  orderliness_id          int(10) NOT NULL, 
  dutifulness_id          int(10) NOT NULL, 
  cautiousness_id         int(10) NOT NULL, 
  achievement_striving_id int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE dutifulness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE orderliness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_discipline (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_efficacy (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE extraversion (
  id                    int(10) NOT NULL AUTO_INCREMENT, 
  percentile            decimal(13, 12) NOT NULL, 
  raw_score             decimal(13, 12) NOT NULL, 
  activity_level_id     int(10) NOT NULL, 
  asssertiveness_id     int(10) NOT NULL, 
  cheerfulness_id       int(10) NOT NULL, 
  excitement_seeking_id int(10) NOT NULL, 
  friendliness_id       int(10) NOT NULL, 
  gregariousness_id     int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE activity_level (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE asssertiveness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE cheerfulness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE excitement_seeking (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE friendliness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE gregariousness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE emotional_range (
  id                    int(10) NOT NULL, 
  percentile            decimal(13, 12) NOT NULL, 
  raw_score             decimal(13, 12) NOT NULL, 
  anger_id              int(10) NOT NULL, 
  anxiety_id            int(10) NOT NULL, 
  depression_id         int(10) NOT NULL, 
  self_consciousness_id int(10) NOT NULL, 
  vulnerability_id      int(10) NOT NULL, 
  immoderation_id       int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE anger (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE anxiety (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE depression (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE immoderation (
  id         int(10) NOT NULL, 
  percenitle decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_consciousness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE vulnerability (
  id         int(10) NOT NULL, 
  percenitle decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE imagination (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE adventurousness (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE artistic_interests (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE emotionality (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE intelect (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile int(10) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE liberalism (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE openness (
  id                   int(10) NOT NULL AUTO_INCREMENT, 
  percentile           decimal(13, 12) NOT NULL, 
  raw_score            decimal(13, 12) NOT NULL, 
  adventurousnessid    int(10) NOT NULL, 
  artistic_interestsid int(10) NOT NULL, 
  emotionalityid       int(10) NOT NULL, 
  imagination_id       int(10) NOT NULL, 
  intelectid           int(10) NOT NULL, 
  liberalismid         int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE needs (
  id                 int(10) NOT NULL, 
  excitement_id      int(10) NOT NULL, 
  harmony_id         int(10) NOT NULL, 
  curiosity_id       int(10) NOT NULL, 
  liberty_id         int(10) NOT NULL, 
  ideal_id           int(10) NOT NULL, 
  closeness_id       int(10) NOT NULL, 
  self_expression_id int(10) NOT NULL, 
  love_id            int(10) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE excitement (
  id         int(10) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE harmony (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE curiosity (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE ideal (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE closeness (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_expression (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE liberty (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE love (
  id         int(10) NOT NULL AUTO_INCREMENT, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_transcendence (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE conservation (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE hedonism (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE self_enhacement (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE open_to_chnage (
  id         int(10) NOT NULL, 
  percentile decimal(13, 12) NOT NULL, 
  raw_score  decimal(13, 12) NOT NULL, 
  PRIMARY KEY (id));
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons420862 (facet_achievement_striving_id), ADD CONSTRAINT FKbit_5_cons420862 FOREIGN KEY (facet_achievement_striving_id) REFERENCES facet_achievement_striving (id);
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons372943 (facet_cautiousness_id), ADD CONSTRAINT FKbit_5_cons372943 FOREIGN KEY (facet_cautiousness_id) REFERENCES facet_cautiousness (id);
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons496706 (facet_orderliness_id), ADD CONSTRAINT FKbit_5_cons496706 FOREIGN KEY (facet_orderliness_id) REFERENCES facet_orderliness (id);
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons999910 (facet_self_discipline_id), ADD CONSTRAINT FKbit_5_cons999910 FOREIGN KEY (facet_self_discipline_id) REFERENCES facet_self_discipline (id);
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons629108 (facet_self_efficacy_id), ADD CONSTRAINT FKbit_5_cons629108 FOREIGN KEY (facet_self_efficacy_id) REFERENCES facet_self_efficacy (id);
ALTER TABLE bit_5_conscientiousness ADD INDEX FKbit_5_cons572314 (facet_dutifulness_id), ADD CONSTRAINT FKbit_5_cons572314 FOREIGN KEY (facet_dutifulness_id) REFERENCES facet_dutifulness (id);
ALTER TABLE client_texts ADD INDEX FKclient_tex272337 (client_id), ADD CONSTRAINT FKclient_tex272337 FOREIGN KEY (client_id) REFERENCES client (id);
ALTER TABLE client_texts ADD INDEX FKclient_tex28101 (watson_resutls_id), ADD CONSTRAINT FKclient_tex28101 FOREIGN KEY (watson_resutls_id) REFERENCES watson_resutls (id);
ALTER TABLE client_texts ADD INDEX FKclient_tex315738 (weka_results_id), ADD CONSTRAINT FKclient_tex315738 FOREIGN KEY (weka_results_id) REFERENCES weka_results (id);
ALTER TABLE watson_resutls ADD INDEX FKwatson_res754496 (big_five_id), ADD CONSTRAINT FKwatson_res754496 FOREIGN KEY (big_five_id) REFERENCES big_five (id);
ALTER TABLE big_five ADD INDEX FKbig_five741985 (agreeableness_id), ADD CONSTRAINT FKbig_five741985 FOREIGN KEY (agreeableness_id) REFERENCES agreeableness (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen775448 (altruismid), ADD CONSTRAINT FKagreeablen775448 FOREIGN KEY (altruismid) REFERENCES altruism (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen228494 (cooperation_id), ADD CONSTRAINT FKagreeablen228494 FOREIGN KEY (cooperation_id) REFERENCES cooperation (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen712003 (modestyid), ADD CONSTRAINT FKagreeablen712003 FOREIGN KEY (modestyid) REFERENCES modesty (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen423027 (moralityid), ADD CONSTRAINT FKagreeablen423027 FOREIGN KEY (moralityid) REFERENCES morality (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen225527 (sympathy_id), ADD CONSTRAINT FKagreeablen225527 FOREIGN KEY (sympathy_id) REFERENCES sympathy (id);
ALTER TABLE agreeableness ADD INDEX FKagreeablen283370 (trust_id), ADD CONSTRAINT FKagreeablen283370 FOREIGN KEY (trust_id) REFERENCES trust (id);
ALTER TABLE big_five ADD INDEX FKbig_five918561 (conscientiousness_id), ADD CONSTRAINT FKbig_five918561 FOREIGN KEY (conscientiousness_id) REFERENCES conscientiousness (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti770217 (self_efficacy_id), ADD CONSTRAINT FKconscienti770217 FOREIGN KEY (self_efficacy_id) REFERENCES self_efficacy (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti630010 (self_discipline_id), ADD CONSTRAINT FKconscienti630010 FOREIGN KEY (self_discipline_id) REFERENCES self_discipline (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti205937 (orderliness_id), ADD CONSTRAINT FKconscienti205937 FOREIGN KEY (orderliness_id) REFERENCES orderliness (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti841261 (dutifulness_id), ADD CONSTRAINT FKconscienti841261 FOREIGN KEY (dutifulness_id) REFERENCES dutifulness (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti324581 (cautiousness_id), ADD CONSTRAINT FKconscienti324581 FOREIGN KEY (cautiousness_id) REFERENCES cautiousness (id);
ALTER TABLE conscientiousness ADD INDEX FKconscienti902133 (achievement_striving_id), ADD CONSTRAINT FKconscienti902133 FOREIGN KEY (achievement_striving_id) REFERENCES achievement_striving (id);
ALTER TABLE extraversion ADD INDEX FKextraversi393838 (activity_level_id), ADD CONSTRAINT FKextraversi393838 FOREIGN KEY (activity_level_id) REFERENCES activity_level (id);
ALTER TABLE extraversion ADD INDEX FKextraversi589203 (asssertiveness_id), ADD CONSTRAINT FKextraversi589203 FOREIGN KEY (asssertiveness_id) REFERENCES asssertiveness (id);
ALTER TABLE extraversion ADD INDEX FKextraversi775863 (cheerfulness_id), ADD CONSTRAINT FKextraversi775863 FOREIGN KEY (cheerfulness_id) REFERENCES cheerfulness (id);
ALTER TABLE extraversion ADD INDEX FKextraversi654039 (excitement_seeking_id), ADD CONSTRAINT FKextraversi654039 FOREIGN KEY (excitement_seeking_id) REFERENCES excitement_seeking (id);
ALTER TABLE extraversion ADD INDEX FKextraversi323917 (gregariousness_id), ADD CONSTRAINT FKextraversi323917 FOREIGN KEY (gregariousness_id) REFERENCES gregariousness (id);
ALTER TABLE extraversion ADD INDEX FKextraversi76127 (friendliness_id), ADD CONSTRAINT FKextraversi76127 FOREIGN KEY (friendliness_id) REFERENCES friendliness (id);
ALTER TABLE big_five ADD INDEX FKbig_five858388 (extraversion_id), ADD CONSTRAINT FKbig_five858388 FOREIGN KEY (extraversion_id) REFERENCES extraversion (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_983007 (anger_id), ADD CONSTRAINT FKemotional_983007 FOREIGN KEY (anger_id) REFERENCES anger (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_430921 (anxiety_id), ADD CONSTRAINT FKemotional_430921 FOREIGN KEY (anxiety_id) REFERENCES anxiety (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_425101 (depression_id), ADD CONSTRAINT FKemotional_425101 FOREIGN KEY (depression_id) REFERENCES depression (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_244304 (self_consciousness_id), ADD CONSTRAINT FKemotional_244304 FOREIGN KEY (self_consciousness_id) REFERENCES self_consciousness (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_779964 (vulnerability_id), ADD CONSTRAINT FKemotional_779964 FOREIGN KEY (vulnerability_id) REFERENCES vulnerability (id);
ALTER TABLE emotional_range ADD INDEX FKemotional_819221 (immoderation_id), ADD CONSTRAINT FKemotional_819221 FOREIGN KEY (immoderation_id) REFERENCES immoderation (id);
ALTER TABLE big_five ADD INDEX FKbig_five151251 (emotional_range_id), ADD CONSTRAINT FKbig_five151251 FOREIGN KEY (emotional_range_id) REFERENCES emotional_range (id);
ALTER TABLE openness ADD INDEX FKopenness211353 (adventurousnessid), ADD CONSTRAINT FKopenness211353 FOREIGN KEY (adventurousnessid) REFERENCES adventurousness (id);
ALTER TABLE openness ADD INDEX FKopenness30931 (artistic_interestsid), ADD CONSTRAINT FKopenness30931 FOREIGN KEY (artistic_interestsid) REFERENCES artistic_interests (id);
ALTER TABLE openness ADD INDEX FKopenness122933 (emotionalityid), ADD CONSTRAINT FKopenness122933 FOREIGN KEY (emotionalityid) REFERENCES emotionality (id);
ALTER TABLE openness ADD INDEX FKopenness8863 (imagination_id), ADD CONSTRAINT FKopenness8863 FOREIGN KEY (imagination_id) REFERENCES imagination (id);
ALTER TABLE openness ADD INDEX FKopenness811217 (intelectid), ADD CONSTRAINT FKopenness811217 FOREIGN KEY (intelectid) REFERENCES intelect (id);
ALTER TABLE openness ADD INDEX FKopenness896634 (liberalismid), ADD CONSTRAINT FKopenness896634 FOREIGN KEY (liberalismid) REFERENCES liberalism (id);
ALTER TABLE big_five ADD INDEX FKbig_five901763 (opennessid), ADD CONSTRAINT FKbig_five901763 FOREIGN KEY (opennessid) REFERENCES openness (id);
ALTER TABLE watson_resutls ADD INDEX FKwatson_res901780 (needs_id), ADD CONSTRAINT FKwatson_res901780 FOREIGN KEY (needs_id) REFERENCES needs (id);
ALTER TABLE needs ADD INDEX FKneeds963089 (excitement_id), ADD CONSTRAINT FKneeds963089 FOREIGN KEY (excitement_id) REFERENCES excitement (id);
ALTER TABLE needs ADD INDEX FKneeds168585 (harmony_id), ADD CONSTRAINT FKneeds168585 FOREIGN KEY (harmony_id) REFERENCES harmony (id);
ALTER TABLE needs ADD INDEX FKneeds552223 (curiosity_id), ADD CONSTRAINT FKneeds552223 FOREIGN KEY (curiosity_id) REFERENCES curiosity (id);
ALTER TABLE needs ADD INDEX FKneeds590094 (liberty_id), ADD CONSTRAINT FKneeds590094 FOREIGN KEY (liberty_id) REFERENCES liberty (id);
ALTER TABLE needs ADD INDEX FKneeds733066 (ideal_id), ADD CONSTRAINT FKneeds733066 FOREIGN KEY (ideal_id) REFERENCES ideal (id);
ALTER TABLE needs ADD INDEX FKneeds327926 (closeness_id), ADD CONSTRAINT FKneeds327926 FOREIGN KEY (closeness_id) REFERENCES closeness (id);
ALTER TABLE needs ADD INDEX FKneeds736617 (self_expression_id), ADD CONSTRAINT FKneeds736617 FOREIGN KEY (self_expression_id) REFERENCES self_expression (id);
ALTER TABLE needs ADD INDEX FKneeds92674 (love_id), ADD CONSTRAINT FKneeds92674 FOREIGN KEY (love_id) REFERENCES love (id);
ALTER TABLE watson_resutls ADD INDEX FKwatson_res988682 (values_id), ADD CONSTRAINT FKwatson_res988682 FOREIGN KEY (values_id) REFERENCES `values` (id);
ALTER TABLE `values` ADD INDEX FKvalues408266 (self_transcendenceid), ADD CONSTRAINT FKvalues408266 FOREIGN KEY (self_transcendenceid) REFERENCES self_transcendence (id);
ALTER TABLE `values` ADD INDEX FKvalues339789 (conservation_id), ADD CONSTRAINT FKvalues339789 FOREIGN KEY (conservation_id) REFERENCES conservation (id);
ALTER TABLE `values` ADD INDEX FKvalues774309 (hedonism_id), ADD CONSTRAINT FKvalues774309 FOREIGN KEY (hedonism_id) REFERENCES hedonism (id);
ALTER TABLE `values` ADD INDEX FKvalues986711 (self_enhacement_id), ADD CONSTRAINT FKvalues986711 FOREIGN KEY (self_enhacement_id) REFERENCES self_enhacement (id);
ALTER TABLE `values` ADD INDEX FKvalues758197 (open_to_chnage_id), ADD CONSTRAINT FKvalues758197 FOREIGN KEY (open_to_chnage_id) REFERENCES open_to_chnage (id);
