import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.lang3.ArrayUtils;

import static java.lang.System.exit;


/**
 * Created by Samael on 29/12/2016.
 */
public class Generator {
    /**
     * Method that is used in order to generate each pseudo-random value.
     * @param bottom The smallest acceptable value.
     * @param top The greatest acceptable value.
     * @return A string value containing the wanted pseudo-random value.
     */
    private String getValue(double bottom, double top) {
        String returnedV = null;
        Double dd = null;
        dd = ThreadLocalRandom.current().nextDouble(bottom, top);

        BigDecimal bd = new BigDecimal(dd);

        String strD = bd.toPlainString();
        try {

            System.out.println(strD.length());
            returnedV = strD.substring(0, 18) + ",";
        }
        catch (StringIndexOutOfBoundsException e) {

            int dif = (18 - strD.length());

            returnedV = strD  + ",";

            //System.out.println(e.toString());
            //System.out.println(dd.toString().length());
            //exit(1);
            //return;
        }
        return returnedV;
    }

    private String addZeros(int i){
        String r = "";
        for (int j = 0; j < i; j++){
            r += "0";
        }
        return  r;
    }

    /**
     * Method used in order to generate each pseudo-random training instance.
     * @return An array containing data for each training instance.
     */
    public String[] generateData() {
        String[] data = ArrayUtils.addAll(generateScientist(), generatePolitician());
        return data;
    }

    /**
     * Method that generates the training instances, classified as "politician".
     * @return A string containing the pseudo-random politician data.
     */
    private String[] generatePolitician() {
        String[] lst = new String[460];
        String politicianData;

        for (int i = 0;  i < 460; i++) {
            //politicianData = " ";
            //Values
            //Self-Transcendence
            politicianData = getValue(0.0, 0.3);
            politicianData += getValue(0.0, 0.4 );
            //Hedonism
            politicianData += (i % 2 == 0) ? getValue(0.0, 0.5)  : getValue(0.8, 1);
            politicianData += getValue(0.0, 0.4 );
            //Open To Change
            politicianData += (i % 2 == 0) ?getValue(0.7, 1)  : getValue(0.0, 0.5);
            politicianData += getValue(0.0, 0.9 );
            //Conservation
            politicianData += getValue(0.3, 0.9 );
            politicianData += getValue(0.0, 0.64 );
            //Self Enhancement
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.45, 1 );

            //Needs
            //Excitement
            politicianData += getValue(0.0, 0.3);
            politicianData += getValue(0.0, 0.33);
            //Harmony
            politicianData += getValue(0.56, 0.78 );
            politicianData += getValue(0.0, 0.63 );
            //Curiosity
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.12 );
            //Liberty
            politicianData += getValue(0.0, 0.47 );
            politicianData += getValue(0.0, 0.34 );
            //Ideal
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.34 );
            //Closeness
            politicianData += getValue(0.0, 0.68 );
            politicianData += getValue(0.32, 0.73 );
            //Self-Expression
            politicianData += getValue(0.0, 0.68 );
            politicianData += getValue(0.0, 0.72 );
            //Love
            politicianData += getValue(0.0, 0.64 );
            politicianData += getValue(0.0, 0.26 );
            //Practicality
            politicianData += getValue(0.0, 0.37);
            politicianData += getValue(0.0, 0.33);
            //Stability
            politicianData += getValue(0.53, 1);
            politicianData += getValue(0.48, 1);
            //Challenge
            politicianData += getValue(0.0, 0.43);
            politicianData += getValue(0.0, 0.44);
            //Structure
            politicianData += getValue(0.51, 1);
            politicianData += getValue(0.53, 1);

            //B5

            //Openness
            politicianData += getValue(0.56, 1 );
            politicianData += getValue(0.0, 0.63 );
            //Adventurousness
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.12 );
            //Artistic Interests
            politicianData += getValue(0.0, 0.12 );
            politicianData += getValue(0.0, 0.38 );
            //Emotionality
            politicianData += getValue(0.0, 0.32 );
            politicianData += getValue(0.0, 0.67 );
            //Imagination
            politicianData += getValue(0.0, 0.12 );
            politicianData += getValue(0.0, 0.68 );
            //Intellect
            politicianData += getValue(0.0, 0.89 );
            politicianData += getValue(0.0, 0.7 );
            //Liberalism
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.31 );

            //Extraversion
            politicianData += getValue(0.52, 1 );
            politicianData += getValue(0.0, 0.67 );
            //Activity Level
            politicianData += getValue(0.0, 0.46 );
            politicianData += getValue(0.0, 0.67 );
            //Assertiveness
            politicianData += getValue(0.0, 0.74 );
            politicianData += getValue(0.0, 0.87 );
            //Cheerfulness
            politicianData += getValue(0.56, 1 );
            politicianData += getValue(0.65, 1 );
            //Excitement Seeking
            politicianData += getValue(0.35, 1 );
            politicianData += getValue(0.38, 1 );
            //Friendliness
            politicianData += getValue(0.5, 1 );
            politicianData += getValue(0.4, 1 );
            //Gregariousness
            politicianData += getValue(0.0, 0.46 );
            politicianData += getValue(0.0, 0.23 );

            //Emotional Range
            politicianData += getValue(0.34, 1 );
            politicianData += getValue(0.0, 0.56 );
            //Anger
            politicianData += getValue(0.0, 0.12 );
            politicianData += getValue(0.0, 0.09 );
            //Anxiety
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.67 );
            //Depression
            politicianData += getValue(0.0, 0.33 );
            politicianData += getValue(0.0, 0.75 );
            //Self - Consciousness
            politicianData += getValue(0.0, 0.22 );
            politicianData += getValue(0.0, 0.21 );
            //Vulnerability
            politicianData += getValue(0.0, 0.34 );
            politicianData += getValue(0.0, 0.26 );
            //Immoderation
            politicianData += getValue(0.0, 0.64 );
            politicianData += getValue(0.0, 0.45 );

            //Conscientiousness
            politicianData += getValue(0.0, 0.34 );
            politicianData += getValue(0.0, 0.34 );
            //Dutifulness
            politicianData += getValue(0.0, 0.47 );
            politicianData += getValue(0.0, 0.26 );
            //Cautiousness
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.56 );
            //Achievement Striving
            politicianData += getValue(0.0, 0.78 );
            politicianData += getValue(0.72, 1 );
            //Self-Efficacy
            politicianData += getValue(0.0, 0.73 );
            politicianData += getValue(0.29, 1 );
            //Self-Discipline
            politicianData += getValue(0.0, 0.64 );
            politicianData += getValue(0.0, 0.58 );
            //Orderliness
            politicianData += getValue(0.0, 0.43 );
            politicianData += getValue(0.0, 0.64 );

            //Agreeableness
            politicianData += getValue(0.51, 1 );
            politicianData += getValue(0.0, 0.74 );
            //Cooperation
            politicianData += getValue(0.0, 0.34 );
            politicianData += getValue(0.0, 0.41 );
            //Morality
            politicianData += getValue(0.0, 0.1 );
            politicianData += getValue(0.0, 0.12 );
            //Trust
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.34 );
            //Altruism
            politicianData += getValue(0.0, 0.12 );
            politicianData += getValue(0.0, 0.23 );
            //Modesty
            politicianData += getValue(0.0, 0.23 );
            politicianData += getValue(0.0, 0.23 );
            //Sympathy
            politicianData += getValue(0.0, 0.45 );
            politicianData += getValue(0.0, 0.76 );

            politicianData += ("politician\n");
            //politicianData.concat("\n");

            lst[i] = politicianData;
        }
        return lst;
    }

    /**
     * Method that generates the training instances, classified as "scientist".
     * @return A string containing the pseudo-random politician data.
     */
    private String[] generateScientist() {
        String[] lst = new String[540];
        String scientistData;

        for (int i = 0;  i < 540; i++) {
            //scientistData = " ";
            //Values
            //Self-Transcendence
            scientistData = getValue(0.0, 0.3);
            scientistData += getValue(0.0, 0.4 );
            //Hedonism
            scientistData += (i % 2 == 0) ? getValue(0.0, 0.5)  : getValue(0.8, 1);
            scientistData += getValue(0.0, 0.4 );
            //Open To Change
            scientistData += (i % 2 == 0) ?getValue(0.7, 1)  : getValue(0.0, 0.5);
            scientistData += getValue(0.0, 0.9 );
            //Conservation
            scientistData += getValue(0.3, 0.9 );
            scientistData += getValue(0.0, 0.64 );
            //Self Enhancement
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.45, 1 );

                //Needs
            //Excitement
            scientistData += getValue(0.0, 0.3);
            scientistData += getValue(0.0, 0.33);
            //Harmony
            scientistData += getValue(0.56, 0.78 );
            scientistData += getValue(0.0, 0.63 );
            //Curiosity
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.12 );
            //Liberty
            scientistData += getValue(0.0, 0.47 );
            scientistData += getValue(0.0, 0.34 );
            //Ideal
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.34 );
            //Closeness
            scientistData += getValue(0.0, 0.68 );
            scientistData += getValue(0.32, 0.73 );
            //Self-Expression
            scientistData += getValue(0.0, 0.68 );
            scientistData += getValue(0.0, 0.72 );
            //Love
            scientistData += getValue(0.0, 0.64 );
            scientistData += getValue(0.0, 0.26 );
            //Practicality
            scientistData += getValue(0.0, 0.37);
            scientistData += getValue(0.0, 0.33);
            //Stability
            scientistData += getValue(0.53, 1);
            scientistData += getValue(0.48, 1);
            //Challenge
            scientistData += getValue(0.0, 0.43);
            scientistData += getValue(0.0, 0.44);
            //Structure
            scientistData += getValue(0.51, 1);
            scientistData += getValue(0.53, 1);

                //B5

            //Openness
            scientistData += getValue(0.56, 1 );
            scientistData += getValue(0.0, 0.63 );
            //Adventurousness
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.12 );
            //Artistic Interests
            scientistData += getValue(0.0, 0.12 );
            scientistData += getValue(0.0, 0.38 );
            //Emotionality
            scientistData += getValue(0.0, 0.32 );
            scientistData += getValue(0.0, 0.67 );
            //Imagination
            scientistData += getValue(0.0, 0.12 );
            scientistData += getValue(0.0, 0.68 );
            //Intellect
            scientistData += getValue(0.0, 0.89 );
            scientistData += getValue(0.0, 0.7 );
            //Liberalism
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.31 );

            //Extraversion
            scientistData += getValue(0.52, 1 );
            scientistData += getValue(0.0, 0.67 );
            //Activity Level
            scientistData += getValue(0.0, 0.46 );
            scientistData += getValue(0.0, 0.67 );
            //Assertiveness
            scientistData += getValue(0.0, 0.74 );
            scientistData += getValue(0.0, 0.87 );
            //Cheerfulness
            scientistData += getValue(0.56, 1 );
            scientistData += getValue(0.65, 1 );
            //Excitement Seeking
            scientistData += getValue(0.35, 1 );
            scientistData += getValue(0.38, 1 );
            //Friendliness
            scientistData += getValue(0.5, 1 );
            scientistData += getValue(0.4, 1 );
            //Gregariousness
            scientistData += getValue(0.0, 0.46 );
            scientistData += getValue(0.0, 0.23 );

            //Emotional Range
            scientistData += getValue(0.34, 1 );
            scientistData += getValue(0.0, 0.56 );
            //Anger
            scientistData += getValue(0.0, 0.12 );
            scientistData += getValue(0.0, 0.09 );
            //Anxiety
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.67 );
            //Depression
            scientistData += getValue(0.0, 0.33 );
            scientistData += getValue(0.0, 0.75 );
            //Self - Consciousness
            scientistData += getValue(0.0, 0.22 );
            scientistData += getValue(0.0, 0.21 );
            //Vulnerability
            scientistData += getValue(0.0, 0.34 );
            scientistData += getValue(0.0, 0.26 );
            //Immoderation
            scientistData += getValue(0.0, 0.64 );
            scientistData += getValue(0.0, 0.45 );

            //Conscientiousness
            scientistData += getValue(0.0, 0.34 );
            scientistData += getValue(0.0, 0.34 );
            //Dutifulness
            scientistData += getValue(0.0, 0.47 );
            scientistData += getValue(0.0, 0.26 );
            //Cautiousness
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.56 );
            //Achievement Striving
            scientistData += getValue(0.0, 0.78 );
            scientistData += getValue(0.72, 1 );
            //Self-Efficacy
            scientistData += getValue(0.0, 0.73 );
            scientistData += getValue(0.29, 1 );
            //Self-Discipline
            scientistData += getValue(0.0, 0.64 );
            scientistData += getValue(0.0, 0.58 );
            //Orderliness
            scientistData += getValue(0.0, 0.43 );
            scientistData += getValue(0.0, 0.64 );

            //Agreeableness
            scientistData += getValue(0.51, 1 );
            scientistData += getValue(0.0, 0.74 );
            //Cooperation
            scientistData += getValue(0.0, 0.34 );
            scientistData += getValue(0.0, 0.41 );
            //Morality
            scientistData += getValue(0.0, 0.1 );
            scientistData += getValue(0.0, 0.12 );
            //Trust
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.34 );
            //Altruism
            scientistData += getValue(0.0, 0.12 );
            scientistData += getValue(0.0, 0.23 );
            //Modesty
            scientistData += getValue(0.0, 0.23 );
            scientistData += getValue(0.0, 0.23 );
            //Sympathy
            scientistData += getValue(0.0, 0.45 );
            scientistData += getValue(0.0, 0.76 );

            scientistData += ("scientist\n");
            //scientistData.concat("\n");

            lst[i] = scientistData;
        }
        return lst;
    }

   
}
