import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.Scanner;

/**
 * Class used in order to generate pseudo-random data, to be used by Weka,
 * in order to create a trained model.
 * Created by S. Mesaretzidis on 29/12/2016.
 */
public class ARFF {
    public static void main(String args[]) {
        //Getting file path.
        System.out.println("please type file path.");
        Scanner scan = new Scanner(System.in);
        String filePath = scan.next();
        scan.close();

        //Getting pseudo-random data.
        Generator gen = new Generator();
        String[] data = gen.generateData();
        //String[] str = gen.generateScientist();
        //System.out.println(StringUtils.countMatches(str[1], ","));
       System.out.println("data length" + data.length + "\n");

        FileWriter writer = null;

        //Writing to file.
        try {
            writer = new FileWriter(filePath, true);
            writer.flush();

            for (int i = 0; i < data.length; i++) {
                writer.write(data[i]);
                //writer.write(System.lineSeparator());
                writer.flush();
                System.out.println(data[i]);
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print("All done");
    }
}
