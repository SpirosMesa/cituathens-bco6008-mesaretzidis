package com.mesaretzidis.spiros.adminPanels.dropUser.controller;

import com.mesaretzidis.spiros.model.appConfig.AppConfig;

/**
 * Controller class for the "AddUser" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class DropUserController {

    /**
     * Method used in order to instantiate the window.
     *
     * @param isAdmin A boolean variable used in order to indicate whether the user is an administrator.
     * @param appConfig An object of the class AppConfig, that contains information regarding the user's username,
     *                  password, as well as the connection string.
     */
    public void showWindow(boolean isAdmin, AppConfig appConfig){

    }
}
