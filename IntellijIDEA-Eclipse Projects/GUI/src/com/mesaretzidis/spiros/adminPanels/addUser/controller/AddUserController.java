package com.mesaretzidis.spiros.adminPanels.addUser.controller;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.errorWindow.controller.ErrorWindowController;
import com.mesaretzidis.spiros.model.appConfig.AppConfig;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

/**
 * Controller class for the "AddUser" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class AddUserController {
    private Parent root = null;
    private AAdminTransactions aAdminTransactions;
    private ACommonUserTransactions aCommonUserTransactions;

    /**
     * Method used in order to instantiate the window.
     *
     * @param isAdmin A boolean variable used in order to indicate whether the user is an administrator.
     * @param appConfig An object of the class AppConfig, that contains information regarding the user's username,
     *                  password, as well as the connection string.
     */
    public void showWindow(boolean isAdmin, AppConfig appConfig){
        ErrorWindowController errorWindowController = new ErrorWindowController();

        try {
            root = FXMLLoader.load(getClass().getResource("..\\fxml\\AddUser.fxml"));
        } catch (IOException e) {
            errorWindowController.showWindow("An error occurred while trying to open the window.\n" +
                    "Please contact a technical advisor.");
            e.printStackTrace();
        }
    }

    public void back() {
    }


    /**
     * Method that is called once the user has pressed the "Exit" button.
     * The method is used in order to exit the application.
     */
    public void exit(){Platform.exit();}
}
