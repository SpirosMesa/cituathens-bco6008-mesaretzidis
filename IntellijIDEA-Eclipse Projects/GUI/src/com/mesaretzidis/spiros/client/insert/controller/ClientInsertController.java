package com.mesaretzidis.spiros.client.insert.controller;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions.AdminTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.errorWindow.controller.ErrorWindowController;
import com.mesaretzidis.spiros.mainScreen.controller.MainScreenController;
import com.mesaretzidis.spiros.model.appConfig.AppConfig;
import com.mesaretzidis.spiros.model.retrieve.RetrieveTextArea;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Controller class of the "InsertClient" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class ClientInsertController {
    /*All the variables where turned into static, for
      1. Performance
      2. To get through a bug.
    */
    private static boolean isAdmin;
    private static AppConfig appConfig = null;
    private static ErrorWindowController errorWindowController;
    private static AAdminTransactions aAdminTransactions;
    private static ACommonUserTransactions aCommonUserTransactions;
    private static Parent root;
    private static TextField fn, ln;
    private static Stage stage;


    /**
     * Method used in order to instantiate the window.
     *
     * @param isAdmin A boolean variable used in order to indicate whether the user is an administrator.
     * @param appConfig An object of the class AppConfig, that contains information regarding the user's username,
     *                  password, as well as the connection string.
     */
    public void showWindow(boolean isAdmin, AppConfig appConfig){
        this.isAdmin = isAdmin;
        System.out.println("isAdmin: " + isAdmin);
        this.appConfig = appConfig;
        System.out.println("insert client showWindow got executed.");
        this.errorWindowController = new ErrorWindowController();

        if (isAdmin) aAdminTransactions = new AdminTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());
        else aCommonUserTransactions = new CommonUserTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());

        try {
            this.root = FXMLLoader.load(getClass().getResource("..\\fxml\\InsertClient.fxml"));
            System.out.println("InsertClientController root: " +root);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);

        /* Getting the TextField controls. The are retrieved in this part of the application, in order to minimize the overhead that would be introduced, when
        *  if they were retrieved each time, the "Insert Client" button was pressed.
        */
        //-----------------------------------------------------------------------------------------

        this.fn = RetrieveTextArea.retrieveInsertClientFNTextArea(root);
        this.ln = RetrieveTextArea.retrieveInsertClientLNTextArea(root);

        stage.show();
    }

    /**
     * Method used in order to exit the application.
     */
    @FXML
    public void exit() {
        Platform.exit();
    }

    /**
     * Method used in order to insert a client.
     */
    @FXML
    public void insertClient(){
        //Getting First and Last name.

        /*TextField fn, ln;

        fn = RetrieveTextArea.retrieveInsertClientFNTextArea(root);
        ln = RetrieveTextArea.retrieveInsertClientLNTextArea(root);
        */

        if (isAdmin) {
            try {
                aAdminTransactions.addNewClient(fn.getText(), ln.getText());
            } catch (SQLException e) {
                errorWindowController.showWindow(e.getMessage());
                e.printStackTrace();
            }
        }

        this.fn.setText("");
        this.ln.setText("");
    }

    /**
     * Method used in order to show the application's main screen.
     */
    @FXML
    public void back() {
        MainScreenController mainScreenController = new MainScreenController();
        mainScreenController.show(this.isAdmin, this.appConfig);
        this.stage.close();
    }
}
