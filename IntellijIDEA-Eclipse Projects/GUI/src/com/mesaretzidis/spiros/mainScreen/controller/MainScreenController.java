package com.mesaretzidis.spiros.mainScreen.controller;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions.AdminTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.client.getById.controller.ClientGetByIdController;
import com.mesaretzidis.spiros.client.insert.controller.ClientInsertController;
import com.mesaretzidis.spiros.errorWindow.controller.ErrorWindowController;
import com.mesaretzidis.spiros.model.appConfig.AppConfig;
import com.mesaretzidis.spiros.watsonResults.controller.WatsonResultsController;
import com.mesaretzidis.spiros.weka.generateDecisionTree.controller.WekaGenerateDecisionTreeController;
import com.mesaretzidis.spiros.weka.getClientTree.controller.WekaGetClientTreeController;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller class of the "MainScreen" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class MainScreenController {
    @FXML
    private Button insertClientButton, genWekaTreeButton, analyzeClientTextButton,
            getClientInformationByIdButton, getClientsWekaTreeButton, getClientsWatsonResutlsButton;
    private static boolean isAdmin;
    private static AppConfig appConfig;
    private ErrorWindowController errorWindowController;
    private AAdminTransactions aAdminTransactions;
    private ACommonUserTransactions aCommonUserTransactions;
    private static Stage stage;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param isAdmin Variable indicating whether the user is an administrator.
     * @param appConfig An AppConfig object, containing various configuration options/information.
     */
    public void show(boolean isAdmin, AppConfig appConfig) {
        this.isAdmin = isAdmin;
        System.out.println("main window isAdmin: " + isAdmin);
        System.out.println("show.appConfig: " + appConfig.toString());
        this.appConfig = appConfig;
        System.out.println("this.appConfig: " + this.appConfig.toString());
        System.out.println("show got executed");
        Parent root = null;
        this.errorWindowController = new ErrorWindowController();
        if (isAdmin) aAdminTransactions =
                new AdminTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());
        else aCommonUserTransactions = new CommonUserTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());

        try {
            root = FXMLLoader.load(getClass().getResource("..\\fxml\\MainScreen.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Method assigned to the "Insert Client" button of the View. It opens
     * the "Insert Client" scene.
     * Called when an onAction Event is thrown.
     */
    @FXML
    public void insertClient(){
        ClientInsertController clientInsertController = new ClientInsertController();
        System.out.println("main window, insert Client isAdmin: " + this.isAdmin);
        System.out.println("main window insert Client appConfig: " + this.appConfig.toString());
        clientInsertController.showWindow(this.isAdmin, this.appConfig);
        this.stage.close();
    }

    /**
     * Method assigned to the "Generate Weka Decision Tree" button of the View It opens
     * the "Generate Weka Decision Tree" scene.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void generateWekaTree() {
        WekaGenerateDecisionTreeController wekaGenerateDecisionTreeController = new WekaGenerateDecisionTreeController();
        wekaGenerateDecisionTreeController.showWindow(isAdmin, appConfig);
    }

    /**
     * Method assigned to the "Analyze Client Text" button of the View. It opens the
     * "Analyze Client Text" scene.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void analyzeClientText() {
        WatsonResultsController watsonResultsController = new WatsonResultsController();
        watsonResultsController.showWindow(isAdmin, appConfig);
    }

    /**
     * Method assigned to the "Get Client Information By Id" button of the View. It opens the
     * "Get Client Information By Id" scene.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void getGetClientInformationById() {
        ClientGetByIdController clientGetByIdController = new ClientGetByIdController();
        clientGetByIdController.showWindow(isAdmin, appConfig);
    }

    /**
     * Method assigned to the "Get Client's Weka Tree" button of the View. It opens the
     * "Get Client's Weka Tree" scene.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void getGetClientsWekaTree() {
        WekaGetClientTreeController wekaGetClientTreeController = new WekaGetClientTreeController();
        wekaGetClientTreeController.showWindow(isAdmin, appConfig);
    }

    /**
     * Method assigned to the "Get Client's Watson Results" button of the View. It opens
     * the "Get Client's Watson Results" scene.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void getGetClientsWatsonResutls() {
        WatsonResultsController watsonResultsController = new WatsonResultsController();
        watsonResultsController.showWindow(isAdmin, appConfig);
    }

    /**
     * Method assigned to the "Exit" button of the View. It closes the application.
     * Called when a onMouseClicked Event is thrown.
     */
    @FXML
    public void exit() {
        Platform.exit();
    }
 }
