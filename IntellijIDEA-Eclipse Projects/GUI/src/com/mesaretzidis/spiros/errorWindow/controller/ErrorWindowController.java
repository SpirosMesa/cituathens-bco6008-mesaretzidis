package com.mesaretzidis.spiros.errorWindow.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import com.mesaretzidis.spiros.model.retrieve.RetrieveTextArea;

import java.io.IOException;

/**
 * Controller class of the "ErrorWindow" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class ErrorWindowController {
    @FXML
    private Button ok_btn;
    @FXML
    private Label message_label;
    @FXML
    private TextArea messageTextArea;

    /**
     * Method that instantiates the window.
     * @param label The label that is going to be portrayed at the window.
     */
    public void showWindow(String label) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("..\\fxml\\ErrorWindow.fxml"));
        } catch (IOException e) {
            System.out.println("IO exception");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        messageTextArea = RetrieveTextArea.retrieveErrorWindowTextArea(root);
        messageTextArea.setText(label);

        Stage stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("An error occurred");
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    /**
     * Method called once the "Ok" button is pressed.
     */
    public void OkButton() {
        Stage stage = (Stage) ok_btn.getScene().getWindow();
        stage.close();
    }
}
