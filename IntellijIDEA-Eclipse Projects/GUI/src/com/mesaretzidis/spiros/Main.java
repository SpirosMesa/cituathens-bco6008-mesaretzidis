package com.mesaretzidis.spiros;

import com.mesaretzidis.spiros.logInScreen.controller.LogInScreenController;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Main extends Application {
    @FXML
    private Button log_in_button;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("logInScreen\\fxml\\LogInScreen.fxml"));
        primaryStage.setTitle("Hello World");

        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        LogInScreenController logInScreenController = new LogInScreenController();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
