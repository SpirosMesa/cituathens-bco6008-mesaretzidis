package com.mesaretzidis.spiros.model.retrieve;

import com.mesaretzidis.spiros.DataStructures.Implementations.Pair;
import com.mesaretzidis.spiros.DataStructures.Interfaces.IPair;
import javafx.scene.control.TextField;

/**
 * Public class used in order to retrieve the credentials entered by the application com.mesaretzidis.spiros.client.
 *
 * @author S. Mesaretzidis
 * @since 10/1/2017.
 */
public class RetrieveCredentials {
    /**
     * Class used to retrieve the username as well as the password that was provided by the application com.mesaretzidis.spiros.client.
     *
     * @param usernameTextField A TextField object containing the entered username.
     * @param passwordTextField A TextField object containing the entered password.
     * @return An object that implements the interface IPair.
     */
    public static IPair<String, String> retrieveCredentials(TextField usernameTextField, TextField passwordTextField) {
        String username, password;

        username = usernameTextField.getCharacters().toString().replace(" ", "");
        password = passwordTextField.getCharacters().toString().replace(" ", "");

        IPair pair = new Pair<String, String>(username, password);
        return pair;
    }
}
