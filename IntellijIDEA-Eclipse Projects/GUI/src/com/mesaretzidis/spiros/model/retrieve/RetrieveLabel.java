package com.mesaretzidis.spiros.model.retrieve;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

/**
 * Public class used in order to reduce all the amount of code behind the View.
 *
 * @author S. Mesaretzidis
 * @since 10/1/2017.
 */
public class RetrieveLabel {

    /**
     * Method used in order to retrieve a label contained within a Parent node. It should be stated,
     * that this code may be subjected to modifications, depending on the parent node of the Label.
     *
     * @param root The Parent object that contains the needed label.
     * @return The required label.
     */
    public static Label getErrorWindowLabel(Parent root) {
        ObservableList<Node> observableList = root.getChildrenUnmodifiable();

        GridPane gridPane = (GridPane) observableList.get(0);
        observableList = gridPane.getChildrenUnmodifiable();

        return (Label)observableList.get(0);
    }
}
