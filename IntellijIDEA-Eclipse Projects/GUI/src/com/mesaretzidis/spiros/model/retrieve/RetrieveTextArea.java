package com.mesaretzidis.spiros.model.retrieve;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;


/**
 * Class used to retrieve a TextArea object from a parent object.
 *
 * @author S. Mesaretzidis
 * @since 10/1/2017.
 */
public class RetrieveTextArea {
    /**
     * <p>This method is used in order to create a TextArea object. The object, is based on a TextArea control, that lies within the ErrorWindow.fxml file.
     * A Parent object is provided and afterwards, its children extracted, and afterwards the corresponding object is created, retrieved, and returned.</p>
     *
     * <p>Due to the fact that it depends heavily on the current layout of the ErrorWindow.fxml file, this method is to be refactored every time there is
     * a change in the layout. Though this method is not the most efficient regarding development time, it ensures, that maximum performance is achieved.
     * This is due to the fact that the exact index of the control is known before hand, and therefore no foreach loop is needed in order to determine
     * which control holds the required index.</p>
     *
     * @param root The parent object.
     * @return A text area, which references the text area of the provided Parent object.
     */
    public static TextArea retrieveErrorWindowTextArea(Parent root) {
        ObservableList<Node> observableList = root.getChildrenUnmodifiable();

        GridPane gridPane = (GridPane) observableList.get(0);

        TextArea textArea = (TextArea) gridPane.getChildrenUnmodifiable().get(1);

        return textArea;
    }

    /**
     * <p>This method is used in order to create a TextArea object. The object, is based on a TextArea control, that lies within the InsertClient.fxml file.
     * A Parent object is provided and afterwards, its children extracted, and afterwards the corresponding object is created, retrieved, and returned.</p>
     *
     * <p>Due to the fact that it depends heavily on the current layout of the InsertClient.fxml file, this method is to be refactored every time there is
     * a change in the layout. Though this method is not the most efficient regarding development time, it ensures, that maximum performance is achieved.
     * This is due to the fact that the exact index of the control is known before hand, and therefore no foreach loop is needed in order to determine
     * which control holds the required index.</p>
     *
     * @param root The parent object.
     * @return A text area, which references the text area of the provided Parent object.
     */
    public static TextField retrieveInsertClientFNTextArea(Parent root) {
        ObservableList<Node> observableList = root.getChildrenUnmodifiable();

        return (TextField) observableList.get(4);
    }

    /**
     * <p>This method is used in order to create a TextArea object. The object, is based on a TextArea control, that lies within the InsertClient.fxml file.
     * A Parent object is provided and afterwards, its children extracted, and afterwards the corresponding object is created, retrieved, and returned.</p>
     *
     * <p>Due to the fact that it depends heavily on the current layout of the InsertClient.fxml file, this method is to be refactored every time there is
     * a change in the layout. Though this method is not the most efficient regarding development time, it ensures, that maximum performance is achieved.
     * This is due to the fact that the exact index of the control is known before hand, and therefore no foreach loop is needed in order to determine
     * which control holds the required index.</p>
     *
     * @param root The parent object.
     * @return A text area, which references the text area of the provided Parent object.
     */
    public static TextField retrieveInsertClientLNTextArea(Parent root) {
        ObservableList<Node> observableList = root.getChildrenUnmodifiable();

        return (TextField) observableList.get(5);
    }
}
