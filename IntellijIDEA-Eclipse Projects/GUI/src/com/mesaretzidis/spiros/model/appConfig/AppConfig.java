package com.mesaretzidis.spiros.model.appConfig;

/**
 * Class used to read or set the various app configurations, like the connection
 * string as well as the path to the logging file.
 *
 * @author S. Mesaretzidis
 * @since 6/1/2017.
 */
public class AppConfig {
    private String connectionString = "jdbc:mysql://localhost:3306/bco_6008_tests?useSSL=false",
    username = "root", password = "root";
    private String logFilePath = "./log/log.txt";

    /**
     * Connection String field setter.
     *
     * @param connectionString The connection string that is to be set.
     */
    public void setConnectionString(String connectionString){if ( (connectionString != null) && !connectionString.isEmpty()) connectionString = connectionString;}

    /**
     * Connection String field getter.
     *
     * @return The set connection string.
     */
    public String getConnectionString() {return connectionString;}

    /**
     * Log file path field setter.
     *
     * @param filePath The file path to be set.
     */
    public void setLogFilePath(String filePath) {if ((filePath != null) && !filePath.isEmpty()) logFilePath = filePath; }

    /**
     * Log file path field getter.
     *
     * @return The set log file path.
     */
    public String getLogFilePath() {return logFilePath;}

    /**
     * Username field getter.
     *
     * @return A String object containing the set username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username field setter.
     *
     * @param username The username to be set.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Password field getter.
     *
     * @return A String object containing the set Password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password field setter.
     *
     * @param password The password to be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
