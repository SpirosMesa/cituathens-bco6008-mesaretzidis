package com.mesaretzidis.spiros.logInScreen.controller;

import com.mesaretzidis.spiros.DataStructures.Interfaces.IPair;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ADatabaseConnection;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.DatabaseConnections.MySqlDatabaseConnectionFactory;
import com.mesaretzidis.spiros.errorWindow.controller.ErrorWindowController;
import com.mesaretzidis.spiros.mainScreen.controller.MainScreenController;
import com.mesaretzidis.spiros.model.appConfig.AppConfig;
import com.mesaretzidis.spiros.model.retrieve.RetrieveCredentials;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Controller class of the "LogInScreen" window.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017
 */
public class LogInScreenController implements Initializable {
    @FXML
    private TextField username_text_field, password_text_field;
    @FXML
    private Button log_in_button;
    private ErrorWindowController errorWindowController;
    private Scene scene;

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        errorWindowController = new ErrorWindowController();
        System.out.println("Error window controller created");
    }

    /**
     * Method that is called when the user clicks on the "LogIn" button.
     */
    public void LogIn() {
        AppConfig appConfig = new AppConfig();

        IPair<String, String> pair = RetrieveCredentials.retrieveCredentials(username_text_field, password_text_field);
        ADatabaseConnection aDatabaseConnection = new MySqlDatabaseConnectionFactory().getObject(pair.getLeft(), pair.getRight(),
                appConfig.getConnectionString());

        boolean isAdmin;

        Stage stage = (Stage) log_in_button.getScene().getWindow();

        try {
           isAdmin = aDatabaseConnection.isAdmin();
            System.out.println("log in isAdmin: " + isAdmin);
        } catch (SQLException e) {
            errorWindowController.showWindow(e.getMessage());
            e.printStackTrace();
            return;
        }

        appConfig.setUsername(pair.getLeft());
        appConfig.setPassword(pair.getRight());

        MainScreenController mainScreenController = new MainScreenController();
        mainScreenController.show(isAdmin, appConfig);

        stage.close();
    }
}
