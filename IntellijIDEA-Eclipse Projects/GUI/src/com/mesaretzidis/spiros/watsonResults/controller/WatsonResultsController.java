package com.mesaretzidis.spiros.watsonResults.controller;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions.AdminTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.errorWindow.controller.ErrorWindowController;
import com.mesaretzidis.spiros.mainScreen.controller.MainScreenController;
import com.mesaretzidis.spiros.model.appConfig.AppConfig;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Controller class of the window "Watson Results".
 *
 * @author S. Mesaretizidis
 * @sinec 8/1/2017.
 */
public class WatsonResultsController {
    /*All the variables where turned into static, for
       1. Performance
       2. To get through a bug.
     */
    private static boolean isAdmin;
    private static AppConfig appConfig = null;
    private static ErrorWindowController errorWindowController;
    private static AAdminTransactions aAdminTransactions;
    private static ACommonUserTransactions aCommonUserTransactions;
    private static Parent root;
    private static Stage stage;


    /**
     * Method used in order to instantiate the window.
     *
     * @param isAdmin A boolean variable used in order to indicate whether the user is an administrator.
     * @param appConfig An object of the class AppConfig, that contains information regarding the user's username,
     *                  password, as well as the connection string.
     */
    public void showWindow(boolean isAdmin, AppConfig appConfig){
        this.isAdmin = isAdmin;
        this.appConfig = appConfig;
        System.out.println("insert client showWindow got executed.");
        Parent root = null;
        this.errorWindowController = new ErrorWindowController();

        if (isAdmin) aAdminTransactions = new AdminTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());
        else aCommonUserTransactions = new CommonUserTransactionsFactory().getObject(appConfig.getUsername(), appConfig.getPassword(), appConfig.getConnectionString());

        try {
            this.root = FXMLLoader.load(getClass().getResource("..\\fxml\\WatsonResults.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.stage = new Stage();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Method used in order to export the results provided by Watson's Personality Insights Analysis.
     */
    @FXML
    public void export() {

    }

    /**
     * Method used in order to exit the application.
     */
    @FXML
    public void exit() {
        Platform.exit();
    }

    /**
     * Method used in order to return to the main screen.
     */
    @FXML
    public void back() {
        MainScreenController mainScreenController = new MainScreenController();
        mainScreenController.show(this.isAdmin, this.appConfig);
    }
}
