package com.mesaretzidis.spiros.DataStructures.Interfaces;

import java.util.List;

/**
 * An interface defining a subset of all of the methods that are contained within
 * Watson's "Trait" class.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/personality_insights/v2/model/Trait.html
 */
public interface ITrait {
    /**
     * Category property getter.
     *
     * @return The trait's category.
     */
    public String getCategory();

    /**
     * Children property getter.
     *
     * @return A List containing all of the available children.
     */
    public List<ITrait> getChildren();

    /**
     * Id property getter.
     *
     * @return The id of the trait.
     */
    public String getId();

    /**
     * Name property getter.
     *
     * @return The name of the trait.
     */
    public String getName();

    /**
     * Percentile property getter.
     *
     * @return The Percentile of the trait.
     */
    public Double getPercentile();

    /**
     * RawScore property getter.
     *
     * @return The raw score of the trait.
     */
    public Double getRawScore();

    /**
     * Category property setter.
     *
     * @param category The category to be set.
     */
    public void setCategory(String category);

    /**
     * Children property setter.
     *
     * @param children The children to be set.
     */
    public void setChildren(List<ITrait> children);

    /**
     * Id property setter.
     *
     * @param id The id to be set.
     */
    public void setId(String id);

    /**
     * Name property setter.
     *
     * @param name The name to be set.
     */
    public void setName(String name);

    /**
     * Percentile property setter.
     *
     * @param Percentile
     */
    public void setPercentile(Double Percentile);

    /**
     * Raw score property setter.
     *
     * @param raw_score The raw score to be set.
     */
    public void setRawScore(Double raw_score);
}
