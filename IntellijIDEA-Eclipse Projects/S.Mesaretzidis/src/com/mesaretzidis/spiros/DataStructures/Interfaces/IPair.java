package com.mesaretzidis.spiros.DataStructures.Interfaces;

/**
 * Interface, used in order to define methods that are to be used by the implementing class.
 * @author S.Mesaretzidis
 * @since 30/12/2016.
 */
public interface IPair<L extends Object, R> {

    //Used to set the pair name.
    public void setPairName(String pairName);

    //Used to get the pair name.
    public String getPairName();

    //User to set the left paired Object.
    public void setLeft(L left);

    //Used to set the right paired Object.
    public void setRight(R right);

    //Used to get the left paired object.
    public L getLeft();

    //Used to get the right paired object.
    public R getRight();
}
