package com.mesaretzidis.spiros.DataStructures.Implementations;

import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;

import java.util.List;

/**
 * A class containing a subset of all of the methods that are contained within
 * Watson's "Trait" class.
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/personality_insights/v2/model/Trait.html
 */
public class Trait implements ITrait {
    private String category = "Category not set", id = null, name = null;
    private List<ITrait> children = null;
    //They are set to 0.0 because the corresponding db row requires value.
    private Double percentile = 0.0, rawScore = 0.0;

    /**
     * Category property getter.
     *
     * @return The trait's category.
     */
    public String getCategory() {return this.category;}

    /**
     * Children property getter.
     *
     * @return A List containing all of the available children.
     */
    public List<ITrait> getChildren() {
        return this.children;
    }

    /**
     * Id property getter.
     *
     * @return The id of the trait.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Name property getter.
     *
     * @return The name of the trait.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Percentile property getter.
     *
     * @return The Percentile of the trait.
     */
    public Double getPercentile() {
        return this.percentile;
    }

    /**
     * RawScore property getter.
     *
     * @return The raw score of the trait.
     */
    public Double getRawScore() {
        return this.rawScore;
    }

    /**
     * Category property setter.
     *
     * @param category The category to be set.
     */
    public void setCategory(String category) { this.category = category; }

    /**
     * Children property setter.
     *
     * @param children The children to be set.
     */
    public void setChildren(List<ITrait> children) {this.children = children;}

    /**
     * Id property setter.
     *
     * @param id The id to be set.
     */
    public void setId(String id) {this.id = id;}

    /**
     * Name property setter.
     *
     * @param name The name to be set.
     */
    public void setName(String name) {this.name = name;}

    /**
     * Percentile property setter.
     *
     * @param percentile
     */
    public void setPercentile(Double percentile) {this.percentile = percentile;}

    /**
     * Raw score property setter.
     *
     * @param raw_score The raw score to be set.
     */
    public void setRawScore(Double raw_score) {this.rawScore = raw_score;}
}
