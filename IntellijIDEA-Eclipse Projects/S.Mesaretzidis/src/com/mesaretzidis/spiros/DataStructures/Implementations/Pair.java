package com.mesaretzidis.spiros.DataStructures.Implementations;

import com.mesaretzidis.spiros.DataStructures.Interfaces.IPair;

/**
 * Class that implements the Pair interface. Used in order to handle a Client-WatsonResults pair, as
 * well as the Client-Weka pair.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public class Pair<L, R> implements IPair<L, R> {
    private L left;
    private R right;
    private String pairName = null;

    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    /**
     * PairName property setter.
     * @param pairName
     */
    public void setPairName(String pairName) {
        this.pairName = pairName;
    }

    /**
     * PairName property getter.
     * @return Returns the value of property PairName.
     */
    public String getPairName() {
        return pairName;
    }

    /**
     * Left property setter.
     * @param left The L object to be set.
     */
   public void setLeft(L left) {
        this.left = left;
    }

    /**
     * Right property setter.
     * @param right The R object instance to be stored.
     */
    public void setRight(R right) {
        this.right = right;
    }

    /**
     * Left property getter.
     * @return The stored L object instance.
     */
    public L getLeft(){
        return this.left;
    }

    /**
     * Right property getter.
     * @return The stored R instance.
     */
    public R getRight() {
        return this.right;
    }
}
