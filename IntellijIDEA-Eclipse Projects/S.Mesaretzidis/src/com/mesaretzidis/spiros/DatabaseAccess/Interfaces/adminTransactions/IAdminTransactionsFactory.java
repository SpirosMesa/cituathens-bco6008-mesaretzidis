package com.mesaretzidis.spiros.DatabaseAccess.Interfaces.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;

/**
 * Interface used to define methods that will be used by the appropriate factory.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public interface IAdminTransactionsFactory {
    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class AAdminTransactions.
     *
     * @param con The connection object that is to be used.
     * @return An object of a concrete class implementation of the abstract class AAdminTransactions.
     * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions
     */

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class AAdminTransactions.
     *
     * @param username The username to be used.
     * @param password The password to be used.
     * @param connectionString The password to be used.
     * @return An object of a concrete class implementation of the abstract class AAdminTransactions.
     * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions
     */
    public AAdminTransactions getObject(String username, String password, String connectionString);
}
