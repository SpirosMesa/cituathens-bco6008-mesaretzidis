package com.mesaretzidis.spiros.DatabaseAccess.Interfaces;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ADatabaseConnection;

/**
 * An interface defining methods used by the appropriate factory class.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
public interface IDatabaseFactory {

    //Method definition, used to return an instance of a concrete class implementation of
    //the abstract class ACommonUserTransactionsProxy.

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class ACommonUserTransactionsProxy.
     *
     * @param username The username that is be used.
     * @param password The password that is to be used.
     * @param connectionString The connection string that is be used.
     * @return An object of a class that extends the abstract class ADatabaseConnection.
     */
    public ADatabaseConnection getObject(String username, String password, String connectionString);
}
