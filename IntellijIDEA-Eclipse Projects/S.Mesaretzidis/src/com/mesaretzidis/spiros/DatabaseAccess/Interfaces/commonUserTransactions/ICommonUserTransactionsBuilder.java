package com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;

/**
 * An interface defining methods used by the appropriate builder class.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
public interface ICommonUserTransactionsBuilder {

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class ACommonUserTransactionsPreProxy.
     *
     * @param username The username to be set.
     * @param password The password to be set.
     * @param connectionString The connection string to be set.
     * @return An object that implements the abstract class ACommonUserTransactions.
     */
    public ACommonUserTransactions getObject(String username, String password, String connectionString);
}
