package com.mesaretzidis.spiros.DatabaseAccess.Interfaces;

import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Abstract class, that holds all the required database transactions.
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public interface IBaseTransactions {

    /**
     * Connection string field getter.
     *
     * @return The set connection string.
     */
    public String getConnectionString();

    /**
     * Connection String field setter.
     *
     * @param connectionString The connection string to be set.
     */
    public void setConnectionString(String connectionString);

    /**
     * Password field getter.
     *
     * @return A string containing the set password.
     */
    public String getPassword();

    /**
     * password field setter.
     *
     * @param password The password to be set.
     */
    public void setPassword(String password);

    /**
     * Username field getter.
     *
     * @return The set password.
     */
    public String getUsername();

    /**
     * Username field setter.
     *
     * @param username The username to be set.
     */
    public void setUsername(String username);

    /**
     * Method used to retrieve all application clients.
     *
     * @return A List implementation used to retrieve all application clients.
     * @see com.mesaretzidis.spiros.Entities.Client
     * @see java.util.List
     */
    public abstract List<Client> getAllClients() throws SQLException;

    /**
     * Method used in order to retrieve a client by his/her id.
     *
     * @param id The client's id.
     * @return A Client object, containing the necessary information.
     * @see com.mesaretzidis.spiros.Entities.Client
     */
    public abstract Client getClientById(int id) throws SQLException;

    /**
     * Method used to retrieve a clients WatsonResults
     *
     * @param clientId The client's id.
     * @return A List implementation containing WatsonResults objects, which in turn contain the client's watson results.
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     * @see java.util.List
     */
    public abstract List<WatsonResults> getClientWatsonResults(int clientId) throws SQLException;

    /**
     * Method used in order to enter a new record in the database.
     *
     * @param client_id the client's id.
     * @param client_text the input text
     * @param wekaResults the weka results tree
     * @param wResults the watson results.
     */
    public abstract int insertRecord(int client_id, String client_text, String wekaResults, WatsonResults wResults) throws SQLException, IOException;

    /**
     * Method used to add a new client
     *
     * @param fn The client's first name.
     * @param ln The client's last name.
     * @return The id of the new client's record.
     */
    public abstract int addNewClient(String fn, String ln) throws SQLException;

    /**
     * Method used in order to get a client's stored texts.
     *
     * @param clientId The client's id.
     * @return A String[] containing the client's texts.
     */
    public abstract String[] getClientTexts(int clientId) throws SQLException;

    /**
     * Method used in order to get a client's stored Weka results tree.
     *
     * @param clientId The client's id.
     * @return A String[] containing the clients Weka results.
     */
    public abstract String[] getWekaResults(int clientId) throws SQLException, IOException;
}
