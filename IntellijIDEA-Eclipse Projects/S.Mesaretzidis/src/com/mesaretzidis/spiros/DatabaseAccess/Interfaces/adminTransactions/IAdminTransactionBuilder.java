package com.mesaretzidis.spiros.DatabaseAccess.Interfaces.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy;

/**
 * An interface defining methods used by the appropriate builder class.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
public interface IAdminTransactionBuilder {

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class AAdminTransactionsPreProxy.
     *
     * @param username The username to be used.
     * @param password The password to be used.
     * @param connectionString The connection string to be used.
     * @return An object of a class that implements the abstract class AAdminTransactionsProxy.
     */
    public AAdminTransactionsPreProxy getAdminTransactions(String username, String password, String connectionString);
}
