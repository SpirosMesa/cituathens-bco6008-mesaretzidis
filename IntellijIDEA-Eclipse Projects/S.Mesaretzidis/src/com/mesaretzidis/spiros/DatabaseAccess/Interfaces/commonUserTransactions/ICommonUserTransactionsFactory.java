package com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;

/**
 * Interface used to define methods that will be used in order to return an object of the appropriate class.
 *
 * @author S. Mesaretzidis
 * @since 9/1/2017.
 */
public interface ICommonUserTransactionsFactory {

    /**
     * Method that will be used in order to get an object of a class that implements ACommonUserTransactions.
     *
     * @param username The username to be set.
     * @param password The password to be set.
     * @param connectionString The connection string to be set.
     * @return An object of a class that implements the abstract class ACommonUserTransactions.
     */
    public ACommonUserTransactions getObject(String username, String password, String connectionString);
}
