package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

import com.mesaretzidis.spiros.Entities.DbUser;

import java.sql.SQLException;
import java.util.List;

/**
 * Abstract class, used to define all the required methods, for the proxy that will later be used.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
public abstract class AAdminTransactions extends AAdminTransactionsPreProxy {
    protected AAdminTransactionsPreProxy aAdminTransactionsPreProxy = null;

    /**
     * Class constructor receiving as input parameter an object of a class that implements ACommonUserTransactions.
     *
     * @param commonUserTransactions An object of a class that extends ACommonUserTransactions.
     */
    private AAdminTransactions(ACommonUserTransactions commonUserTransactions) {
        super(commonUserTransactions);
    }

    /**
     * Class constructor that receives as input an object of a class that implements aAdminTransactionsPreProxy.
     *
     * @param aAdminTransactionsPreProxy An object of a class that implements aAdminTransactionsPreProxy.
     */
    public AAdminTransactions(AAdminTransactionsPreProxy aAdminTransactionsPreProxy) {
        this.aAdminTransactionsPreProxy = aAdminTransactionsPreProxy;
    }

    /**
     * Default class constructor.
     */
    public AAdminTransactions(){}

    /**
     * Method used to remove db user.
     *
     * @param username The username of the user to be removed.
     */
    public abstract void dropDbUser(String username) throws SQLException;

    /**
     * Method used to add db user.
     *
     * @param usr Db user's fn.
     * @param psw Db user's psw.
     */
    public abstract void addUser(String usr, String psw) throws SQLException;

    /**
     * Method used to retrieve all db users.
     *
     * @return A list implementation containing information on all db users.
     */
    public abstract List<DbUser> getAllUsers() throws SQLException;

    public void setaAdminTransactionsPreProxy(AAdminTransactionsPreProxy aAdminTransactionsPreProxy) {
        this.aAdminTransactionsPreProxy = aAdminTransactionsPreProxy;
    }
}
