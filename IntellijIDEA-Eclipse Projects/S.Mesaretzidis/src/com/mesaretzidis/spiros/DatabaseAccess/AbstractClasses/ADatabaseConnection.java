package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

import com.mesaretzidis.spiros.Exceptions.CredentialsNotSetException;
import com.sun.istack.internal.NotNull;

import java.sql.SQLException;

/**
 * Abstract class defining a methods that are used in order to determine whether the user exists.
 *
 * @author S. Mesaretzidis on 31/12/2016.
 */
public abstract class ADatabaseConnection {
    protected String username, password, connectionString;

    /**
     * Constructor that receives as input the user's username, password and a not-default connection string.
     *
     * @param username The username.
     * @param password The password.
     * @param connectionString The not default connection string.
     */
    public ADatabaseConnection(@NotNull String username, @NotNull String password, @NotNull String connectionString)  {
        this.username = username;
        this.password = password;
        this.connectionString = connectionString;
    }

    /**
     * Abstract method used to retrieve a database connection.
     *
     * @return A boolean value indicating if the user has administrator rights.
     * @throws CredentialsNotSetException
     */
    public abstract boolean isAdmin() throws SQLException;

    /**
     * Username property getter.
     *
     * @return The set username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username property setter
     *
     * @param username The username to be set.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Connection string property getter.
     *
     * @return The set connection string.
     */
    public String getConnectionString() {
        return connectionString;
    }

    /**
     * Connection string property setter
     *
     * @param connectionString The username to be set.
     */
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    /**
     * Password property getter.
     *
     * @return The set password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password property setter
     *
     * @param password The username to be set.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
