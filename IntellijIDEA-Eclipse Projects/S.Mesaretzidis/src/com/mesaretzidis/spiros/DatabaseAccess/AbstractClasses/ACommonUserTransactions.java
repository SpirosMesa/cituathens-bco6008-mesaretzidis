package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

/**
 * Abstract class, used in order to define the proxy of the concrete implementation of ACommonUserTransactionsPreProxy.
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
public abstract class ACommonUserTransactions extends ACommonUserTransactionsBase{
    protected ACommonUserTransactionsPreProxy commonUserTransactionsPreProxy;

    /**
     * Default class constructor.
     *
     * @param commonUserTransactionsPreProxy An object of a class that extends ACommonUserTransactionsPreProxy
     */
    public ACommonUserTransactions(ACommonUserTransactionsPreProxy commonUserTransactionsPreProxy) {
        super(commonUserTransactionsPreProxy);
    }

    public ACommonUserTransactions() {};

    /**
     * CommonUserTransactions field setter.
     *
     * @param aCommonUserTransactionsPreProxy
     */
    public void setPreProxyObject(ACommonUserTransactionsPreProxy aCommonUserTransactionsPreProxy){
        commonUserTransactionsPreProxy = aCommonUserTransactionsPreProxy;
    }
}
