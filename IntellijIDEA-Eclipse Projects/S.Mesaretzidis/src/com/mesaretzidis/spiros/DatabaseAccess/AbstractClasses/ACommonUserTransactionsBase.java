package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.IBaseTransactions;

/**
 * Abstract class used in order to hold the constructor that will be used in order to instantiate the class.
 *
 * @author S. Mesaretzidis
 * @since 15/1/2017.
 */
public abstract class ACommonUserTransactionsBase implements IBaseTransactions {
    protected ACommonUserTransactionsPreProxy commonUserTransactionsPreProxy;

    /**
     * Class constructor that receives as argument an object of a class that extends ACommonUserTransactionsProxy.
     *
     * @param commonUserTransactionsPreProxy An object of a class that extends ACommonUserTransactionsPreProxy
     */
    public ACommonUserTransactionsBase(ACommonUserTransactionsPreProxy commonUserTransactionsPreProxy) {
        this.commonUserTransactionsPreProxy = commonUserTransactionsPreProxy;
    }

    /**
     * Default class constructor.
     */
    public ACommonUserTransactionsBase(){};
}
