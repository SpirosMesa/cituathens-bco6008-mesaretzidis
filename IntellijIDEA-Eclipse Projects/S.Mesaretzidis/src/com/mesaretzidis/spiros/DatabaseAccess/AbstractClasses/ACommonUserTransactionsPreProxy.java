package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.IBaseTransactions;

/**
 * Abstract class used to define methods that will be used the concrete implementation of this class.
 *
 * @author S.Mesaretzidis
 * @since 31/12/2016
 */
public abstract class ACommonUserTransactionsPreProxy implements IBaseTransactions{
    protected String username, password, connectionString;

    /**
     * Class constructor accepting all possible arguments.
     *
     * @param username         The username to be set.
     * @param password         The password to be set.
     * @param connectionString The connection string to be set.
     */
    public ACommonUserTransactionsPreProxy(String username, String password, String connectionString) {
        this.username = username;
        this.password = password;
        this.connectionString = connectionString;
    }

    /**
     * Default class constructor.
     */
    public ACommonUserTransactionsPreProxy(){};
}


