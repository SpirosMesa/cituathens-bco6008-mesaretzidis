package com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses;

/**
 * Abstract class used in order to hold the constructor that will be used in order to instantiate the class.
 *
 * @author S. Mesaretzidis
 * @since 15/1/2017.
 */
public class AAdminTransactionsPreProxyBase {
    protected ACommonUserTransactions commonUserTransactions;

    /**
     * Default class constructor
     *
     * @param commonUserTransactions An object of a class that implements ACommonUserTransactions.
     */
    public AAdminTransactionsPreProxyBase(ACommonUserTransactions commonUserTransactions) {
        this.commonUserTransactions = commonUserTransactions;
    }
}
