package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions;

import com.mesaretzidis.spiros.DataStructures.Implementations.Trait;
import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactionsPreProxy;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Concrete class implementation of the abstract class ACommonUserTransactionsPreProxy
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactionsPreProxy
 */
public class CommonUserTransactionsPreProxy extends ACommonUserTransactionsPreProxy {

    /**
     * Default class constructor accepting all possible arguments.
     *
     * @param username         The username to be set.
     * @param password         The password to be set.
     * @param connectionString The connection string to be set.
     */
    public CommonUserTransactionsPreProxy(String username, String password, String connectionString) {
        super(username, password, connectionString);
    }

    /**
     * Connection string field getter.
     *
     * @return The set connection string.
     */
    @Override
    public String getConnectionString() {
        return this.connectionString;
    }

    /**
     * Connection String field setter.
     *
     * @param connectionString
     */
    @Override
    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    /**
     * Password field getter.
     *
     * @return String containing the set password.
     */
    @Override
    public String getPassword() {
        return this.password;
    }

    /**
     * password field setter.
     *
     * @param password The password to be set.
     */
    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Username field getter.
     *
     * @return The set password.
     */
    @Override
    public String getUsername() {
        return this.username;
    }

    /**
     * Username field setter.
     *
     * @param username The username to be set.
     */
    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method used to retrieve all application clients.
     *
     * @return A List implementation used to retrieve all application clients.
     * @see com.mesaretzidis.spiros.Entities.Client
     * @see java.util.List
     */
    @Override
    public List<Client> getAllClients() throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("Select count(*) from app_client");
        rs.next();

        int count = rs.getInt(1);

        rs = stmt.executeQuery("Select * from app_client where active = 1");
        rs.next();

        Client cl = null;
        List<Client> clientLst = new LinkedList<>();
        int id;
        String fn, ln;

        for (int i = 0; i < count; i++){
            id = rs.getInt(1);
            fn = rs.getString(2);
            ln = rs.getString(3);

            cl = new Client(id, fn, ln);

            clientLst.add(cl);
            rs.next();
        }

        rs.close();
        stmt.close();
        connection.close();

        return clientLst;
    }

    /**
     * Method used in order to retrieve a client by his/her id.
     *
     * @param id The client's id.
     * @return A Client object, containing the necessary information.
     * @see com.mesaretzidis.spiros.Entities.Client
     */
    @Override
    public Client getClientById(int id) throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM app_client WHERE id = " + id + " AND active = 1");
        rs.next();
        Client cl = new Client(id, rs.getString(2), rs.getString(3));

        rs.close();
        stmt.close();
        connection.close();

        return cl;
    }

    /**
     * Method used to retrieve a clients WatsonResults
     *
     * @param clientId The client's id.
     * @return A List implementation containing WatsonResults objects, which in turn contain the client's watson results.
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     * @see java.util.List
     */
    @Override
    public List<WatsonResults> getClientWatsonResults(int clientId) throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        PreparedStatement preStmt = connection.prepareStatement("select fk_watson_results_id from client_texts where fk_app_client_id = ?");
        preStmt.setInt(1, clientId);

        ResultSet rs = preStmt.executeQuery( );

        ArrayList<Integer> wResultsIds = new ArrayList<>();
        List<WatsonResults> wrLst = new LinkedList<>();
        WatsonResults ws = null;

        while (rs.next()) {
            ws = getClientSingleWatsonResult(rs.getInt(1));

            wrLst.add(ws);
        }

        rs.close();
        preStmt.close();
        connection.close();
        return  wrLst;
    }

    /**
     * Method used in order to enter a new record in the database.
     *
     * @param client_id   the client's id.
     * @param client_text the input text
     * @param wekaResults the weka results tree
     * @param wResults    the watson results.
     */
    public int insertRecord( int client_id, String client_text, String wekaResults, WatsonResults wResults) throws SQLException, IOException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();

        int weka_results_id, watson_results_id;

        ResultSet rs = null;


        PreparedStatement ps = connection.prepareStatement("INSERT INTO weka_results(tree) values( ? )");
        ps.setString(1, wekaResults);
        ps.executeUpdate();

        rs = stmt.executeQuery("SELECT  last_insert_id() from weka_results");
        rs.next();
        weka_results_id = rs.getInt(1);

        watson_results_id = addNewWatsonResult(wResults);

        stmt.executeUpdate("INSERT  INTO client_texts(fk_app_client_id, fk_watson_results_id, fk_weka_results_id) values(" +
            client_id + "," + watson_results_id + "," + weka_results_id + ")");

        rs = stmt.executeQuery("SELECT  last_insert_id() from client_texts");
        rs.next();

        int i = rs.getInt(1);
        stmt.close();
        rs.close();
        connection.close();
        return i;
    }

    /**
     * Method used to add a new client
     * @param fn The client's first name.
     * @param ln The client's last name.
     * @return The id of the new client's record.
     */
    @Override
    public int addNewClient(String fn, String ln) throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();

        stmt.executeUpdate("insert into app_client(fn, ln, active) VALUES('" + fn +"', '" + ln + "' ,1)");

        ResultSet rs = stmt.executeQuery("select last_insert_id() from app_client");
        rs.next();
        int i = rs.getInt(1);

        stmt.close();
        rs.close();
        connection.close();
        return i;
    }

    /**
     * Method used in order to add a client's watson results.
     *
     * @param watsonResults The results of Watson's Personality Insights Service.
     */
    private int addNewWatsonResult(WatsonResults watsonResults) throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();

        int big_5_id, fk_needs_id, fk_values_id;
        fk_values_id = insert_watson_values(watsonResults.getLstNativeValues());
        fk_needs_id = insert_watson_needs(watsonResults.getLstNativeNeeds());
        big_5_id = insert_watson_b5(watsonResults.getLstB5());

        ResultSet rs;
        stmt.executeUpdate("insert into watson_results(fk_big_five_id, fk_needs_id, fk_values_id) values(" +
        big_5_id + "," + fk_needs_id + "," + fk_values_id + ")");
        rs = stmt.executeQuery("SELECT last_insert_id() from watson_results");
        rs.next();

        int i = rs.getInt(1);

        stmt.close();
        rs.close();
        connection.close();
        return i;
    }

    /**
     * Method used in order to get a client's stored text.
     *
     * @param clientId The client's id.
     * @return A String containing the client's texts.
     */
    public String[] getClientTexts(int clientId) throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        PreparedStatement preStmt = null;
        ResultSet rs = null;

        //Getting number of rows.

        preStmt = connection.prepareStatement("Select client_text from client_texts where fk_app_client_id = ?");

        preStmt.setInt(1, clientId);

        rs = preStmt.executeQuery();

        //Getting number of rows, for array initialization.
        rs.last();
        int arrSize = rs.getRow();
        rs.beforeFirst();

        String[] arr = new String[arrSize];

        //Recycling variables.
        arrSize = 0;

        while(rs.next()) {
            arr[arrSize] = rs.getString(1);
            arrSize++;
        }

        preStmt.close();
        rs.close();
        connection.close();

        return arr;
    }

    /**
     * Method used in order to get a client's stored Weka results tree.
     *
     * @param clientId The client's id.
     * @return A String[] containing the client's weka results.
     */
    public String[] getWekaResults(int clientId) throws SQLException, IOException {
        Connection connection = DriverManager.getConnection(this.connectionString ,this.username, this.password);

        PreparedStatement preStmt = connection.prepareStatement("select tree from weka_results join "
                + "client_texts where " +
                "weka_results.id = client_texts.fk_weka_results_id " +
                "And client_texts.fk_app_client_id = ?");

        preStmt.setInt(1, clientId);

        ResultSet rs = preStmt.executeQuery();

        //Getting row count, used for array size.
        rs.last();
        int arrSize = rs.getRow();
        rs.beforeFirst();

        String[] arr = new String[arrSize];

        //Variable Recycling.
        arrSize = 0;

        while(rs.next()){
            arr[arrSize] = rs.getString(1);
            arrSize++;
        }

        preStmt.close();
        rs.close();
        connection.close();

        return arr;
    }

    /**
     * Method used to insert into
     * @param b5Lst The client's Big 5 list that is to be inserted.
     * @return A integer containing the last inserted record's id.
     * @throws SQLException
     */
    private int insert_watson_b5(List<ITrait> b5Lst) throws SQLException {
        ITrait openTrt = b5Lst.get(0), conTrt = b5Lst.get(1), extraTrt = b5Lst.get(2), agreeTrt = b5Lst.get(3), erTrt = b5Lst.get(4);
        List<ITrait> openCh = openTrt.getChildren(), conCh = conTrt.getChildren(), extroCh = extraTrt.getChildren(), agreeCh = agreeTrt.getChildren(),
                erCh = erTrt.getChildren();

        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();
        ResultSet rs;

        //Openness
        int adventId = 0, aiId = 0, emotId = 0, imaginId = 0, intel_id = 0, liberId = 0 ,opennessId = 0;

        if ( (openCh.size() == 1) || (openCh.size() > 1)) {
        //Adventurousness
        stmt.executeUpdate("Insert into  big_5_openness_adventurousness(percentile, raw_score) values (" + openCh.get(0).getPercentile() + "," + openCh.get(0).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        adventId = rs.getInt(1); }
        else {
            stmt.executeUpdate("Insert into  big_5_openness_adventurousness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            adventId = rs.getInt(1);
        }

        if ( (openCh.size() >= 2)) {
        //Artistic interests
        stmt.executeUpdate("Insert into  big_5_openness_artistic_interests(percentile, raw_score) values (" + openCh.get(1).getPercentile() + "," + openCh.get(1).getRawScore() +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        aiId = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_openness_artistic_interests(percentile, raw_score) values (" + null + "," + null +")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            aiId = rs.getInt(1);
        }

        if ( (openCh.size() == 3)) {
        //Emotionality
        stmt.executeUpdate("Insert into  big_5_openness_emotionality(percentile, raw_score) values (" + openCh.get(2).getPercentile() + "," + openCh.get(2).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        emotId = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_openness_emotionality(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            emotId = rs.getInt(1);
        }

        if ( (openCh.size() >= 4)) {
        //Imagination
        stmt.executeUpdate("Insert into  big_5_openness_imagination(percentile, raw_score) values (" + openCh.get(3).getPercentile() + "," + openCh.get(3).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        imaginId = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_openness_imagination(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            imaginId = rs.getInt(1);
        }

        if ( (openCh.size() >= 5)) {
        //intellect
        stmt.executeUpdate("Insert into  big_5_openness_intellect(percentile, raw_score) values (" + openCh.get(4).getPercentile() + "," + openCh.get(4).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        intel_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_openness_intellect(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            intel_id = rs.getInt(1);
        }

        if ( (openCh.size() == 6)) {
        //liberalism
        stmt.executeUpdate("Insert into  big_5_openness_liberalism(percentile, raw_score) values (" + openCh.get(5).getPercentile() + "," + openCh.get(5).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        liberId = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_openness_liberalism(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            liberId = rs.getInt(1);
        }

        //Getting open Id values
        stmt.executeUpdate("Insert INTO big_5_openness(fk_adventurousness_id,  fk_artistic_interests_id, fk_emotionality_id, fk_imagination_id, fk_intellect_id, fk_liberalism_id)" +
        "values(" + adventId + "," + aiId + ", " + emotId + "," + imaginId + "," + intel_id + "," + liberId +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        opennessId = rs.getInt(1);

        //Extroversion
        int alId = 0, assertId = 0, cheer_id = 0, ex_seek_id = 0, friend_id = 0, grega_id = 0, extroversion_id = 0;

        if ( (openCh.size() >= 1)) {
        //Activity level
        stmt.executeUpdate("Insert into big_5_extraversion_activity_level(percentile, raw_score) values (" + extroCh.get(0).getPercentile() + "," + extroCh.get(0).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        alId = rs.getInt(1);}
        else {
                //Activity level
                stmt.executeUpdate("Insert into big_5_extraversion_activity_level(percentile, raw_score) values (" + null + "," + null + ")");
                rs = stmt.executeQuery("SELECT last_insert_id()");
                rs.next();
                alId = rs.getInt(1);
        }

        if ( (openCh.size() >= 2)) {
        //assertiveness
        stmt.executeUpdate("Insert into  big_5_extraversion_assertiveness(percentile, raw_score) values (" + extroCh.get(1).getPercentile() + "," + extroCh.get(1).getRawScore() +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        assertId = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_extraversion_assertiveness(percentile, raw_score) values (" + null + "," + null +")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            assertId = rs.getInt(1);
        }

        if ( (openCh.size() >= 3)) {
        //cheerfulness
        stmt.executeUpdate("Insert into  big_5_extraversion_cheerfulness(percentile, raw_score) values (" + extroCh.get(2).getPercentile() + "," + extroCh.get(2).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        cheer_id = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_extraversion_cheerfulness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            cheer_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 4)) {
        //excitement seeking
        stmt.executeUpdate("Insert into  big_5_extraversion_excitement_seeking(percentile, raw_score) values (" + extroCh.get(3).getPercentile() + "," + extroCh.get(3).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        ex_seek_id = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_extraversion_excitement_seeking(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            ex_seek_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 5)) {
        //friendliness
        stmt.executeUpdate("Insert into  big_5_extraversion_friendliness(percentile, raw_score) values (" + extroCh.get(4).getPercentile() + "," + extroCh.get(4).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        friend_id = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_extraversion_friendliness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            friend_id = rs.getInt(1);
        }

        if ( (openCh.size() == 6 )) {
               //gregariousness
        stmt.executeUpdate("Insert into  big_5_extraversion_gregariousness(percentile, raw_score) values (" + extroCh.get(5).getPercentile() + "," + extroCh.get(5).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        grega_id = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_extraversion_gregariousness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            grega_id = rs.getInt(1);
        }

        //Getting extro Id values
        stmt.executeUpdate("Insert INTO big_5_extraversion(fk_activity_level_id,  fk_assertiveness_id, fk_cheerfulness_id, fk_excitement_seeking_id, fk_gregariousness_id, fk_friendliness_id)" +
                "values(" + alId + "," + assertId + ", " + cheer_id + "," + ex_seek_id + "," + grega_id + "," + friend_id +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        extroversion_id = rs.getInt(1);

        //EmotionalRange
        int ang_id = 0, anx_id = 0 , dep_id = 0, self_con_id = 0, vuln_id = 0, imo_id = 0, emotional_range_id = 0;

        if ( (openCh.size() >= 1)) {
        //anger
        stmt.executeUpdate("Insert into big_5_emotional_range_anger(percentile, raw_score) values (" + erCh.get(0).getPercentile() + "," + erCh.get(0).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        ang_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_emotional_range_anger(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            ang_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 2)) {
        //anxiety
        stmt.executeUpdate("Insert into  big_5_emotional_range_anxiety(percentile, raw_score) values (" + erCh.get(1).getPercentile() + "," + erCh.get(1).getRawScore() +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        anx_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_emotional_range_anxiety(percentile, raw_score) values (" + null + "," + null +")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            anx_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 3)) {
        //depression
        stmt.executeUpdate("Insert into  big_5_emotional_range_depression(percentile, raw_score) values (" + erCh.get(2).getPercentile() + "," + erCh.get(2).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        dep_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_emotional_range_depression(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            dep_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 4)) {
        //self consciousness
        stmt.executeUpdate("Insert into big_5_emotional_range_self_consciousness(percentile, raw_score) values (" + erCh.get(3).getPercentile() + "," + erCh.get(3).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        self_con_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_emotional_range_self_consciousness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            self_con_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 5)) {
        //vulnerability
        stmt.executeUpdate("Insert into big_5_emotional_range_vulnerability(percentile, raw_score) values (" + erCh.get(4).getPercentile() + "," +erCh.get(4).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        vuln_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_emotional_range_vulnerability(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            vuln_id = rs.getInt(1);
        }

        if ( (openCh.size() == 6)) {
        //immoderation
        stmt.executeUpdate("Insert into  big_5_emotional_range_immoderation(percentile, raw_score) values (" + erCh.get(5).getPercentile() + "," + erCh.get(5).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        imo_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_emotional_range_immoderation(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            imo_id = rs.getInt(1);
        }

        //emotional range insert id.
        stmt.executeUpdate("Insert INTO big_5_emotional_range(fk_anger_id,  fk_anxiety_id, fk_depression_id, fk_self_consciousness_id, fk_vulnerability_id, fk_immoderation_id)" +
                "values(" + ang_id + "," + anx_id + ", " + dep_id + "," + self_con_id + "," + vuln_id + "," + imo_id +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        emotional_range_id = rs.getInt(1);

        //conscientiousness
        int duti_id = 0, caut_id= 0, achieve_strive_id = 0, self_eff_id = 0, self_disc_id = 0, order_id = 0, conscientiousness_id = 0;

        if ( (openCh.size() >= 1)) {
        //dutifulness
        stmt.executeUpdate("Insert into big_5_conscientiousness_dutifulness(percentile, raw_score) values (" + conCh.get(0).getPercentile() + "," + conCh.get(0).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        duti_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_conscientiousness_dutifulness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            duti_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 2)) {
        //cautiousness
        stmt.executeUpdate("Insert into  big_5_conscientiousness_cautiousness(percentile, raw_score) values (" + conCh.get(1).getPercentile() + "," + conCh.get(1).getRawScore() +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        caut_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_conscientiousness_cautiousness(percentile, raw_score) values (" + null + "," + null +")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            caut_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 3)) {
        //achievement striving
        stmt.executeUpdate("Insert into  big_5_conscientiousness_achievement_striving(percentile, raw_score) values (" + conCh.get(2).getPercentile() + "," + conCh.get(2).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        achieve_strive_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_conscientiousness_achievement_striving(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            achieve_strive_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 4)) {
        //self efficacy
        stmt.executeUpdate("Insert into big_5_conscientiousness_self_efficacy(percentile, raw_score) values (" + conCh.get(3).getPercentile() + "," + conCh.get(3).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        self_eff_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_conscientiousness_self_efficacy(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            self_eff_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 5)) {
        //self discipline
        stmt.executeUpdate("Insert into big_5_conscientiousness_self_discipline(percentile, raw_score) values (" + conCh.get(4).getPercentile() + "," + conCh.get(4).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        self_disc_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_conscientiousness_self_discipline(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            self_disc_id = rs.getInt(1);
        }

        if ( (openCh.size() == 6)) {
        //orderliness
        stmt.executeUpdate("Insert into  big_5_conscientiousness_orderliness(percentile, raw_score) values (" + conCh.get(5).getPercentile() + "," + conCh.get(5).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        order_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_conscientiousness_orderliness(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            order_id = rs.getInt(1);
        }

        //conscientiousness insert id.
        stmt.executeUpdate("Insert INTO big_5_conscientiousness(fk_self_efficacy_id,  fk_self_discipline_id, fk_orderliness_id, fk_dutifulness_id, fk_cautiousness_id, fk_achievement_striving_id)" +
                "values(" + self_eff_id + "," + self_disc_id + ", " + order_id + "," + duti_id + "," + caut_id + "," + achieve_strive_id +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        conscientiousness_id = rs.getInt(1);

        //Agreeablenes
        int coop_id = 0, mor_id = 0, trust_id = 0, altruism_id = 0, mode_id = 0, sympa_id = 0, agreeableness_id = 0;

        if ( (openCh.size() >= 1)) {
        //cooperation
        stmt.executeUpdate("Insert into big_5_agreeableness_cooperation(percentile, raw_score) values (" + agreeCh.get(0).getPercentile() + "," + agreeCh.get(0).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        coop_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_agreeableness_cooperation(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            coop_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 2)) {
        //morality
        stmt.executeUpdate("Insert into  big_5_agreeableness_morality(percentile, raw_score) values (" + agreeCh.get(1).getPercentile() + "," + agreeCh.get(1).getRawScore() +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        mor_id = rs.getInt(1);}
            else {
            stmt.executeUpdate("Insert into  big_5_agreeableness_morality(percentile, raw_score) values (" + null + "," + null +")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            mor_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 3)) {
        //trust
        stmt.executeUpdate("Insert into  big_5_agreeableness_trust(percentile, raw_score) values (" + agreeCh.get(2).getPercentile() + "," + agreeCh.get(2).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        trust_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_agreeableness_trust(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            trust_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 4)) {
        //altruism
        stmt.executeUpdate("Insert into big_5_agreeableness_altruism(percentile, raw_score) values (" + agreeCh.get(3).getPercentile() + "," + agreeCh.get(3).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        altruism_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_agreeableness_altruism(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            altruism_id = rs.getInt(1);
        }

        if ( (openCh.size() >= 5)) {
        //modesty
        stmt.executeUpdate("Insert into big_5_agreeableness_modesty(percentile, raw_score) values (" + agreeCh.get(4).getPercentile() + "," + agreeCh.get(4).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        mode_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into big_5_agreeableness_modesty(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            mode_id = rs.getInt(1);
        }


        if ( (openCh.size() >= 6)) {
        //sympathy
        stmt.executeUpdate("Insert into  big_5_agreeableness_sympathy(percentile, raw_score) values (" + agreeCh.get(5).getPercentile() + "," + agreeCh.get(5).getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        sympa_id = rs.getInt(1);}
        else {
            stmt.executeUpdate("Insert into  big_5_agreeableness_sympathy(percentile, raw_score) values (" + null + "," + null + ")");
            rs = stmt.executeQuery("SELECT last_insert_id()");
            rs.next();
            sympa_id = rs.getInt(1);
        }

        //agreeableness insert id.
        stmt.executeUpdate("Insert INTO big_5_agreeableness(fk_altruism_id,  fk_cooperation_id, fk_modesty_id, fk_morality_id, fk_sympathy_id, fk_trust_id)" +
                "values(" + altruism_id + "," + coop_id + ", " + mode_id + "," + mor_id + "," + sympa_id + "," + trust_id +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        agreeableness_id = rs.getInt(1);

        stmt.executeUpdate("insert into big_five(fk_big_5_openness_id, fk_big_5_extraversion_id, fk_big_5_emotional_range_id, fk_big_5_conscientiousness_id," +
                "fk_big_5_agreeableness_id) values(" + opennessId + "," + extroversion_id + "," + emotional_range_id + "," + conscientiousness_id + "," + agreeableness_id +")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        int i = rs.getInt(1);

        rs.close();
        stmt.close();
        connection.close();

        return i;
    }

    /**
     * Private method used in order to insert within the Watson Needs db table.
     *
     * @param needsLst A list Containing ITrait objects.
     * @return The inserted records id.
     * @throws SQLException
     * @see com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
     */
    private int insert_watson_needs(List<ITrait> needsLst) throws  SQLException {
        Connection connection =  DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();

        ResultSet rs = null;

        for (int i = 0; i < needsLst.size(); i++) {
            System.out.println(i + " is i and percentile " + needsLst.get(i).getPercentile() + " raw score " + needsLst.get(i).getRawScore());
        }


        rs = stmt.executeQuery("SELECT f_ins_watson_needs(" + needsLst.get(0).getPercentile() + "," + needsLst.get(0).getRawScore() + "," +
        needsLst.get(1).getPercentile() + "," + needsLst.get(1).getRawScore() + "," + needsLst.get(2).getPercentile() + "," + needsLst.get(2).getRawScore() + ","
        + needsLst.get(3).getPercentile() + "," + needsLst.get(3).getRawScore() +"," + needsLst.get(4).getPercentile() +"," + needsLst.get(4).getRawScore() +"," +
        needsLst.get(5).getPercentile() +"," + needsLst.get(5).getRawScore() +"," + needsLst.get(6).getPercentile() + "," + needsLst.get(6).getRawScore() +"," +
        needsLst.get(7).getPercentile() +"," + needsLst.get(7).getRawScore() +"," + needsLst.get(8).getPercentile() +"," + needsLst.get(8).getRawScore()
                +"," + needsLst.get(9).getPercentile() +"," + needsLst.get(9).getRawScore() +"," + needsLst.get(10).getPercentile() +"," + needsLst.get(10).getRawScore()
                +"," +needsLst.get(11).getPercentile() +"," + needsLst.get(11).getRawScore() + ")");

        rs.next();
        int i = rs.getInt(1);

        rs.close();
        stmt.close();
        connection.close();

        return i;


       /* ITrait excitementTrt = needsLst.get(3), harmonyTrt = needsLst.get(4), curiosityTrt = needsLst.get(2),
                libertyTrt = needsLst.get(6), practicalityTrt = needsLst.get(8), challengeTrt = needsLst.get(0),
                idealTrt = needsLst.get(5), closenessTrt = needsLst.get(1), self_expressionTrt = needsLst.get(9),
                loveTrt = needsLst.get(7), stabilityTrt = needsLst.get(10), structureTrt = needsLst.get(11);

        int excitementId, harmonyId, curiosityId, libertyId, practicalityId, challengeId, idealId, closenessId, self_expressionId,
        loveId, stabilityId, structureId;

        Statement stmt = connection.createStatement();

        System.out.println(stmt.equals(null));

        ResultSet rs;
       //Excitement
        stmt.executeUpdate("INSERT  INTO watson_needs_excitement(percentile, raw_score) values(" + excitementTrt.getPercentile() + ","
         + excitementTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");

        excitementId = rs.getInt(1);

        //Harmony
        stmt.executeUpdate("INSERT  INTO watson_needs_harmony(percentile, raw_score) VALUES (" + harmonyTrt.getPercentile() + "," +
        harmonyTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        harmonyId = rs.getInt(1);

        //Curiosity
        stmt.executeUpdate("INSERT  INTO watson_needs_curiosity(percentile, raw_score) VALUES (" + curiosityTrt.getPercentile() + "," +
        curiosityTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        curiosityId = rs.getInt(1);

        //Liberty
        stmt.executeUpdate("INSERT  into watson_needs_liberty(percentile, raw_score) values(" + libertyTrt.getPercentile() + "," + libertyTrt.getRawScore() + ")" );
        rs = stmt.executeQuery("SELECT last_insert_id()");
        libertyId = rs.getInt(1);

        //ideal
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + idealTrt.getPercentile() + "," + idealTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        idealId = rs.getInt(1);

        //closeness
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + closenessTrt.getPercentile() + "," + closenessTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        closenessId = rs.getInt(1);

        //self-expression
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + self_expressionTrt.getPercentile() + "," + self_expressionTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        self_expressionId = rs.getInt(1);

        //love
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + loveTrt.getPercentile() + "," + loveTrt.getRawScore() +")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        loveId = rs.getInt(1);

        //practicality
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + practicalityTrt.getPercentile() + "," + practicalityTrt.getRawScore() +")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        practicalityId = rs.getInt(1);

        //stability
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + stabilityTrt.getPercentile() + "," + stabilityTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        stabilityId = rs.getInt(1);

        //Challenge
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" +  challengeTrt.getPercentile() + "," + challengeTrt.getRawScore() +")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        challengeId = rs.getInt(1);

        //Structure
        stmt.executeUpdate("INSERT  into watson_needs_ (percentile, raw_score) values(" + structureTrt.getPercentile() + "," + stabilityTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        structureId = rs.getInt(1);

        stmt.executeUpdate("INSERT Into watson_needs(fk_excitement_id, fk_harmony_id, fk_curiosity_id, fk_liberty_id, fk_ideal_id, fk_closeness_id, fk_self_expression_id," +
                "fk_love_id), fk_practicality_id, fk_stability_id, fk_challenge_id, fk_structure_id) values("
                + excitementId + "," + harmonyId + "," + curiosityId + "," + libertyId + "," + idealId + "," + closenessId + "," + self_expressionId + "," +
        loveId + ", " + practicalityId + "," + stabilityId + "," + challengeId + "," + structureId + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        rs.next();
        int lastInsert = rs.getInt(1);
        return lastInsert;*/
    }


    /**
     * Method used to insert into watson's values db table.
     *
     * @param valuesLst
     * @return The id of the table the it was inserted in.
     * @throws SQLException
     */
    private int insert_watson_values(List<ITrait> valuesLst) throws SQLException {
        //Traits assigned based on how watson handles and provides traits.
        ITrait self_transcendenceTrt = valuesLst.get(4), conservationTrt = valuesLst.get(0), hedonismTrt = valuesLst.get(2),
                self_enhancementTrt = valuesLst.get(3), open_to_changeTrt = valuesLst.get(1);

        int self_transcendenceId, conservationId , hedonismId, self_enhancementId, open_to_changeId;

        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        //Insert self transcendence
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("Insert into watson_values_self_transcendence(percentile, raw_score) values (" +
        self_transcendenceTrt.getPercentile() +"," + self_enhancementTrt.getRawScore()+")" );
        ResultSet rs = stmt.executeQuery("select last_insert_id()");
        rs.next();
        self_transcendenceId = rs.getInt(1);

        //Hedonism
        stmt.executeUpdate("insert into watson_values_hedonism(percentile, raw_score) values (" + hedonismTrt.getPercentile() + "," +
        hedonismTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT  last_insert_id()");
        rs.next();
        hedonismId = rs.getInt(1);

        //Open to change
        stmt.executeUpdate("INSERT  INTO watson_values_open_to_change(percentile, raw_score) values(" + open_to_changeTrt.getPercentile() + "," +
        open_to_changeTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        open_to_changeId = rs.getInt(1);
        System.out.println("open to change id = " +open_to_changeId );

        //Conservation
        stmt.execute("INSERT INTO watson_values_conservation(percentile, raw_score) values(" + conservationTrt.getPercentile() + "," +
        conservationTrt.getRawScore() + ")");
        rs = stmt.executeQuery("SELECT last_insert_id()");
        rs.next();
        conservationId = rs.getInt(1);

        //self_enhancement
        stmt.execute("Insert into watson_values_self_enhancement(percentile, raw_score) VALUES (" +
         self_enhancementTrt.getPercentile() +"," + self_enhancementTrt.getRawScore() + ")");
        rs = stmt.executeQuery("select last_insert_id()");
        rs.next();
        self_enhancementId = rs.getInt(1);

        //Insert into watson values
        stmt.execute("insert into watson_values(fk_self_transcendence_id, fk_conservation_id, fk_hedonism_id, fk_self_enhancement_id, fk_open_to_change_id) values("
        + self_transcendenceId + "," + conservationId + ","  + hedonismId + "," + self_enhancementId + "," + open_to_changeId + ")");
        rs = stmt.executeQuery(" SELECT last_insert_id()");
        rs.next();
        int i = rs.getInt(1);

        rs.close();
        stmt.close();
        connection.close();

        return rs.getInt(1);
    }


    /**
     * Private method used in order to get a client's watson resutls.
     *
     * @param clientId The client's id.
     * @return A Watson results object that has the client's information.
     * @throws SQLException
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     */
    //Tested when was public.
    private WatsonResults getClientSingleWatsonResult(int clientId) throws SQLException{
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        Statement stmt = connection.createStatement();

        //Getting fk_watson_results_id;
        ResultSet rs = stmt.executeQuery("select fk_watson_results_id from client_texts where fk_app_client_id = "+ clientId);
        rs.next();
        int fk_watson_results_id = rs.getInt(1);

        //Getting the ids of each subsequent table.
        rs = stmt.executeQuery("select fk_big_five_id, fk_needs_id, fk_values_id from watson_results where id = " + fk_watson_results_id);
        rs.next();

        int big_5_id, values_id, needs_id;
        big_5_id = rs.getInt(1);
        needs_id = rs.getInt(2);
        values_id = rs.getInt(3);

        int b5Open, b5Extra, b5er, b5con, b5agree;

        rs = stmt.executeQuery("select fk_big_5_openness_id, fk_big_5_agreeableness_id, fk_big_5_extraversion_id, fk_big_5_emotional_range_id, fk_big_5_conscientiousness_id\n" +
                "from big_five where id = " + big_5_id);
        rs.next();

        b5Open = rs.getInt(1);
        b5Extra = rs.getInt(2);
        b5er = rs.getInt(3);
        b5con = rs.getInt(4);
        b5agree = rs.getInt(5);

        //Getting watsonValues
        List<com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait> nValuesTrtLst = new LinkedList<>();
        com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait self_tranc = new Trait(), hedon = new Trait(), otc = new Trait(),
                conservation = new Trait(), self_enha = new Trait();
        WatsonResults wResults = new WatsonResults();

        CallableStatement callStmt = connection.prepareCall("{call get_watson_values(?)}");
        callStmt.setInt(1, values_id);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        //Adding values to the trait objects.
        self_tranc.setCategory("Watson values");
        self_tranc.setName("Self transendence");
        self_tranc.setPercentile(rs.getDouble(1));
        self_tranc.setRawScore(rs.getDouble(2));
        nValuesTrtLst.add(self_tranc);

        hedon.setCategory("Watson Values");
        hedon.setId("Hedonism");
        hedon.setPercentile(rs.getDouble(3));
        hedon.setRawScore(rs.getDouble(4));
        nValuesTrtLst.add(hedon);

        otc.setCategory("Watson Values");
        otc.setId("open to change");
        otc.setPercentile(rs.getDouble(5));
        otc.setRawScore(rs.getDouble(6));
        nValuesTrtLst.add(otc);

        conservation.setCategory("Watson values");
        conservation.setId("Conservation");
        conservation.setPercentile(rs.getDouble(7));
        conservation.setRawScore(rs.getDouble(8));
        nValuesTrtLst.add(conservation);

        self_enha.setCategory("Watson values");
        self_enha.setId("self enhancement");
        self_enha.setPercentile(rs.getDouble(9));
        self_enha.setRawScore(rs.getDouble(10));
        nValuesTrtLst.add(self_enha);

        wResults.setLstNativeValues(nValuesTrtLst);

        //Watson needs
        List<com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait> nNeedsTrtLst = new LinkedList<>();
        com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait excitement = new Trait(), harmony = new Trait(), curiosity = new Trait(),
                liberty = new Trait(), practicality = new Trait(), challenge = new Trait(), ideal = new Trait(),
                closeness = new Trait(), self_expression = new Trait(), love = new Trait(), stability = new Trait(),
                structure = new Trait();

        callStmt = connection.prepareCall("{call get_watson_needs(?)}");
        callStmt.setInt(1, needs_id);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        excitement.setCategory("Watson needs");
        excitement.setId("excitement");
        excitement.setPercentile(rs.getDouble(1));
        excitement.setRawScore(rs.getDouble(2));
        nNeedsTrtLst.add(excitement);

        harmony.setCategory("Watson needs");
        harmony.setId("harmony");
        harmony.setPercentile(rs.getDouble(3));
        harmony.setRawScore(rs.getDouble(4));
        nNeedsTrtLst.add(harmony);

        curiosity.setCategory("Watson needs");
        curiosity.setId("curiosity");
        curiosity.setPercentile(rs.getDouble(5));
        curiosity.setRawScore(rs.getDouble(6));
        nNeedsTrtLst.add(curiosity);

        liberty.setCategory("Watson needs");
        liberty.setId("liberty");
        liberty.setPercentile(rs.getDouble(7));
        liberty.setRawScore(rs.getDouble(8));
        nNeedsTrtLst.add(liberty);

        practicality.setCategory("Watson needs");
        practicality.setId("practicality");
        practicality.setPercentile(rs.getDouble(9));
        practicality.setRawScore(rs.getDouble(10));
        nNeedsTrtLst.add(practicality);

        challenge.setCategory("Watson needs");
        challenge.setId("challenge");
        challenge.setPercentile(rs.getDouble(11));
        challenge.setPercentile(rs.getDouble(12));
        nNeedsTrtLst.add(challenge);

        ideal.setCategory("Watson needs");
        ideal.setId("ideal");
        ideal.setPercentile(rs.getDouble(13));
        ideal.setPercentile(rs.getDouble(14));
        nNeedsTrtLst.add(ideal);

        closeness.setCategory("Watson needs");
        closeness.setId("closeness");
        closeness.setPercentile(rs.getDouble(15));
        closeness.setRawScore(rs.getDouble(16));
        nNeedsTrtLst.add(closeness);

        self_expression.setCategory("Watson needs");
        self_expression.setId("self_expression");
        self_expression.setPercentile(rs.getDouble(17));
        self_expression.setRawScore(rs.getDouble(18));
        nNeedsTrtLst.add(self_expression);

        love.setCategory("Watson needs");
        love.setId("love");
        love.setPercentile(rs.getDouble(19));
        love.setRawScore(rs.getDouble(20));
        nNeedsTrtLst.add(love);

        stability.setCategory("Watson needs");
        stability.setId("stability");
        stability.setRawScore(rs.getDouble(21));
        stability.setPercentile(rs.getDouble(22));
        nNeedsTrtLst.add(stability);

        structure.setCategory("Watson needs");
        structure.setId("structure");
        structure.setPercentile(rs.getDouble(23));
        structure.setRawScore(rs.getDouble(24));
        nNeedsTrtLst.add(structure);

        wResults.setLstNativeNeeds(nNeedsTrtLst);

        //B5 opennness
        List<ITrait> nB5 = new LinkedList<>(), nB5OpenCh = new LinkedList<>();
        ITrait adventurous = new Trait(), artistic_interests = new Trait(), emotionality = new Trait(),
                imagination = new Trait(), intelect = new Trait(), liberalism = new Trait(), b5OpenTrt = new Trait();

        rs = stmt.executeQuery("select percentile, raw_score from big_5_openness where id = " + b5Open);
        rs.next();

        //Getting percentile.
        b5OpenTrt.setId("big 5 openness");
        b5OpenTrt.setCategory("big 5");
        b5OpenTrt.setPercentile(rs.getDouble(1));
        b5OpenTrt.setRawScore(rs.getDouble(2));

        //Getting b5 openness children.
        callStmt = connection.prepareCall("{call getB5Open(?)}");
        callStmt.setInt(1,b5Open);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        adventurous.setCategory("b5 openness");
        adventurous.setId("adventurous");
        adventurous.setPercentile(rs.getDouble(1));
        adventurous.setRawScore(rs.getDouble(2));
        nB5OpenCh.add(adventurous);

        artistic_interests.setCategory("b5 openness");
        artistic_interests.setId("artistic interests");
        artistic_interests.setPercentile(rs.getDouble(3));
        artistic_interests.setRawScore(rs.getDouble(4));
        nB5OpenCh.add(artistic_interests);

        emotionality.setCategory("b5 openness");
        emotionality.setId("emotionality");
        emotionality.setPercentile(rs.getDouble(5));
        emotionality.setRawScore(rs.getDouble(6));
        nB5OpenCh.add(emotionality);

        imagination.setCategory("b5 openness");
        imagination.setId("facet imagination");
        imagination.setPercentile(rs.getDouble(7));
        imagination.setRawScore(rs.getDouble(8));
        nB5OpenCh.add(imagination);

        intelect.setCategory("b5 openness");
        intelect.setId("facet intelect");
        intelect.setPercentile(rs.getDouble(9));
        intelect.setRawScore(rs.getDouble(10));
        nB5OpenCh.add(intelect);

        liberalism.setCategory("b5 openness");
        liberalism.setId("liberalism");
        liberalism.setPercentile(rs.getDouble(11));
        liberalism.setRawScore(rs.getDouble(12));
        nB5OpenCh.add(liberalism);

        b5OpenTrt.setChildren(nB5OpenCh);

        //B5 extroversion
        rs = stmt.executeQuery("select percentile, raw_score from big_5_extraversion where id = " + b5Extra);
        rs.next();
        ITrait b5ExtraTrt = new Trait(), activity_level = new Trait(), assertiveness = new Trait(), cheerfulness = new Trait(),
                excitement_seeking = new Trait(), friendliness = new Trait(), gregariousness = new Trait();
        List<ITrait> b5ExtrCh = new LinkedList<>();

        //Getting percentile.
        b5ExtraTrt.setId("big 5 extroversion");
        b5ExtraTrt.setCategory("big 5");
        b5ExtraTrt.setPercentile(rs.getDouble(1));
        b5ExtraTrt.setRawScore(rs.getDouble(2));

        //Getting b5 openness children.
        callStmt = connection.prepareCall("{call getB5Extra(?)}");
        callStmt.setInt(1,b5Extra);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        activity_level.setCategory("b5 extroversion");
        activity_level.setId("facet activity level");
        activity_level.setPercentile(rs.getDouble(1));
        activity_level.setRawScore(rs.getDouble(2));
        b5ExtrCh.add(activity_level);

        assertiveness.setCategory("b5 extroversion");
        assertiveness.setId("facet assertiveness");
        assertiveness.setPercentile(rs.getDouble(3));
        assertiveness.setRawScore(rs.getDouble(4));
        b5ExtrCh.add(assertiveness);

        cheerfulness.setCategory("b5 extroversion");
        cheerfulness.setId("facet cheerfulness");
        cheerfulness.setPercentile(rs.getDouble(5));
        cheerfulness.setRawScore(rs.getDouble(6));
        b5ExtrCh.add(cheerfulness);

        excitement_seeking.setCategory("b5 extroversion");
        excitement_seeking.setId("facet excitement seeking");
        excitement_seeking.setPercentile(rs.getDouble(7));
        excitement_seeking.setRawScore(rs.getDouble(8));
        b5ExtrCh.add(excitement_seeking);

        friendliness.setCategory("b5 extroversion");
        friendliness.setId("facet friendliness");
        friendliness.setPercentile(rs.getDouble(9));
        friendliness.setRawScore(rs.getDouble(10));
        b5ExtrCh.add(friendliness);

        gregariousness.setCategory("b5 extroversion");
        gregariousness.setId("facet gregariousness");
        gregariousness.setPercentile(rs.getDouble(11));
        gregariousness.setRawScore(rs.getDouble(12));
        b5ExtrCh.add(gregariousness);

        b5ExtraTrt.setChildren(b5ExtrCh);

        //B5 emotional range
        rs = stmt.executeQuery("select percentile, raw_score from big_5_emotional_range where id = " + b5er);
        rs.next();
        ITrait b5ErTrt = new Trait(),anger = new Trait(), anxiety = new Trait(), depression = new Trait(),
                self_conscious = new Trait(), vulnerability = new Trait(), immoderation = new Trait();
        List<ITrait> b5ErCh = new LinkedList<>();

        //Getting percentile, raw score
        b5ErTrt.setId("big 5 emotional range");
        b5ErTrt.setCategory("big 5");
        b5ErTrt.setPercentile(rs.getDouble(1));
        b5ErTrt.setRawScore(rs.getDouble(2));

        //Getting b5 er children.
        callStmt = connection.prepareCall("{call getB5ER(?)}");
        callStmt.setInt(1,b5er);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        anger.setCategory("big 5 emotional range");
        anger.setId("facet anger");
        anger.setPercentile(rs.getDouble(1));
        anger.setRawScore(rs.getDouble(2));
        b5ErCh.add(anger);

        anxiety.setId("big 5 emotional range");
        anxiety.setCategory("facet anxiety");
        anxiety.setPercentile(rs.getDouble(3));
        anxiety.setRawScore(rs.getDouble(4));
        b5ErCh.add(anxiety);

        depression.setId("big 5 emotional range");
        depression.setCategory("facet depression");
        depression.setPercentile(rs.getDouble(5));
        depression.setRawScore(rs.getDouble(6));
        b5ErCh.add(depression);

        self_conscious.setId("big 5 emotional range");
        self_conscious.setCategory("facet self conscious");
        self_conscious.setPercentile(rs.getDouble(7));
        self_conscious.setRawScore(rs.getDouble(8));
        b5ErCh.add(self_conscious);

        vulnerability.setId("big 5 emotional range");
        vulnerability.setCategory("facet vulnerability");
        vulnerability.setPercentile(rs.getDouble(9));
        vulnerability.setRawScore(rs.getDouble(10));
        b5ErCh.add(vulnerability);

        immoderation.setId("big 5 emotional range");
        immoderation.setCategory("facet immoderation");
        imagination.setPercentile(rs.getDouble(11));
        imagination.setRawScore(rs.getDouble(12));
        b5ErCh.add(immoderation);

        b5ErTrt.setChildren(b5ErCh);

        //B5 Conscientiousness
        rs = stmt.executeQuery("select percentile, raw_score from big_5_conscientiousness where id = " + b5con);
        rs.next();
        ITrait b5ConTrt = new Trait(),dutifulness = new Trait(), cautiousness = new Trait(), achievement_striving = new Trait(),
                self_efficacy = new Trait(), self_discipline = new Trait(), orderliness = new Trait();
        List<ITrait> b5ConCh = new LinkedList<>();

        b5ConTrt.setId("big 5 conscientiousness");
        b5ConTrt.setCategory("big 5");
        b5ConTrt.setPercentile(rs.getDouble(1));
        b5ConTrt.setRawScore(rs.getDouble(2));

        //Getting b5 con children.
        callStmt = connection.prepareCall("{call getB5Con(?)}");
        callStmt.setInt(1,b5con);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        dutifulness.setId("big 5 conscientiousness");
        dutifulness.setCategory("facet dutifulness");
        dutifulness.setPercentile(rs.getDouble(1));
        dutifulness.setRawScore(rs.getDouble(2));
        b5ConCh.add(dutifulness);

        cautiousness.setId("big 5 conscientiousness");
        cautiousness.setCategory("facet cautiousness");
        cautiousness.setPercentile(rs.getDouble(3));
        cautiousness.setRawScore(rs.getDouble(4));
        b5ConCh.add(cautiousness);

        achievement_striving.setId("big 5 conscientiousness");
        achievement_striving.setId("facet achievement striving");
        achievement_striving.setPercentile(rs.getDouble(5));
        achievement_striving.setRawScore(rs.getDouble(6));
        b5ConCh.add(achievement_striving);

        self_efficacy.setId("big 5 conscientiousness");
        self_conscious.setCategory("facet self_conscious");
        self_conscious.setPercentile(rs.getDouble(7));
        self_conscious.setRawScore(rs.getDouble(8));
        b5ConCh.add(self_conscious);

        self_discipline.setId("big 5 conscientiousness");
        self_discipline.setCategory("facet self_discipline");
        self_discipline.setPercentile(rs.getDouble(9));
        self_discipline.setRawScore(rs.getDouble(10));
        b5ConCh.add(self_discipline);

        orderliness.setId("b5 orderliness");
        orderliness.setCategory("facet orderliness");
        orderliness.setPercentile(rs.getDouble(11));
        orderliness.setRawScore(rs.getDouble(12));
        b5ConCh.add(orderliness);

        b5ConTrt.setChildren(b5ConCh);

        //Agreeableness
        rs = stmt.executeQuery("select percentile, raw_score from big_5_agreeableness where id = " + b5agree);
        rs.next();
        ITrait b5AgreeTrt = new Trait(),cooperation = new Trait(), morality = new Trait(), trust = new Trait(),
                altruism = new Trait(), modesty = new Trait(), sympathy = new Trait();
        List<ITrait> b5AgreeCh = new LinkedList<>();

        b5AgreeTrt.setId("b5 agreeableness");
        b5AgreeTrt.setCategory("big 5");
        b5AgreeTrt.setPercentile(rs.getDouble(1));
        b5AgreeTrt.setRawScore(rs.getDouble(2));

        //Getting b5 con children.
        callStmt = connection.prepareCall("{call getB5Con(?)}");
        callStmt.setInt(1,b5con);
        callStmt.execute();
        rs = callStmt.getResultSet();
        rs.next();

        cooperation.setId("facet cooperation");
        cooperation.setCategory("big 5 agreeableness");
        cooperation.setPercentile(rs.getDouble(1));
        cooperation.setRawScore(rs.getDouble(2));
        b5AgreeCh.add(cooperation);

        morality.setCategory("big 5 agreeableness");
        morality.setId("facet morality");
        morality.setPercentile(rs.getDouble(3));
        morality.setRawScore(rs.getDouble(4));
        b5AgreeCh.add(morality);

        trust.setCategory("big 5 agreeableness");
        trust.setId("facet trust");
        trust.setPercentile(rs.getDouble(5));
        trust.setRawScore(rs.getDouble(6));
        b5AgreeCh.add(trust);

        altruism.setCategory("big 5 agreeableness");
        altruism.setId("facet altruism");
        altruism.setPercentile(rs.getDouble(7));
        altruism.setRawScore(rs.getDouble(8));
        b5AgreeCh.add(altruism);

        modesty.setCategory("big 5 agreeableness");
        modesty.setId("facet modesty");
        modesty.setPercentile(rs.getDouble(9));
        modesty.setRawScore(rs.getDouble(10));
        b5ConCh.add(modesty);

        sympathy.setCategory("big 5 agreeableness");
        sympathy.setId("facet sympathy");
        sympathy.setPercentile(rs.getDouble(11));
        sympathy.setRawScore(rs.getDouble(12));
        b5ConCh.add(sympathy);

        b5ConTrt.setChildren(b5ConCh);

        List<ITrait> b5AllLst = new LinkedList<>();

        b5AllLst.add(b5OpenTrt);
        b5AllLst.add(b5ExtraTrt);
        b5AllLst.add(b5ErTrt);
        b5AllLst.add(b5ConTrt);
        b5AllLst.add(b5AgreeTrt);

        wResults.setLstB5(b5AllLst);

        return wResults;
    }
}
