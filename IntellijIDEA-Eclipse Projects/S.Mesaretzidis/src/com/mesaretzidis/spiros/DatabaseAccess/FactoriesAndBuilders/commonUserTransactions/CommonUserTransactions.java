package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactionsPreProxy;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Concrete class implementation of the abstract class ACommonUserTransactionsProxy
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions
 */
class CommonUserTransactions extends ACommonUserTransactions {

    /**
     * Class constructor accepting as input an object of a class that extends ACommonUserTransactionsPreProxy.
     *
     * @param commonUserTransactionsPreProxy An object of a class that extends ACommonUserTransactionsPreProxy
     */
    public CommonUserTransactions(ACommonUserTransactionsPreProxy commonUserTransactionsPreProxy) {
        super(commonUserTransactionsPreProxy);
    }

    /**
     * Default class constructor.
     */
    public CommonUserTransactions() {}

    /**
     * Method used in order to get a client's stored text.
     *
     * @param clientId The client's id.
     * @return A String containing a
     */
    public String[] getClientTexts(int clientId) throws SQLException {
        return commonUserTransactionsPreProxy.getClientTexts(clientId);
    }

    /**
     * Method used in order to get a client's stored Weka results tree.
     *
     * @param clientId The client's id.
     * @return
     */
    public String[] getWekaResults(int clientId) throws IOException, SQLException {
        return commonUserTransactionsPreProxy.getWekaResults(clientId);
    }

    /**
     * Connection string field getter.
     *
     * @return The set connection string.
     */
    @Override
    public String getConnectionString() {
        return commonUserTransactionsPreProxy.getConnectionString();
    }

    /**
     * Connection String field setter.
     *
     * @param connectionString
     */
    @Override
    public void setConnectionString(String connectionString) {
        commonUserTransactionsPreProxy.setConnectionString(connectionString);
    }

    /**
     * Password field getter.
     *
     * @return String containing the set password.
     */
    @Override
    public String getPassword() {
        return commonUserTransactionsPreProxy.getPassword();
    }

    /**
     * password field setter.
     *
     * @param password The password to be set.
     */
    @Override
    public void setPassword(String password) {
        commonUserTransactionsPreProxy.setPassword(password);
    }

    /**
     * Username field getter.
     *
     * @return The set password.
     */
    @Override
    public String getUsername() {
        return commonUserTransactionsPreProxy.getUsername();
    }

    /**
     * Username field setter.
     *
     * @param username
     */
    @Override
    public void setUsername(String username) {
        commonUserTransactionsPreProxy.setUsername(username);
    }

    /**
     * Method used to retrieve all application clients.
     * @return A List implementation used to retrieve all application clients.
     * @see com.mesaretzidis.spiros.Entities.Client
     * @see java.util.List
     */
    @Override
    public List<Client> getAllClients() throws SQLException {
        return commonUserTransactionsPreProxy.getAllClients();
    }

    /**
     * Method used in order to retrieve a client by his/her id.
     * @param id The client's id.
     * @return A Client object, containing the necessary information.
     * @see com.mesaretzidis.spiros.Entities.Client
     */
    @Override
    public Client getClientById(int id) throws SQLException {
       return commonUserTransactionsPreProxy.getClientById(id);
    }

    /**
     * Method used to retrieve a clients WatsonResults
     *
     * @param clientId
     * @return A List implementation containing WatsonResults objects, which in turn contain the client's watson results.
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     * @see java.util.List
     */
    @Override
    public List<WatsonResults> getClientWatsonResults(int clientId) throws SQLException {
        return commonUserTransactionsPreProxy.getClientWatsonResults(clientId);
    }

    /**
     * Method used in order to enter a new record in the database.
     *
     * @param clientId   the client's id.
     * @param clientText the input text
     * @param wekaResults the weka results tree
     * @param wResults    the watson results.
     */
    @Override
    public int insertRecord(int clientId, String clientText, String wekaResults, WatsonResults wResults) throws SQLException, IOException {
        return commonUserTransactionsPreProxy.insertRecord(clientId, clientText, wekaResults, wResults);
    }

    /**
     * Method used to add a new client
     *
     * @param fn The client's first name.
     * @param ln The client's last name.
     * @return The id of the new client's record.
     */
    @Override
    public int addNewClient(String fn, String ln) throws SQLException {
        return commonUserTransactionsPreProxy.addNewClient(fn, ln);
    }
}
