package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.adminTransactions.IAdminTransactionBuilder;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions.ICommonUserTransactionsFactory;

/**
 * This class was developed in order to implement the builder design pattern.
 *
 * @author S. Mesaretzidis
 * @since 9/1/2017.
 */
class AdminTransactionsBuilder implements IAdminTransactionBuilder {

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class AAdminTransactionsPreProxy.
     *
     * @param username         The username to be used.
     * @param password         The password to be used.
     * @param connectionString The connection string to be used.
     * @return An object of a class that implements the abstract class AAdminTransactionsProxy.
     */
    @Override
    public AAdminTransactionsPreProxy getAdminTransactions(String username, String password, String connectionString) {
        ICommonUserTransactionsFactory commonUserTransactionsFactory = new CommonUserTransactionsFactory();

        ACommonUserTransactions commonUserTransactions = commonUserTransactionsFactory.getObject(username, password, connectionString);

        AAdminTransactionsPreProxy aAdminTransactionsPreProxy = new AdminTransactionsPreProxy();

        aAdminTransactionsPreProxy.setCommonUserTransactions(commonUserTransactions);

        AdminTransactions adminTransactions = new AdminTransactions();

        adminTransactions.setPreProxy(aAdminTransactionsPreProxy);

        return aAdminTransactionsPreProxy;
    }
}






