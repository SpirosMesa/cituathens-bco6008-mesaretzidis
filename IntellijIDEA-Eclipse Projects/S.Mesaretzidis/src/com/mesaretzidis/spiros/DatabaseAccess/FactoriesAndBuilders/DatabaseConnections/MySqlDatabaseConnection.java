package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.DatabaseConnections;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ADatabaseConnection;
import com.sun.istack.internal.NotNull;

import java.sql.*;

/**
 * Concrete implementation of abstract class ADatabaseConnection. Used to perform transactions
 * for
 *
 * @author S. Mesaretzidis
 * @since 2/1/2017.
 */
class MySqlDatabaseConnection extends ADatabaseConnection {

    /**
     * Constructor that receives as input the user's username, password and a not-default connection string.
     *
     * @param username         The username.
     * @param password         The password.
     * @param connectionString The not default connection string.
     */
    public MySqlDatabaseConnection(@NotNull String username, @NotNull String password, @NotNull String connectionString) {
        super(username, password, connectionString);
    }

    /**
     * Method used to retrieve a database connection.
     *
     * @return A boolean value indicating if the user has administrator rights.
     * @throws com.mesaretzidis.spiros.Exceptions.CredentialsNotSetException
     */
    public boolean isAdmin() throws SQLException {
        Connection connection = DriverManager.getConnection(this.connectionString, this.username, this.password);

        boolean isAdmin = false;

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from mysql.user");
        rs.next();

        //Only admin has delete privileges.
        if (rs.getString("Delete_priv").equals("Y")) isAdmin = true;

        rs.close();
        stmt.close();
        connection.close();

        return isAdmin;
    }
}
