package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.adminTransactions.IAdminTransactionsFactory;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions.ICommonUserTransactionsFactory;

/**
 * Factory used in order to provide an object of a class that implements/extends AAdminTransactionsPreProxy
 *
 * @author S. Mesaretzidis
 * @since 8/1/2017.
 */
public class AdminTransactionsFactory implements IAdminTransactionsFactory {

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class AAdminTransactions.
     *
     * @param username         The username to be used.
     * @param password         The password to be used.
     * @param connectionString The password to be used.
     * @return An object of a concrete class implementation of the abstract class AAdminTransactions.
     * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions
     */
    @Override
    public AAdminTransactions getObject(String username, String password, String connectionString) {
        ICommonUserTransactionsFactory iCommonUserTransactionsFactory = new CommonUserTransactionsFactory();
        ACommonUserTransactions aCommonUserTransactions = iCommonUserTransactionsFactory.getObject(username, password, connectionString);

        AAdminTransactionsPreProxy aAdminTransactionsPreProxy = new AdminTransactionsPreProxy();
        //Builder pattern.
        aAdminTransactionsPreProxy.setCommonUserTransactions(aCommonUserTransactions);

        AAdminTransactions aAdminTransactions = new AdminTransactions();
        aAdminTransactions.setaAdminTransactionsPreProxy(aAdminTransactionsPreProxy);

        return aAdminTransactions;
    }
}
