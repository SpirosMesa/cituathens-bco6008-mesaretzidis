package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions.ICommonUserTransactionsBuilder;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions.ICommonUserTransactionsFactory;

/**
 * Factory class used to get an object of a class that implements the abstract class ACommonUserTransactions.
 *
 * @author S. Mesaretzidis
 * @since 9/1/2017.
 */
public class CommonUserTransactionsFactory implements ICommonUserTransactionsFactory {

    /**
     * Method that will be used in order to get an object of a class that implements ACommonUserTransactions.
     *
     * @param username         The username to be set.
     * @param password         The password to be set.
     * @param connectionString The connection string to be set.
     * @return An object of a class that implements the abstract class ACommonUserTransactions.
     */
    @Override
    public ACommonUserTransactions getObject(String username, String password, String connectionString) {

        ICommonUserTransactionsBuilder commonUserTransactionsBuilder = new CommonUserTransactionsBuilder();

        return commonUserTransactionsBuilder.getObject(username, password, connectionString);
    }
}
