package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.DatabaseConnections;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ADatabaseConnection;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.IDatabaseFactory;

/**
 * Factory used to get the a appropriate ADatabaseConnection object.
 *
 * @author S. Mesaretzidis
 * @since 2/1/2017.
 * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ADatabaseConnection
 */
public class MySqlDatabaseConnectionFactory implements IDatabaseFactory {

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class ACommonUserTransactionsProxy.
     *
     * @param username         The username that is be used.
     * @param password         The password that is to be used.
     * @param connectionString The connection string that is be used.
     * @return An object of a class that extends the abstract class ADatabaseConnection.
     */
    @Override
    public ADatabaseConnection getObject(String username, String password, String connectionString) {
        MySqlDatabaseConnection mySqlDatabaseConnection = new MySqlDatabaseConnection(username, password, connectionString);
        return mySqlDatabaseConnection;
    }
}
