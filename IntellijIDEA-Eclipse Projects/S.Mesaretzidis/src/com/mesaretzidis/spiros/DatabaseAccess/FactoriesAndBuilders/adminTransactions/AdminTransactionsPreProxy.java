package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.DbUser;
import com.mesaretzidis.spiros.Entities.WatsonResults;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Concrete class implementation of the abstract class AAdminTransactionsPreProxy
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy
 */
class AdminTransactionsPreProxy extends AAdminTransactionsPreProxy {

    /**
     * Class constructor receiving as input parameter an object of a class that implements ACommonUserTransactions.
     *
     * @param commonUserTransactions An object of a class that extends ACommonUserTransactions.
     */
    public AdminTransactionsPreProxy(ACommonUserTransactions commonUserTransactions) {
        super(commonUserTransactions);
    }

    /**
     * Default class constructor.
     */
    public AdminTransactionsPreProxy(){}

    /**
     * Method used to remove db user.
     * @param username The username of the user to be removed.
     */
    @Override
    public void dropDbUser(String username) throws SQLException {
        Connection connection = DriverManager.getConnection(commonUserTransactions.getConnectionString(), commonUserTransactions.getUsername(),
                commonUserTransactions.getPassword());

        Statement stmt = connection.createStatement();

        stmt.executeUpdate("drop user '" + username +"'@localhost");

        connection.close();
    }

    /**
     * Method used to add db user.
     * @param usr Db user's fn.
     * @param psw Db user's psw.
     */
    @Override
    public void addUser(String usr, String psw) throws SQLException {
        Connection connection = DriverManager.getConnection(commonUserTransactions.getConnectionString(), commonUserTransactions.getUsername(),
                commonUserTransactions.getPassword());

        Statement stmt = connection.createStatement();

        stmt.executeUpdate("CREATE User '" + usr +"'@localhost identified by '" + "'");

        stmt.executeUpdate("grant select, insert, update on bco_6008_tests.* to '" + usr + "'@localhost;");

        connection.close();
    }

    /**
     * Method used to retrieve all db users.
     * @return A list implementation containing information on all db users.
     * @see com.mesaretzidis.spiros.Entities.DbUser
     */
    @Override
    public List<DbUser> getAllUsers() throws SQLException {
        List<DbUser> dbUserLst = new LinkedList<>();

        Connection connection = DriverManager.getConnection(commonUserTransactions.getConnectionString(), commonUserTransactions.getUsername(),
                commonUserTransactions.getPassword());

        Statement stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery("SELECT  * from mysql.user");

        DbUser dbUsr;

        while (rs.next()){
            dbUsr = new DbUser(rs.getString(2));
            dbUserLst.add(dbUsr);
        }

        rs.close();
        stmt.close();

        connection.close();

        return dbUserLst;
    }

    /**
     * Connection string field getter.
     *
     * @return The set connection string.
     */
    @Override
    public String getConnectionString() {
        return commonUserTransactions.getConnectionString();
    }

    /**
     * Connection String field setter.
     *
     * @param connectionString
     */
    @Override
    public void setConnectionString(String connectionString) {
        commonUserTransactions.setConnectionString(connectionString);
    }

    /**
     * Password field getter.
     *
     * @return String containing the set password.
     */
    @Override
    public String getPassword() {
        return commonUserTransactions.getPassword();
    }

    /**
     * password field setter.
     *
     * @param password The password to be set.
     */
    @Override
    public void setPassword(String password) {
        commonUserTransactions.setPassword(password);
    }

    /**
     * Username field getter.
     *
     * @return The set password.
     */
    @Override
    public String getUsername() {
        return commonUserTransactions.getUsername();
    }

    /**
     * Username field setter.
     *
     * @param username
     */
    @Override
    public void setUsername(String username) {
        commonUserTransactions.setUsername(username);
    }

    /**
     * Method used to retrieve all application clients.
     *
     * @return A List implementation used to retrieve all application clients.
     * @see com.mesaretzidis.spiros.Entities.Client
     * @see java.util.List
     */
    @Override
    public List<Client> getAllClients() throws SQLException {
        return commonUserTransactions.getAllClients();
    }

    /**
     * Method used in order to retrieve a client by his/her id.
     *
     * @param id The client's id.
     * @return A Client object, containing the necessary information.
     * @see com.mesaretzidis.spiros.Entities.Client
     */
    @Override
    public Client getClientById(int id) throws SQLException {
        return commonUserTransactions.getClientById(id);
    }

    /**
     * Method used to retrieve a clients WatsonResults
     *
     * @param clientId
     * @return A List implementation containing WatsonResults objects, which in turn contain the client's watson results.
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     * @see java.util.List
     */
    @Override
    public List<WatsonResults> getClientWatsonResults(int clientId) throws SQLException {
        return commonUserTransactions.getClientWatsonResults(clientId);
    }

    /**
     * Method used in order to enter a new record in the database.
     *
     * @param clientId   the client's id.
     * @param clientText the input text
     * @param wekaResults the weka results tree
     * @param watsonResults    the watson results.
     */
    @Override
    public int insertRecord(int clientId, String clientText, String wekaResults, WatsonResults watsonResults) throws SQLException, IOException {
        return commonUserTransactions.insertRecord(clientId, clientText, wekaResults, watsonResults);
    }

    /**
     * Method used to add a new client
     *
     * @param fn The client's first name.
     * @param ln The client's last name.
     * @return The id of the new client's record.
     */
    @Override
    public int addNewClient(String fn, String ln) throws SQLException {
        return commonUserTransactions.addNewClient(fn, ln);
    }

    /**
     * Method used in order to get a client's stored text.
     *
     * @param clientId The client's id.
     * @return A String containing a
     */
    public String[] getClientTexts(int clientId) throws SQLException {
        return commonUserTransactions.getClientTexts(clientId);
    }

    /**
     * Method used in order to get a client's stored Weka results tree.
     *
     * @param clientId The client's id.
     * @return
     */
    public String[] getWekaResults(int clientId) throws IOException, SQLException {
        return commonUserTransactions.getWekaResults(clientId);
    }
}
