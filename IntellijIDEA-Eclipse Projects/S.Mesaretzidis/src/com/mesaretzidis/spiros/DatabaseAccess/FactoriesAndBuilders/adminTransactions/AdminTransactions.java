package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactionsPreProxy;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.DbUser;
import com.mesaretzidis.spiros.Entities.WatsonResults;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Concrete implementation of the abstract class AAdminTransactions
 * @author S. Mearetzidis
 * @since 31/12/2016.
 * @see com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions
 */
class AdminTransactions extends AAdminTransactions {

    /**
     * Class constructor receiving as input parameter an object of a class that implements AAdminTransactions.
     *
     * @param aAdminTransactionsPreProxy An object of a class that extends AAdminTransactions.
     */
    public AdminTransactions(AAdminTransactionsPreProxy aAdminTransactionsPreProxy) {
        super(aAdminTransactionsPreProxy);
    }

    /**
     * Default class constructor.
     */
    public AdminTransactions(){}

    /**
     * Method used to remove db user.
     *
     * @param username The username of the db user to be removed.
     */
    @Override
    public void dropDbUser(String username) throws SQLException {
        aAdminTransactionsPreProxy.dropDbUser(username);
    }

    /**
     * Method used to add db user.
     * @param usr Db user's fn.
     * @param psw Db user's psw.
     */
    @Override
    public void addUser(String usr, String psw) throws SQLException {
        aAdminTransactionsPreProxy.addUser(usr, psw);
    }

    /**
     * Method used to retrieve all db users.
     *
     * @return A list implementation containing information on all db users.
     */
    @Override
    public List<DbUser> getAllUsers() throws SQLException {
        return aAdminTransactionsPreProxy.getAllUsers();
    }

    /**
     * Connection string field getter.
     *
     * @return The set connection string.
     */
    @Override
    public String getConnectionString() {
        return null;
    }

    /**
     * Connection String field setter.
     *
     * @param connectionString The connection string to be set.
     */
    @Override
    public void setConnectionString(String connectionString) {

    }

    /**
     * Password field getter.
     *
     * @return String containing the set password.
     */
    @Override
    public String getPassword() {
        return null;
    }

    /**
     * password field setter.
     *
     * @param password The password to be set.
     */
    @Override
    public void setPassword(String password) {

    }

    /**
     * Username field getter.
     *
     * @return The set password.
     */
    @Override
    public String getUsername() {
        return null;
    }

    /**
     * Username field setter.
     *
     * @param username
     */
    @Override
    public void setUsername(String username) {

    }

    /**
     * Method used to retrieve all application clients.
     *
     * @return A List implementation used to retrieve all application clients.
     * @see com.mesaretzidis.spiros.Entities.Client
     * @see java.util.List
     */
    @Override
    public List<Client> getAllClients() throws SQLException {
        return aAdminTransactionsPreProxy.getAllClients();
    }

    /**
     * Method used in order to retrieve a client by his/her id.
     *
     * @param id The client's id.
     * @return A Client object, containing the necessary information.
     * @see com.mesaretzidis.spiros.Entities.Client
     */
    @Override
    public Client getClientById(int id) throws SQLException {
        return aAdminTransactionsPreProxy.getClientById(id);
    }

    /**
     * Method used to retrieve a clients WatsonResults
     *
     * @param clientId
     * @return A List implementation containing WatsonResults objects, which in turn contain the client's watson results.
     * @see com.mesaretzidis.spiros.Entities.WatsonResults
     * @see java.util.List
     */
    @Override
    public List<WatsonResults> getClientWatsonResults(int clientId) throws SQLException {
        return aAdminTransactionsPreProxy.getClientWatsonResults(clientId);
    }

    /**
     * Method used in order to enter a new record in the database.
     *
     * @param clientId   the client's id.
     * @param clientText the input text
     * @param wekaResults the weka results tree
     * @param watsonResults    the watson results.
     */
    @Override
    public int insertRecord(int clientId, String clientText, String wekaResults, WatsonResults watsonResults) throws SQLException, IOException {
        return aAdminTransactionsPreProxy.insertRecord(clientId, clientText, wekaResults, watsonResults);
    }

    /**
     * Method used to add a new client
     *
     * @param fn The client's first name.
     * @param ln The client's last name.
     * @return The id of the new client's record.
     */
    @Override
    public int addNewClient(String fn, String ln) throws SQLException {
        return aAdminTransactionsPreProxy.addNewClient(fn, ln);
    }

    /**
     * Method used in order to get a client's stored texts.
     *
     * @param clientId The client's id.
     * @return A String[] containing the client's texts.
     */
    @Override
    public String[] getClientTexts(int clientId) throws SQLException {
        return aAdminTransactionsPreProxy.getClientTexts(clientId);
    }

    /**
     * Method used in order to get a client's stored Weka results tree.
     *
     * @param clientId The client's id.
     * @return A String array containing the client's Weka results.
     */
    @Override
    public String[] getWekaResults(int clientId) throws SQLException, IOException {
        return aAdminTransactionsPreProxy.getWekaResults(clientId);
    }

    /**
     * Field setter, used to implement the Builder design pattern.
     *
     * @param adminTransactionsPreProxy An object of a class that implements AAdminTransactionsPreProxy.
     */
    public void setPreProxy(AAdminTransactionsPreProxy adminTransactionsPreProxy) {
        this.aAdminTransactionsPreProxy = adminTransactionsPreProxy;
    }
}

