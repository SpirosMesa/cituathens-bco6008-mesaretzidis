package com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.Interfaces.commonUserTransactions.ICommonUserTransactionsBuilder;

/**
 * Class used to implement the Builder design pattern.
 *
 * @author S. Mesaretzidis
 * @since 9/1/2017.
 */
class CommonUserTransactionsBuilder implements ICommonUserTransactionsBuilder{

    /**
     * Method definition, used to return an instance of a concrete class implementation of
     * the abstract class ACommonUserTransactionsPreProxy.
     *
     * @param username         The username to be set.
     * @param password         The password to be set.
     * @param connectionString The connection string to be set.
     * @return An object that implements the abstract class ACommonUserTransactions.
     */
    @Override
    public ACommonUserTransactions getObject(String username, String password, String connectionString) {

        CommonUserTransactionsPreProxy commonUserTransactionsPreProxy = new CommonUserTransactionsPreProxy(username, password, connectionString);

        CommonUserTransactions commonUserTransactions = new CommonUserTransactions();

        //Builder design pattern.
        commonUserTransactions.setPreProxyObject(commonUserTransactionsPreProxy);

        return commonUserTransactions;
    }
}
