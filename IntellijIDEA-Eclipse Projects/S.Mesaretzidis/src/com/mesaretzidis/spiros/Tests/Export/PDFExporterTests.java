package com.mesaretzidis.spiros.Tests.Export;


import com.ibm.watson.developer_cloud.personality_insights.v3.PersonalityInsights;
import com.itextpdf.text.DocumentException;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Exceptions.ExporterNotSupportedException;
import com.mesaretzidis.spiros.Exceptions.FilePathNotSetException;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import com.mesaretzidis.spiros.Export.Implementations.ExporterFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.swing.filechooser.FileSystemView;
import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class PDFExporterTests {
    WatsonResults watsonResults;
    String wekaTree = "this is a tree", filePath;
    Boolean log = false;
    Client client = new Client(1, "new", "client");
    String text = "You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day" +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted. " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection." +
            " Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy" +
            " sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay." +
            " Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            " form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            "thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            "long fat cold saw neat. Put boy carried chiefly shy general. You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way  " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day " +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new  " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished.  " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.  " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose  " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir  " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end  " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me  " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden  " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. " +
            "Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy " +
            "sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay. " +
            "Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            "form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            " thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            " long fat cold saw neat. Put boy carried chiefly shy general. "+
            "You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day" +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted. " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection." +
            " Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy" +
            " sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay." +
            " Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            " form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            "thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            "long fat cold saw neat. Put boy carried chiefly shy general. You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way  " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day " +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new  " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished.  " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.  " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose  " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir  " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end  " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me  " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden  " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. " +
            "Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy " +
            "sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay. " +
            "Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            "form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            " thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            " long fat cold saw neat. Put boy carried chiefly shy general. " +
            "You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day" +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted. " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection." +
            " Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy" +
            " sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay." +
            " Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            " form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            "thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            "long fat cold saw neat. Put boy carried chiefly shy general. You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way  " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day " +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new  " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished.  " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.  " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose  " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir  " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end  " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me  " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden  " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. " +
            "Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy " +
            "sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay. " +
            "Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            "form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            " thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            " long fat cold saw neat. Put boy carried chiefly shy general. " +
            "You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day" +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted. " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection." +
            " Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy" +
            " sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay." +
            " Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            " form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            "thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            "long fat cold saw neat. Put boy carried chiefly shy general. You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way  " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day " +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new  " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished.  " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.  " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose  " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir  " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end  " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me  " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden  " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. " +
            "Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy " +
            "sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay. " +
            "Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            "form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            " thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            " long fat cold saw neat. Put boy carried chiefly shy general. " +
            "You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day" +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted. " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection." +
            " Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy" +
            " sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay." +
            " Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            " form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            "thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            "long fat cold saw neat. Put boy carried chiefly shy general. You vexed shy mirth now noise. Talked him people valley add use her depend letter. Allowance too applauded now way  " +
            "something recommend. Mrs age men and trees jokes fancy. Gay pretended engrossed eagerness continued ten. Admitting day " +
            " him contained unfeeling attention mrs out. Oh acceptance apartments up sympathize astonished delightful. Waiting him new  " +
            "lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished.  " +
            "What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.  " +
            "Up maids me an ample stood given. Certainty say suffering his him collected intention promotion. Hill sold ham men made lose  " +
            "case. Views abode law heard jokes too. Was are delightful solicitude discovered collecting man day. Resolving neglected sir  " +
            "tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting. Breakfast procuring nay end  " +
            "happiness allowance assurance frankness. Met simplicity nor difficulty unreserved who. Entreaties mr conviction dissimilar me  " +
            "astonished estimating cultivated. On no applauded exquisite my additions. Pronounce add boy estimable nay suspected. You sudden  " +
            "nay elinor thirty esteem temper. Quiet leave shy you gay off asked large style. Tiled say decay spoil now walls meant house. My mr " +
            "interest thoughts screened of outweigh removing. Evening society musical besides inhabit ye my. Lose hill well up will he over on. " +
            "Increasing sufficient everything men him admiration unpleasing sex. Around really his use uneasy longer him man. His our pulled nature " +
            "elinor talked now for excuse result. Admitted add peculiar get joy doubtful. Kept in sent gave feel will oh it we. Has pleasure procured " +
            "men laughing shutters nay. Old insipidity motionless continuing law shy partiality. Depending acuteness dependent eat use dejection. " +
            "Unpleasing astonished discovered not nor shy. Morning hearted now met yet beloved evening. Has and upon his last here must. Gave read " +
            "use way make spot how nor. In daughter goodness an likewise oh consider at procured wandered. Songs words wrong by me hills heard timed. " +
            "Happy eat may doors songs. Be ignorant so of suitable dissuade weddings together. Least whole timed we is. An smallness deficient " +
            "discourse do newspaper be an eagerness continued. Mr my ready guest ye after short at. Ignorant branched humanity led now marianne too " +
            "strongly entrance. Rose to shew bore no ye of paid rent form. Old design are dinner better nearer silent excuse. She which are maids boy " +
            "sense her shade. Considered reasonable we affronting on expression in. So cordial anxious mr delight. Shot his has must wish from sell nay. " +
            "Remark fat set why are sudden depend change entire wanted. Performed remainder attending led fat residence far. Remember outweigh do he desirous " +
            "no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced " +
            "distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty " +
            "two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near." +
            "Had strictly mrs handsome mistaken cheerful. We it so if resolution invitation remarkably unpleasant conviction. As into ye then" +
            "form. To easy five less if rose were. Now set offended own out required entirely. Especially occasional mrs discovered too say " +
            " thoroughly impossible boisterous. My head when real no he high rich at with. After so power of young as. Bore year does has get " +
            " long fat cold saw neat. Put boy carried chiefly shy general. ";


    @Before
    public void setUpWatsonAndFilePath() {
        //Generating WatsonResults Object.
        PersonalityInsights pi = new PersonalityInsights(LocalDateTime.now().toString());

        pi.setUsernameAndPassword("428e2476-d441-43b0-bdd4-44390ad2ca59", "5h8FRC3BeYDU");

        watsonResults = new WatsonResults(pi.getProfile(text).execute());
        //---------------------------------------------------------------------------------
        //Getting file path.
        File[] paths;

        FileSystemView fileSystemView = FileSystemView.getFileSystemView();

        paths = fileSystemView.getRoots();

        filePath = paths[0].getPath() + "\\exporter.pdf";

        System.out.println("filePath: " +filePath);
    }

    @Test
    public void exportFileWOLgTest() {
        AExporter pdfExporter = null;

        try {
            pdfExporter = new ExporterFactory().getExporter(ExporterFactory.PDFExporter, watsonResults, wekaTree, filePath, false, client);
        } catch (ExporterNotSupportedException e) {
            e.printStackTrace();
        }

        try {
            pdfExporter.exportFile();
        } catch (FilePathNotSetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (com.mesaretzidis.spiros.Exceptions.DocumentException e) {
            e.printStackTrace();
        }

        File file = new File(filePath);

        assert(file.exists());
    }

    //To be run only after the existence and proper contents of the file have been confirmed.
    @After
    public void deleteFile(){
        File file = new File(filePath);

        file.delete();
    }
}
