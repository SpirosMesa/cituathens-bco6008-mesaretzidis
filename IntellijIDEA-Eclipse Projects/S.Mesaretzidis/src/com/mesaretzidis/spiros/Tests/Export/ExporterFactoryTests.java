package com.mesaretzidis.spiros.Tests.Export;

import com.mesaretzidis.spiros.Exceptions.ExporterNotSupportedException;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import com.mesaretzidis.spiros.Export.Implementations.ExporterFactory;
import org.junit.Test;

public class ExporterFactoryTests {
    String filePath = "C:\\";

    @Test
    public void pdfExporterTest(){
        AExporter pdfExporter = null;

        try {
            pdfExporter = new ExporterFactory().getExporter(ExporterFactory.PDFExporter);
        } catch (ExporterNotSupportedException e) {
            e.printStackTrace();
        }

        assert(pdfExporter.getClass().getSimpleName().equals("PDFExporter"));
    }

    @Test
    public void txtExporterTest() {
        AExporter txtExporter = null;

        try {
            txtExporter = new ExporterFactory().getExporter(ExporterFactory.TXTExporter);
        } catch (ExporterNotSupportedException e) {
            e.printStackTrace();
        }

        assert(txtExporter.getClass().getSimpleName().equals("TXTExporter"));
    }

    @Test
    public void xmlExporterTest() {
        AExporter xmlExporter = null;

        try {
            xmlExporter = new ExporterFactory().getExporter(ExporterFactory.XMLExporter);
        } catch (ExporterNotSupportedException e) {
            e.printStackTrace();
        }

        assert(xmlExporter.getClass().getSimpleName().equals("XMLExporter"));
    }
}
