package com.mesaretzidis.spiros.Tests.DatabaseAccess.Factories;


import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.AAdminTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.adminTransactions.AdminTransactionsFactory;
import com.mesaretzidis.spiros.Entities.DbUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.util.List;

public class AdminTransactionsTest {
    private boolean test_database_exists = false;
    private Connection connection;
    private String connString = null;

    @Before
    public void TestConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/?useSSL=false", "root", "root");
        } catch (SQLException e) {
            System.out.println("error code is: " + e.getErrorCode());
            System.out.println("message is" + e.getMessage());
            e.printStackTrace();
        }

        Statement stmt = null;

        if (connection != null) {
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else assert(false);

        ResultSet rs = null;

        if (stmt != null) {
            try {
                rs = stmt.executeQuery("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'bco_6008_tests'");
                rs.next();
                if(!rs.getNString(1).equals(null)) {
                    connString = "jdbc:mysql://localhost:3306/bco_6008_tests?useSSL=false";
                }
                else {
                    connString = "jdbc:mysql://localhost:3306/bco_6008?useSSL=false";
                }

                connection = DriverManager.getConnection(connString, "root", "root");
                rs.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else assert(false);
    }

    @Test
    public void adminTrasactionsFactoryTest() {
        AAdminTransactions aAdminTransactions = new AdminTransactionsFactory().getObject("root", "root", connString);

        String name = aAdminTransactions.getClass().getSimpleName();

        assert(name.equals("AdminTransactions"));
    }

    @Test
    public void getAllUsers() {
        AAdminTransactions aAdminTransactions = new AdminTransactionsFactory().getObject("root", "root", connString);

        //Getting the list by the method.
        List<DbUser> methLst = null;
        try {
            methLst = aAdminTransactions.getAllUsers();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Getting the count of users.

        PreparedStatement preStmt = null;
        ResultSet rs = null;
        int count = -2;

        try {
            preStmt = connection.prepareStatement("Select count(*) from mysql.user");
            rs = preStmt.executeQuery();
            rs.next();
            count = rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert(methLst.size() == count);
    }

    @After
    public void afterTests(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
