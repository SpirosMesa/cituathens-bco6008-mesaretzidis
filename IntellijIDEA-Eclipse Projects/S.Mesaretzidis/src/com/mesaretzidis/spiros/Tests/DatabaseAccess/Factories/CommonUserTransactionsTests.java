package com.mesaretzidis.spiros.Tests.DatabaseAccess.Factories;

import com.mesaretzidis.spiros.DatabaseAccess.AbstractClasses.ACommonUserTransactions;
import com.mesaretzidis.spiros.DatabaseAccess.FactoriesAndBuilders.commonUserTransactions.CommonUserTransactionsFactory;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Class used in order to test the functions of the CommonUserTransactions class, as well as the corresponding factory.
 *
 * @since 1/2/2017
 * @author S. Mesaretzidis
 */
public class CommonUserTransactionsTests {
    private boolean test_database_exists = false;
    private Connection connection;
    private String connString = null;

    @Before
    public void TestConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/?useSSL=false", "root", "root");
        } catch (SQLException e) {
            System.out.println("error code is: " + e.getErrorCode());
            System.out.println("message is" + e.getMessage());
            e.printStackTrace();
        }

        Statement stmt = null;

        if (connection != null) {
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else assert(false);

        ResultSet rs = null;

        if (stmt != null) {
            try {
                rs = stmt.executeQuery("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'bco_6008_tests'");
                rs.next();
                if(!rs.getNString(1).equals(null)) {
                    connString = "jdbc:mysql://localhost:3306/bco_6008_tests?useSSL=false";
                }
                else {
                    connString = "jdbc:mysql://localhost:3306/bco_6008?useSSL=false";
                }

                connection = DriverManager.getConnection(connString, "testUser", "test");
                rs.close();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else assert(false);
    }


    @Test
    public void commonUserTransactionsFactoryTest(){

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        String name = aCommonUserTransactions.getClass().getSimpleName();
        if (name.equals("CommonUserTransactions")){
            System.out.println(name);
            assert(true);
        }
        else {
            System.out.println(name);
            assert(false);
        }
    }

    @Test
    public void getAllClientsTest() throws Exception {

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        //Getting the count of clients, old fashioned way.
        int clientCount = 0;

        Statement stmt = null;

        try {
            stmt = connection.createStatement();


        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("message is: " + e.getMessage());
            System.out.println("state is: " + e.getSQLState());
        }

        ResultSet rs =null;

        try {

            rs = stmt.executeQuery("select count(*) from app_client");

            rs.next();
            clientCount = rs.getInt(1);
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //Getting Clients via method

        List<Client> clientLst = new LinkedList<Client>();

        try {
            clientLst = aCommonUserTransactions.getAllClients();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("message is: " + e.getMessage());
            System.out.println("state is: " + e.getSQLState());
        }

        System.out.println("client count is: " + clientCount);
        System.out.println("clientLst is: " + clientLst.size());
        assert(clientCount == clientLst.size());
    }

    @Test
    public void getClientTextsTest() {

        //Getting client texts
        Statement stmt = null;
        String text = null, query = "Select client_text from client_texts where fk_app_client_id = 1";
        ResultSet rs;
        int arraySize;
        String[] clientTextsArray = null;

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            //Getting the number of rows, in order to use them while initializing the array.
            rs.last();
            arraySize =  rs.getRow();
            rs.beforeFirst();

            clientTextsArray = new String[arraySize];

            //Will be used in order to stored the current array index within the while loop.
            arraySize = 0;

            while(rs.next()) {
                text = rs.getString(1);
                clientTextsArray[arraySize] = text;
                arraySize = 0;
            }


        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("message is: " + e.getMessage());
            System.out.println("state is: " + e.getSQLState());
        }

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        String[] methodRetrievedTexts = null;

        try {
            methodRetrievedTexts = aCommonUserTransactions.getClientTexts(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(clientTextsArray.length != methodRetrievedTexts.length){
            assert(false);
        }

        //Recycling again.
        arraySize = 0;

        for(String cText : clientTextsArray) {
            if (!clientTextsArray[arraySize].equals(clientTextsArray[arraySize])){
                assert(false);
            }
        }
        assert(true);
    }

    @Test
    public void getWekaResultsTest() {
        //Getting client Weka Results
        Statement stmt = null;
        String text = null, query = "Select client_text from client_texts where fk_app_client_id = 1";
        ResultSet rs;
        int arraySize;
        String[] clientWekaArray = null;

        try {
            stmt = connection.createStatement();
            rs = stmt.executeQuery(query);

            //Getting the number of rows, in order to use them while initializing the array.
            rs.last();
            arraySize =  rs.getRow();
            rs.beforeFirst();

            clientWekaArray = new String[arraySize];

            //Will be used in order to stored the current array index within the while loop.
            arraySize = 0;

            while(rs.next()) {
                text = rs.getString(1);
                clientWekaArray[arraySize] = text;
                arraySize = 0;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("message is: " + e.getMessage());
            System.out.println("state is: " + e.getSQLState());
        }

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        String[] methodRetrievedWeka = null;

        try {
            methodRetrievedWeka = aCommonUserTransactions.getWekaResults(1);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(clientWekaArray.length != methodRetrievedWeka.length){
            assert(false);
        }

        //Recycling again.
        arraySize = 0;

        for(String cText : clientWekaArray) {
            if (!clientWekaArray[arraySize].equals(clientWekaArray[arraySize])){
                assert(false);
            }
        }
        assert(true);
    }

    @Test
    public void getClientByIdTest() {
        //Get client without method
        PreparedStatement preStmt = null;
        ResultSet rs = null;
        Client tradClient = null, methClient = null;


        try {
            preStmt = connection.prepareStatement("Select * from app_client where active = 1 and id = 1");
            rs = preStmt.executeQuery();
            rs.next();

            tradClient = new Client(rs.getInt(1), rs.getString(2), rs.getString(3));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        try {
            methClient = aCommonUserTransactions.getClientById(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        assert(methClient.equals(tradClient));
    }

    /*@Test
    public void getClientWatsonResultsTest() {
        //Getting client with JDBC.
        List<WatsonResults> watsonResultsTrad = null;

        PreparedStatement preStmt;
        ResultSet rs = null;
        int tradCount = -2;

        try {
            preStmt = connection.prepareStatement("SELECT Count(*) from client_texts where fk_app_client_id = ?");
            preStmt.setInt(1, 1);

            rs = preStmt.executeQuery();
            rs.next();
            tradCount = rs.getInt(1);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        try {
            watsonResultsTrad = aCommonUserTransactions.getClientWatsonResults(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (tradCount != watsonResultsTrad.size()) assert(false);

        for(WatsonResults wResults : watsonResultsTrad) {
            if(wResults.equals(null)) assert(false);
        }

        assert(true);
    }*/

    @Test
    public void addNewClientTest() {
        //Inserting new client
        int clientId = -2;

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        try {
            clientId = aCommonUserTransactions.addNewClient("New", "Client");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        PreparedStatement preStmt;
        ResultSet rs = null;


        try {
            preStmt = connection.prepareStatement("SELECT Count(*) FROM app_client\n" +
                    "ORDER BY id DESC\n" +
                    "LIMIT 1");
            rs = preStmt.executeQuery();
            rs.next();

            if (rs.getInt(1) == clientId) assert(true);
            else assert(false);

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Test
    public void insertRecord(){
        WatsonResults wRs = new WatsonResults();
        int clientId = -2, record = -2, tradRecord = -2;

        ACommonUserTransactions aCommonUserTransactions = new CommonUserTransactionsFactory().getObject("testUser", "test", connString);

        try {
            clientId = aCommonUserTransactions.addNewClient("new", "client");

            record = aCommonUserTransactions.insertRecord(clientId, "bli", "bloo", wRs);

            PreparedStatement preStmt = connection.prepareStatement("Select last_insert_id() from client_texts");

            ResultSet rs = preStmt.executeQuery();
            rs.next();
            tradRecord = rs.getInt(1);

            if (record == tradRecord) assert(true);
            else assert(false);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void afterTests(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
