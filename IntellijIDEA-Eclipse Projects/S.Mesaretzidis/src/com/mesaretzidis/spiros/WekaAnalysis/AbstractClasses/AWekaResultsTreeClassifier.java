package com.mesaretzidis.spiros.WekaAnalysis.AbstractClasses;

import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Exceptions.InheritanceException;
import com.mesaretzidis.spiros.Exceptions.UnimplementedInterfaceException;
import com.mesaretzidis.spiros.WekaAnalysis.Interfaces.IWekaResultsTreeClassifier;
import weka.classifiers.AbstractClassifier;
import weka.core.Drawable;
import weka.core.OptionHandler;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Abstract class used in order to define a constructor for the solid class implementation.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public abstract class AWekaResultsTreeClassifier implements IWekaResultsTreeClassifier {
    protected WatsonResults wResults;
    protected Boolean log = false;
    protected String[] options;
    protected String filePath;

    /**
     * Default constructor receiving all possible class parameters.
     *
     * @param wResults The WatsonResults object that is to be set.
     * @param log A boolean value indicating whether logging functionality should be set.
     * @param filePath A String value indicating the file path of the trained model.
     * @param options A String array, used by the tree classifier.
     */
    public AWekaResultsTreeClassifier(WatsonResults wResults, Boolean log, String filePath, String[] options) {
        this.wResults = wResults;
        this.log = log;
        this.filePath = filePath;
        this.options = options;
    }

    /**
     * Method used in order to retrieve the trained model.
     *
     * @return  A Classifier object that implements the required interface.
     * @throws IOException Thrown when the model file has not been found.
     * @throws ClassNotFoundException Thrown when de-serialization of the model file failed.
     * @throws UnimplementedInterfaceException Thrown when the provided model, does not implement the required interfaces.
     * @throws InheritanceException Thrown when the provided model does not inherit from the required super class.
     */
    public <T extends AbstractClassifier & OptionHandler & Drawable> T getClassifier() throws IOException,
            ClassNotFoundException, UnimplementedInterfaceException, InheritanceException {
        //Deserialize the object.
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath));
        Object obj = ois.readObject();
        ois.close();

        //Used to check if it implements the required interfaces and the required super classes.
        boolean implentsInterAndClass = false;

        //Checking to see if the provided classifier implements the OptionsHandler interface
        Class<?>[] classArr = obj.getClass().getInterfaces();

        String implemented_interfaces = "";
        for (Class<?> e : classArr) {
            implemented_interfaces += e.getCanonicalName();
        }

        if (!implemented_interfaces.contains("weka.core.OptionHandler")) throw new UnimplementedInterfaceException("The required interface " +
                "OptionHandler is not implemented");
        else if (!implemented_interfaces.contains("weka.core.Drawable")) throw new UnimplementedInterfaceException("The required interface" +
                "Drawable is not implemented.");

        //Checking to see if it implements the required superclass.
        if (!obj.getClass().getSuperclass().getCanonicalName().contains("AbstractClassifier")) throw new InheritanceException("The super class " +
                "AbstractClassifier is not implemented");

        //After all the tests have been passed we can cast the object.
        T cls = (T) obj;

        return cls;
    }
}
