package com.mesaretzidis.spiros.WekaAnalysis.Interfaces;

/**
 * Interface that defines a method used to retrieve the results tree
 * provided by the appropriate Weka classifier.
 *
 * @author S. Mesaretzidis
 * @since 30/12/2016
 */
public interface IWekaResultsTreeClassifier extends IWekaResultsBase {

    /**
     * Method used to retrieve the results tree provided by the chosen
     * classifier.
     * @return
     * @throws Exception
     */
    public String getResultsTree() throws Exception;
}
