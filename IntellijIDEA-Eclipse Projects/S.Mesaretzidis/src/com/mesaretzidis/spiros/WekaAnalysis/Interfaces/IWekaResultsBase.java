package com.mesaretzidis.spiros.WekaAnalysis.Interfaces;

import java.io.FileNotFoundException;

/**
 * Interface used in order to define methods used
 * to set the logging operations, and set the model file.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public interface IWekaResultsBase {
    /**
     * Method used to set the logging operations.
     * @param log A boolean variable used to indicate whether logging should be enabled.
     */
    public void setLogging(Boolean log);

    /**
     * Method used to set the path of the model file.
     * @param filePath The file path of the model file.
     * @throws FileNotFoundException Thrown when the file cannot be located.
     */
    public void setModelFile(String filePath) throws FileNotFoundException;
}
