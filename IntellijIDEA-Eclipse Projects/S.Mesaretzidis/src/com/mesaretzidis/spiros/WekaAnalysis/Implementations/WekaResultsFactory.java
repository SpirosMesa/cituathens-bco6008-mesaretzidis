package com.mesaretzidis.spiros.WekaAnalysis.Implementations;

import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.WekaAnalysis.AbstractClasses.AWekaResultsTreeClassifier;

/**
 * The factory used in
 * @author S. Mesaretzidis
 * @since 30/10/2016
 */
public class WekaResultsFactory {
    /**
     * Factory used in order to return a required object.
     * @param wResults The required WatsonResults object.
     * @param log Boolean parameter indicating whether logging operations should be enabled.
     * @param filePath String variable used to set the file path.
     * @param options String array used to set the options for the classifier.
     * @return An object of a concrete class implementation of the abstract class AWekaResults
     */
    public AWekaResultsTreeClassifier getObject(WatsonResults wResults, Boolean log, String filePath, String[] options){
        return new WekaResultsTreeClassifierWJ48(wResults, log, filePath, options);
    }
}
