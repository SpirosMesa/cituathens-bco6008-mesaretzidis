package com.mesaretzidis.spiros.WekaAnalysis.Implementations;

import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.WekaAnalysis.AbstractClasses.AWekaResultsTreeClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.StringReader;

/**
 * Class used in order to get Weka's results, based solely on the Watson results as well as on the
 * trained model.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
class WekaResultsTreeClassifierWJ48 extends AWekaResultsTreeClassifier {
    private static String classifierName;

    /**
     * Default constructor receiving all possible class parameters.
     *
     * @param wResults The WatsonResults object that is to be set.
     * @param log A boolean value indicating whether logging functionality should be set.
     * @param filePath A String value indicating the file path of the trained model.
     * @param options A String array, used by the tree classifier.
     */
    public WekaResultsTreeClassifierWJ48(WatsonResults wResults, Boolean log, String filePath, String[] options) {
        super(wResults, log, filePath, options);
    }

    /**
     * Method used in order to retrieve the tree produced by the analysis completed by Weka.
     *
     * @return A String containing the results tree.
     * @throws Exception
     */
    public String getResultsTree() throws Exception {
        if (log == true) return getResultsTreeLg();

        else return getResultsTreeWOLg();
    }

    /**
     * Method used in order to set logging operations on or off.
     *
     * @param log A boolean variable used to indicate whether logging should be enabled.
     */
    public void setLogging(Boolean log) {
        this.log = log;
    }

    /**
     * filePath field setter
     *
     * @param filePath The file path of the model file.
     * @throws FileNotFoundException
     */
    public void setModelFile(String filePath) throws FileNotFoundException{
        File file = new File(filePath);
        if (file.exists() == false) throw new FileNotFoundException("The model file has not been found");
        this.filePath = filePath;
    }

    /**
     * Method used to retrive the weka decision tree, while the logging operations are enabled.
     *
     * @return A String containing the decision tree.
     * @throws Exception
     */
    private String getResultsTreeLg() throws Exception {return null;}

    /**
     * Method used to retrive the weka decision tree, while the logging operations are enabled.
     *
     * @return A String containing the decision tree.
     * @throws Exception
     */
    private String getResultsTreeWOLg() throws Exception {
        J48 j48 = getClassifier();

        j48.setOptions(options);

        Instances data = new Instances(new StringReader(wResults.getResultsForWeka()));

        j48.classifyInstance(data.get(0));

        return  j48.graph();
    }
}
