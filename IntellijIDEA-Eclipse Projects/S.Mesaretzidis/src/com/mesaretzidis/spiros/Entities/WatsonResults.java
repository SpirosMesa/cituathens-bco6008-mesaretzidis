package com.mesaretzidis.spiros.Entities;

import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait;


import java.util.LinkedList;
import java.util.List;

/**
 * Wrapper class, used in order to de-couple, the view from the model,
 * by not making the view use Watson's DTOs.
 * @author S.Mesaretzidis
 * @since 30/12/2016.
 */
public class WatsonResults {
    private String rslStr = null, warnings = null;

    List<ITrait> lstNativeNeeds = null, lstNativeValues = null,
            lstB5 = null;

    /**
     * Class constuctor that receives as argument a Watson Profile object.
     * @param prof
     */
    public WatsonResults(Profile prof) {
        Transfer tran = new Transfer(prof);
        this.warnings = prof.getWarnings().toString();
        lstNativeNeeds = tran.transferNeeds();
        lstNativeValues = tran.transferValues();
        lstB5 = tran.transferBig5();
    }

    /**
     * Empty class constructor
     */
    public WatsonResults(){}

    public String getWarning() {
        return  this.warnings;
    }
    public String getResultsForWeka() {
        return null;
    }

    /**
     * LsNativeNeeds property getter.
     *
     * @return A List implementation containing all the of traits that correspond to the user's needs.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public List<ITrait> getLstNativeNeeds() {
        return lstNativeNeeds;
    }

    /**
     * LsNativeNeeds property getter.
     *
     * @param lstNativeNeeds A List implementation of traits, used to set the class' lstNativeNeeds property.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public void setLstNativeNeeds(List<ITrait> lstNativeNeeds) {
        this.lstNativeNeeds = lstNativeNeeds;
    }
    /**
     * LstNativeValues property getter.
     *
     * @return A List implementation containing all the of traits that correspond to the user's values.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public List<ITrait> getLstNativeValues() {
        return lstNativeValues;
    }

    /**
     * lstNativeValues property setter.
     *
     * @param lstNativeValues A List implementation of traits, used to set the class' lstNativeValues property.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public void setLstNativeValues(List<ITrait> lstNativeValues) {
        this.lstNativeValues = lstNativeValues;
    }

    /**
     * lstB5 property getter.
     *
     * @return A List implementation containing all the of traits that correspond to the user's Big Five facets and traits.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public List<ITrait> getLstB5() {
        return lstB5;
    }

    /**
     * lstB5 property getter.
     *
     * @return A List implementation containing all the of traits that correspond to the user's Big Five facets and traits.
     * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
     */
    public void setLstB5(List<ITrait> lstB5) {
        this.lstB5 = lstB5;
    }

    @Override
    public String toString() {
        String nNeedsStr = lstNativeNeeds.toString(), nValues = lstNativeValues.toString(),nB5 = lstB5.toString();

        nNeedsStr.replace("]", "");
        nNeedsStr.replace("[", "");
        nValues.replace("[", "");
        nValues.replace("]", "");
        nB5.replace("[", "");
        nB5.replace("]", "");

        return nNeedsStr + nValues + nB5;
    }

    /**
     * Method used to generate the string that is going to be used in order to classify the user's profession based on their text.
     * @return A String that is to be used by a Weka classifier.
     */
    public String generateWekaString() {
        String wekaString = "@relation";
        wekaString += System.lineSeparator();

        //Values. All in expected order. The extra variable is used in order to increase readability.
        wekaString += lstNativeValues.get(4).getPercentile().toString() + ",";
        wekaString += lstNativeValues.get(4).getRawScore().toString() + ",";
        wekaString += lstNativeValues.get(2).getPercentile().toString() + ",";
        wekaString += lstNativeValues.get(2).getRawScore().toString() + ",";
        wekaString += lstNativeValues.get(1).getPercentile().toString() + ",";
        wekaString += lstNativeValues.get(1).getRawScore().toString() + ",";
        wekaString += lstNativeValues.get(0).getPercentile().toString() + ",";
        wekaString += lstNativeValues.get(0).getRawScore().toString() + ".";
        wekaString += lstNativeValues.get(3).getPercentile().toString() + ",";
        wekaString += lstNativeValues.get(3).getRawScore().toString() + ",";

        wekaString += lstNativeNeeds.get(3).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(3).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(4).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(4).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(2).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(2).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(6).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(6).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(5).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(5).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(1).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(1).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(9).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(9).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(7).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(7).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(8).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(8).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(10).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(10).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(0).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(0).getRawScore().toString() + ",";
        wekaString += lstNativeNeeds.get(11).getPercentile().toString() + ",";
        wekaString += lstNativeNeeds.get(11).getRawScore().toString() + ",";

        //Big 5
        //Openness
        List<com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait> lst = lstB5, lstB5ch;
        com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait trt = null;
        for (int i = 0; i < 5; i++) {
            //For every trait get it's values. Trt used to increase readability.
            trt = lst.get(i);
            wekaString +=  trt.getPercentile().toString() + ",";
            wekaString += lstB5.get(i).getRawScore().toString() + ",";
            //Get it's children/facets.
            lstB5ch = lstB5.get(i).getChildren();
            //If due to the user's text we have some nulls, just add some empty results.

            for (int j = 0; j < lstB5ch.size(); j++) {
                //Get their values.
                wekaString += lstB5ch.get(j).getPercentile().toString() + ",";
                wekaString += lstB5ch.get(j).getRawScore().toString() + ",";
            }
            //If due to the user's text we have some nulls, just add some empty results.
            if (lstB5ch.size() < 5) {
                for (int k = 0; k < 6 - lst.size(); k++) {
                    wekaString += "0.0" + ",";
                }
            }
        }

        for (int i = 0; i < 10; i++) {
            wekaString += "0.0" + ",";
        }
        //Removing the end ",".
        wekaString = wekaString.substring(0, wekaString.length() - 1);
        wekaString += "," + "politician";
        return wekaString;
    }


    /**
     * A private class, used to handle all needed transfer/copy operations between objects of type com.mesaretzidis.spiros.DataStructures.Implementations.Trait,
     * and objects of Watson's Trait class.
     * @author S. Mesaretzidis
     * @since 31/12/2016
     */
    private class Transfer {
        Profile prof = null;
        public Transfer(Profile prof){
            this.prof = prof;
        }

        /**
         * A private method used to transfer all the traits corresponding to the user's needs containing the corresponding values.
         *
         * @return A native Trait implementation containing the required children..
         * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
         */
        private List<ITrait> transferNeeds() {
            com.mesaretzidis.spiros.DataStructures.Implementations.Trait nTrt;
            List<Trait> needsLst = prof.getNeeds();
            //Watson trait
            Trait wTrait;
            List<ITrait> nativeNeedsLst = new LinkedList<>();

            //Creating a native needs list.
            for (int i = 0; i < 12; i++){
                wTrait = needsLst.get(i);
                nTrt = new com.mesaretzidis.spiros.DataStructures.Implementations.Trait();

                nTrt.setCategory(wTrait.getCategory());
                nTrt.setId(wTrait.getTraitId());
                nTrt.setName(wTrait.getName());
                nTrt.setPercentile(wTrait.getPercentile());
                nTrt.setRawScore(wTrait.getRawScore());

                nativeNeedsLst.add(nTrt);
            }
            return nativeNeedsLst;
        }

        /**
         * A private method used to transfer all the traits corresponding to the user's Values.
         *
         * @return A List implementation containing objects of the native "Trait" class containing the corresponding values.
         * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
         */
        private List<ITrait> transferValues() {
            ITrait nTrt;
            List<Trait> valuesLst = prof.getValues();
            //Watson Trait
            List<ITrait> nativeValuesLst = new LinkedList<>();

            //Creating a native values list.
            for (int i = 0; i < 5; i++) {
               nTrt = copyTrait(valuesLst.get(i));

                nativeValuesLst.add(nTrt);
            }
            return nativeValuesLst;
        }

        /**
         * A private method used to transfer all the traits corresponding to the user's Big 5 traits and facets.
         *
         * @return A List implementation containing objects of the native "Trait" class containing the corresponding values.
         * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
         */
        private List<ITrait> transferBig5() {
            //N stands for native, Ch for children/child.
            ITrait nTrt = null;
            List<Trait> wChLst = new LinkedList<>();
            List<ITrait> nChLst = null, nIChLst;

            //Get personality traits Lst.
            List<Trait> pTraits = prof.getPersonality();

            nChLst = new LinkedList<>();
            for (int i = 0; i < 5; i++) {
                //Copying all of the properties of the trait except its children.
                Trait wTrait = pTraits.get(i);
                nTrt = copyTrait(wTrait);

                //Native trait children list.
                nIChLst = new LinkedList<>();
                //Getting the children.
                wChLst = wTrait.getChildren();
                for (int j = 0; j < 6; j++) {
                    nIChLst.add(copyTrait(wChLst.get(j)));
                }

                //Adding the children to the "parent" trait.
                nTrt.setChildren(nChLst);
                nChLst.add(nTrt);
            }
            return nChLst;
        }

        /**
         * A method used to copy a Watson Trait object, into a native Trait object.
         *
         * @param wTrt A Watson Trait object.
         * @return A native Trait object.
         * @see com.mesaretzidis.spiros.DataStructures.Implementations.Trait
         */
        private ITrait copyTrait(Trait wTrt) {
            com.mesaretzidis.spiros.DataStructures.Implementations.Trait trt = new com.mesaretzidis.spiros.DataStructures.Implementations.Trait();

            trt.setName(wTrt.getName());
            trt.setId(wTrt.getTraitId());
            trt.setCategory(wTrt.getCategory());
            trt.setRawScore(wTrt.getRawScore());
            trt.setPercentile(wTrt.getPercentile());

            return trt;
        }
    }
}
