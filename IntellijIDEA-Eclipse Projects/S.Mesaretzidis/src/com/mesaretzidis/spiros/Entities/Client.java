package com.mesaretzidis.spiros.Entities;

/**
 * Entity used to hold the client's information.
 * Created by S.Mesaretzidis on 30/12/2016.
 */
public class Client {
    private int id;
    private String fn = null, ln = null;

    /**
     * @param id Client's id
     * @param fn Client's first name.
     * @param ln Client's last name.
     */
    public  Client(int id, String fn, String ln) {
        this.id = id;
        this.fn = fn;
        this.ln = ln;
    }

    /**
     * @param fn The client's first name.
     * @param ln The client's last name.
     */
    public  Client(String fn, String ln){
        this.fn = fn;
        this.ln = ln;
    }

    /**
     * Id property getter.
     * @return Client's Id.
     */
    public int getId() {
        return this.id;
    }

    /**
     * Id property setter.
     * @param id Client's Id.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Fn property getter.
     * @return Client's fn.
     */
    public String getFn() {
        return this.fn;
    }

    /**
     * Fn property setter.
     * @param fn Client's fn.
     */
    public void setFn(String fn){
        this.fn = fn;
    }

    /**
     * Ln property getter.
     * @return Client's ln.
     */
    public String getLn(){
        return  this.ln;
    }

    /**
     * Ln property setter.
     * @param ln Client's ln
     */
    public void setLn(String ln) {
        this.ln = ln;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;

        Client client = (Client) o;

        if (id != client.id) return false;
        if (!fn.equals(client.fn)) return false;
        if (!ln.equals(client.ln)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + fn.hashCode();
        result = 31 * result + ln.hashCode();
        return result;
    }
}
