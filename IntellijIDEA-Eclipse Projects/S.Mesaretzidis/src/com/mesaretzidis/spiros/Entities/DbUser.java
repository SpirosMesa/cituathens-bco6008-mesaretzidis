package com.mesaretzidis.spiros.Entities;

/**
 * Class used in order to hold all the information regarding a database user.
 * Created by S.Mesaretzidis on 30/12/2016.
 */
public class DbUser {
    private String username = null;

    /**
     * Username field getter
     * @return The DbUser's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Username field setter
     * @param username The dbUser's username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Class constructor
     *
     * @param username The username of the database user.
     */
    public DbUser( String username){
        this.username = username;
    }



}
