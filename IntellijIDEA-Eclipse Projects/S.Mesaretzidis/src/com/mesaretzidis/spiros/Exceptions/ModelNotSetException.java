package com.mesaretzidis.spiros.Exceptions;

/** Exception class thrown when a trained Weka model has not been set.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public class ModelNotSetException extends Exception{
    /**
     * Default class constructor. No arguments.
     */
    public ModelNotSetException(){
        super("A trained model has not been set.");
    }
}
