package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class, thrown when the file path has not been properly set.
 * @author S. Mesaretzidis
 * @since  31/12/2016.
 */
public class FilePathNotSetException extends  Exception {
    public FilePathNotSetException(){
        super("The file path has not been set");
    }
}
