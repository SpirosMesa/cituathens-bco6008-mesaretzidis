package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class thrown when the a username has not been set.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public class UsernameNotSetException extends Exception{
    /**
     * Default class constructor. No arguments.
     */
    public UsernameNotSetException() {
        super("The username has not been set.");
    }
}
