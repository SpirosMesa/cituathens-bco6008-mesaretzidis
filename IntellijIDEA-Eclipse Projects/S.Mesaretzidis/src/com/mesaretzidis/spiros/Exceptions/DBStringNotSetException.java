package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class thrown when the user has provide a valid connection string object.
 *
 * @author S. Mesaretzidis
 * @since 2/1/2017.
 */
public class DBStringNotSetException extends Exception{

    public DBStringNotSetException(){
        super("DBString not properly set.");
    }
}
