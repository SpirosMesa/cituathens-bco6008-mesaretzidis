package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class, that is thrown when the user has not set the required user credentials.
 * @author S.Mesaretzidis
 * @since 30/12/2016
 */
public class CredentialsNotSetException extends Exception {
    /**
     * Default class constructor. No arguments.
     */
    public CredentialsNotSetException() {
        super("The user credentials have not been set.");
    }
}
