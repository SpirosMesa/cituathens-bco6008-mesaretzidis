package com.mesaretzidis.spiros.Exceptions;

/**
 * @since 30/12/2016
 * @author S. Mesaretzidis
 */
public class TextNotSetException extends Exception {
    /**
     * Default class constructor. No arguments.
     */
    public TextNotSetException() {
        super("The user's text has not been set.");
    }
}
