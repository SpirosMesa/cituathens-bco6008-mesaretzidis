package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class, thrown when the required file type is not yet supported.
 * @since 30/12/2016.
 * @author S. Mesaretzidis
 */
public class FileNotImplementedException extends Exception {
    /**
     * Default class constructor. No arguments.
     */
    public FileNotImplementedException() {
        super("Functionality for this file type has not been implemented yet.");
    }
}
