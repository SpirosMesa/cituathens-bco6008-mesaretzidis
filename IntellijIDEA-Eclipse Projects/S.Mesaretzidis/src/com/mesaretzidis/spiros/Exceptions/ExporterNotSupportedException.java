package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class thrown when the wanted exporter is not yet supported.
 * @since 30/12/2016
 * @author S. Mesaretzidis
 */
public class ExporterNotSupportedException extends Exception {
    /**
     * Default class constructor. No arguments.
     */
    public ExporterNotSupportedException() {
        super("The wanted exported is not yet supported.");
    }
}
