package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class used thrown when a class is not a subclass of a required class.
 *
 * @author S. Mesaretzidis
 * @since 5/1/2017.
 */
public class InheritanceException extends Exception {
    public InheritanceException(String message) {
        super(message);
    }
}
