package com.mesaretzidis.spiros.Exceptions;

/**
 * Wrapper Exception class for the DocumentException class of the used IText library.
 *
 * @author S. Mesaretzidis
 * @since 6-1-2017
 */
public class DocumentException extends Exception {
    private com.itextpdf.text.DocumentException documentException;
    public DocumentException(com.itextpdf.text.DocumentException documentException){
        super(documentException.getMessage());
        this.documentException = documentException;
    }

    public Throwable getCause(){
        return this.documentException.getCause();
    }

    public StackTraceElement[] getStackTrace() {
        return  this.documentException.getStackTrace();
    }

    @Override
    public void printStackTrace() {
        this.documentException.printStackTrace();
    }
}
