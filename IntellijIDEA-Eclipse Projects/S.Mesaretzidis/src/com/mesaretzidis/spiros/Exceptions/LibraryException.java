package com.mesaretzidis.spiros.Exceptions;

import com.ibm.watson.developer_cloud.service.exception.*;

/**
 * Exception class, that act as a wrapper for any exception thrown by Watson's IBM.
 *
 * @author S. Mesaretzidis
 * @author 2/1/2017.
 */
public class LibraryException extends Exception {
    private Throwable cause;
    private String message;
    private BadRequestException bre = null;
    private UnauthorizedException ue = null;
    private TooManyRequestsException tre = null;
    private InternalServerErrorException isee;
    private ServiceResponseException sre;

    /**
     * Class constructor
     *
     * @param ex The original BadRequestException that was thrown.
     * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/service/BadRequestException.html
     */
    public LibraryException(BadRequestException ex) {
        this.cause = ex.getCause();
        this.message = ex.getMessage();
        bre = ex;
    }

    /**
     * Class constructor
     *
     * @param ex The original UnauthorizedException that was thrown.
     * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/service/UnauthorizedException.html
     */
    public LibraryException(UnauthorizedException ex){
        this.cause = ex.getCause();
        this.message = ex.getMessage();
        ue = ex;
    }

    /**
     * Class constructor
     *
     * @param ex The original TooManyRequestException that was thrown.
     * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/service/TooManyRequestsException.html
     */
    public LibraryException(TooManyRequestsException ex){
        this.cause = ex.getCause();
        this.message = ex.getMessage();
        tre = ex;
    }

    /**
     * Class constructor
     *
     * @param ex The original InternalServerErrorException that was thrown.
     * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/service/InternalServerErrorException.html
     */
    public LibraryException(InternalServerErrorException ex) {
        this.cause = ex.getCause();
        this.message = ex.getMessage();
        isee = ex;
    }

    /**
     * Class constructor
     *
     * @param ex The original ServiceResponseException that was thrown.
     * @link http://watson-developer-cloud.github.io/java-sdk/docs/java-sdk-2.10.0/com/ibm/watson/developer_cloud/service/ServiceResponseException.html
     */
    public LibraryException(ServiceResponseException ex) {
        this.cause = ex.getCause();
        this.message = ex.getMessage();
        sre = ex;
    }

    /**
     * Prints the exception's message
     *
     * @return A String containing the message of the exception.
     */
    public String getMessage() {return this.message;}

    /**
     * Returns a Throwable object which indicates the cause of the exception.
     *
     * @return A Throwable object which indicates the cause of the exception.
     */
    public Throwable getCause() {return this.cause;}

    /**
     * Method that prints teh stack trace based on the provided exception.
     */
    public void printStactTrace() {
        if(bre != null) {
            bre.printStackTrace();
            return;
        }
        else if (ue != null) {
            ue.printStackTrace();
            return;
        }
        else if (tre != null) {
            tre.printStackTrace();
            return;
        }
        else if (isee != null) {
            isee.printStackTrace();
            return;
        }
        else if (sre != null) {
            sre.printStackTrace();
            return;
        }
    }
}
