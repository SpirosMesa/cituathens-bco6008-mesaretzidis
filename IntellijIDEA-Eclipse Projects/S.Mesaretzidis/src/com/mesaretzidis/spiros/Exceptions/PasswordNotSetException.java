package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class, thrown when the a db user password has not been set.
 * @author S. Mesaretzidis
 * @since 30/12/2016
 */
public class PasswordNotSetException extends Exception {
    /**
     * Default class constructor. No arguments.
     */
    public PasswordNotSetException() {
        super("The password was not set.");
    }
}
