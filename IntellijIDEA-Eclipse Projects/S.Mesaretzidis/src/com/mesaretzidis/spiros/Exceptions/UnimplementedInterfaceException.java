package com.mesaretzidis.spiros.Exceptions;

/**
 * Exception class thrown when a class does not implement the required interface.
 *
 * @since 4/1/2016
 * @author 5/1/2017.
 */
public class UnimplementedInterfaceException extends  Exception {
    /**
     * Exception class constructor.
     * @param message The message to be used.
     */
    public  UnimplementedInterfaceException(String message) {
        super(message);
    }
}
