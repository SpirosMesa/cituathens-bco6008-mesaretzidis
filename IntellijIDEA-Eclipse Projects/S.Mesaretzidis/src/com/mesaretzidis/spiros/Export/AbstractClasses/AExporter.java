package com.mesaretzidis.spiros.Export.AbstractClasses;

import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Exceptions.FilePathNotSetException;
import com.itextpdf.text.DocumentException;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

/**
 * Abstract class used to define all the operations that are to be implemented for any
 * export operation.
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public abstract class AExporter {
    protected String wekaTree = null, filePath = null;
    protected WatsonResults wResults = null;
    protected boolean logging = false;
    protected  Client client;

    /**
     * Class constructor, taking as input the resulting Weka Tree.
     * @param wekaTree
     */
    public AExporter(String wekaTree) {
        this.wekaTree = wekaTree;
    }

    /**
     * Class constructor, taking as input a WekaResults Object.
     * @param wResults
     */
    public AExporter(WatsonResults wResults) {
        this.wResults = wResults;
    }

    /**
     * Class constructor taking as input all possible arguments.
     * @param wResults The wanted WatsonResults object.
     * @param wekaTree The wanted Weka Tree object.
     * @param filePath Used to set the desired export file path.
     * @param log Used to indicate where logging operations are required.
     * @param client The client whose information is to be exported.
     */
    public AExporter(WatsonResults wResults, String wekaTree, String filePath, boolean log, Client client) {
        this.wResults = wResults;
        this.wekaTree = wekaTree;
        this.logging = log;
        this.filePath = filePath;
        this.client = client;
    }
    /**
     * Default class constructor.
     */
    public AExporter() {}

    /**
     * Logging property setter.
     * @param log Used to set the logging operations.
     */
    public void setLogging(boolean log) {
        this.logging = log;
    }

    /**
     * Logging property getter.
     * @return Indicates if logging will occur.
     */
    public boolean getLogging() {
        return this.logging;
    }

    /**
     * Weka Tree property setter.
     * @param wekaTree The tree to be set.
     */
    public void setWekaTree(String wekaTree){
        this.wekaTree = wekaTree;
    }

    /**
     * Weka Tree property getter.
     * @return The set tree.
     */
    public String getWekaTree() {
        return this.wekaTree;
    }

    /**
     * WatsonResults property setter.
     * @param wResults The WatsonResults object, to be set.
     */
    public void setWatsonResults(WatsonResults wResults) {
        this.wResults = wResults;
    }

    /**
     * Watson results property getter.
     * @return Returns the stored WatsonResults object.
     */
    public WatsonResults getWatsonResults() {
        return this.wResults;
    }

    /**
     * File Path property getter.
     * @return Returns a String containing the file path.
     */
    public String getFilePath(){
        return  this.getFilePath();
    }

    /**
     * File path property setter.
     * @param filePath The File Path to be set.
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Client field setter.
     *
     * @param client The Client object to be set.
     */
    public void setClient(Client client) {this.client = client;}

    /**
     * Client field getter.
     *
     * @return The set Client object.
     */
    public Client getClient() {return this.client;}

    /**
     * Method used to export the data to a file.
     */
    public abstract void exportFile() throws FilePathNotSetException, IOException, DocumentException, XMLStreamException, com.mesaretzidis.spiros.Exceptions.DocumentException;

    /**
     * Method used to export the data to a file.
     * @param filePath The file path, that the data will be exported to.
     */
    public abstract void exportFile(String filePath) throws IOException, com.mesaretzidis.spiros.Exceptions.DocumentException, XMLStreamException;

}
