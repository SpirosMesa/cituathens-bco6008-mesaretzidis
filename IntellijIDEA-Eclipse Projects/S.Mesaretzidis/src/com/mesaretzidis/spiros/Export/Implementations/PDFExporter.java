package com.mesaretzidis.spiros.Export.Implementations;

import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;


/** Protected class used to export all the needed data to a PDF file.
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
class PDFExporter extends AExporter {
    private static final Logger log = Logger.getLogger(PDFExporter.class);
    private static File file;

    /**
     * Class constructor receiving all possible arguments.
     *
     * @param wResults A WatsonResults object that is to be exported.
     * @param wekaTree A String containing the Weka-generated Decision Tree that is to be exported.
     * @param filePath The file that the file should be exported to.
     * @param log Stating whether the methods should use logging.
     * @param client A Client object that is to be exported.
     */
    public PDFExporter(WatsonResults wResults, String wekaTree, String filePath, boolean log, Client client) {
        super(wResults, wekaTree, filePath, log, client);
    }

    /**
     * Default class exported accepting no arguments.
     */
    public PDFExporter() {
        super();
    }


    /**
     * Method used to export the needed data to a file.
     */
    @Override
    public void exportFile() throws FileNotFoundException, com.mesaretzidis.spiros.Exceptions.DocumentException {
        if(logging == true) exportFileLg();
        else exportFileWoLg();
    }

    /**
     * Method used to export data to a file,
     * while logging operations are disabled.
     */
    private void exportFileWoLg() throws FileNotFoundException, com.mesaretzidis.spiros.Exceptions.DocumentException {
        String fnUpper = client.getFn().replace(client.getFn().charAt(0), client.getFn().toUpperCase().charAt(0));
        String lnUpper = client.getLn().replace(client.getLn().charAt(0), client.getLn().toUpperCase().charAt(0));

        try {
            file = new File(this.filePath);
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(file));
            doc.open();
            doc.addTitle("Results of " + lnUpper + " " + fnUpper);
            doc.addAuthor("S. Mesaretzidis");
            doc.addCreator("S. Mesaretzidis");
            doc.addSubject("BCO 6008 - PDF Export");
            doc.addCreationDate();
            pdfAddTitlePage(doc, fnUpper, lnUpper);
            pdfAddWekaTree(doc, wekaTree);

            if (wResults == null) {
                doc.add(new Paragraph("Watson Results not available"));
                doc.close();
                return;
            }
            pdfAddWatsonValues(doc);
            pdfAddWatsonNeeds(doc);
            pdfAddWatsonBig5(doc);
            doc.close();
        }
        catch (DocumentException e) {
            throw new com.mesaretzidis.spiros.Exceptions.DocumentException(e);
        }
    }

    /**
     * Private method used for export file operations, while logging is enabled.
     */
    private void exportFileLg() throws com.mesaretzidis.spiros.Exceptions.DocumentException, FileNotFoundException {
        String fnUpper = client.getFn().replace(client.getFn().charAt(0), client.getFn().toUpperCase().charAt(0));
        String lnUpper = client.getLn().replace(client.getLn().charAt(0), client.getLn().toUpperCase().charAt(0));

        try {
            file = new File(this.filePath);
            log.info("Got file object.");
            Document doc = new Document();
            log.info("Got document object");
            PdfWriter.getInstance(doc, new FileOutputStream(file));
            log.info("Got pdf writer object");
            doc.open();

            log.info("Opened doc object. Adding metadata.");
            doc.addTitle("Results of " + lnUpper + " " + fnUpper);
            doc.addAuthor("S. Mesaretzidis");
            doc.addCreator("S. Mesaretzidis");
            doc.addSubject("BCO 6008 - PDF Export");
            doc.addCreationDate();
            log.info("Added document metadata.");

            log.info("Adding title page.");
            pdfAddTitlePage(doc, fnUpper, lnUpper);
            log.info("Adding weka tree");
            pdfAddWekaTree(doc, wekaTree);

            if (wResults == null) {
                doc.add(new Paragraph("Watson Results not available"));
                doc.close();
                return;
            }

            log.info("Adding watson results.");
            pdfAddWatsonValues(doc);
            log.info("Adding watson needs.");
            pdfAddWatsonNeeds(doc);
            log.info("Adding watson b5");
            pdfAddWatsonBig5(doc);
            doc.close();
        }
        catch (DocumentException e) {
            log.error("Caught document exception. Message: " + e.getMessage() + "\n StackTrace: " + e.getStackTrace() + "\n Cause: " + e.getCause());
            throw new com.mesaretzidis.spiros.Exceptions.DocumentException(e);
        } catch (FileNotFoundException e) {
            log.error("Caught document exception. Message: " + e.getMessage() + "\n StackTrace: " + e.getStackTrace() + "\n Cause: " + e.getCause());
            throw e;
        }
    }

    /**
     * Method used to export the needed data to the file
     * provided by the file path.
     *
     * @param filePath The file path, that the data will be exported to.
     */
    @Override
    public void exportFile(String filePath) throws FileNotFoundException, com.mesaretzidis.spiros.Exceptions.DocumentException {
        if(logging == true) exportFileLg(filePath);
        else exportFileWoLg(filePath);
    }

    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are disabled.
     *
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileWoLg(String filePath) throws FileNotFoundException, com.mesaretzidis.spiros.Exceptions.DocumentException {
        String fnUpper = client.getFn().replace(client.getFn().charAt(0), client.getFn().toUpperCase().charAt(0) );
        String lnUpper = client.getLn().replace(client.getLn().charAt(0), client.getLn().toUpperCase().charAt(0));

        try {
            file = new File(filePath);
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(file));
            doc.open();
            doc.addTitle("Results of " + lnUpper + " " + fnUpper);
            doc.addAuthor("S. Mesaretzidis");
            doc.addCreator("S. Mesaretzidis");
            doc.addSubject("BCO 6008 - PDF Export");
            doc.addCreationDate();
            pdfAddTitlePage(doc, fnUpper, lnUpper);
            pdfAddWekaTree(doc, wekaTree);

            if (wResults == null) {
                doc.add(new Paragraph("Watson Results not available"));
                doc.close();
                return;
            }
            pdfAddWatsonValues(doc);
            pdfAddWatsonNeeds(doc);
            pdfAddWatsonBig5(doc);
            doc.close();
        }
        catch (DocumentException e) {
            throw new com.mesaretzidis.spiros.Exceptions.DocumentException(e);
        }
    }

    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are enabled.
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileLg(String filePath) throws com.mesaretzidis.spiros.Exceptions.DocumentException, FileNotFoundException {
        String fnUpper = client.getFn().replace(client.getFn().charAt(0), client.getFn().toUpperCase().charAt(0));
        String lnUpper = client.getLn().replace(client.getLn().charAt(0), client.getLn().toUpperCase().charAt(0));

        try {
            file = new File(filePath);
            log.info("Got file object.");
            Document doc = new Document();
            log.info("Got document object");
            PdfWriter.getInstance(doc, new FileOutputStream(file));
            log.info("Got pdf writer object");
            doc.open();

            log.info("Opened doc object. Adding metadata.");
            doc.addTitle("Results of " + lnUpper + " " + fnUpper);
            doc.addAuthor("S. Mesaretzidis");
            doc.addCreator("S. Mesaretzidis");
            doc.addSubject("BCO 6008 - PDF Export");
            doc.addCreationDate();
            log.info("Added document metadata.");

            log.info("Adding title page.");
            pdfAddTitlePage(doc, fnUpper, lnUpper);
            log.info("Adding weka tree");
            pdfAddWekaTree(doc, wekaTree);

            if (wResults == null) {
                doc.add(new Paragraph("Watson Results not available"));
                doc.close();
                return;
            }

            log.info("Adding watson results.");
            pdfAddWatsonValues(doc);
            log.info("Adding watson needs.");
            pdfAddWatsonNeeds(doc);
            log.info("Adding watson b5");
            pdfAddWatsonBig5(doc);
            doc.close();
        }
        catch (DocumentException e) {
            log.error("Caught document exception. Message: " + e.getMessage() + "\n StackTrace: " + e.getStackTrace() + "\n Cause: " + e.getCause());
            throw new com.mesaretzidis.spiros.Exceptions.DocumentException(e);
        } catch (FileNotFoundException e) {
            log.error("Caught document exception. Message: " + e.getMessage() + "\n StackTrace: " + e.getStackTrace() + "\n Cause: " + e.getCause());
            throw e;
        }
    }

    /**
     * Private method used to add a title page to a pdf document.
     *
     * @param doc The document to be exported.
     * @param fn The client's first name.
     * @param ln The client's last name.
     */
    private void pdfAddTitlePage(Document doc, String fn,String ln) throws DocumentException {
        Paragraph titlePage = new Paragraph();
        titlePage.add(System.lineSeparator());
        titlePage.add(new Paragraph("Results of client " + System.lineSeparator()) + ln + " " + fn);
        titlePage.add(new Paragraph(System.lineSeparator()));
        titlePage.add(new Paragraph("Document created by:"));
        titlePage.add(new Paragraph("S. Mesatzidis"));
        doc.add(titlePage);
        doc.newPage();
    }

    /**
     * Private method used in order to add the result tree produced by weka.
     *
     * @param doc The document where Weka's decision tree will be written in.
     * @param tree The tree to be written.
     */
    private void pdfAddWekaTree(Document doc, String tree) throws DocumentException {
        if ((tree == null) || tree.isEmpty()) {
            doc.add(new Paragraph("Weka decision tree not available"));
            doc.newPage();
            return;
        }
        //No else statement needed.
        doc.add(new Paragraph("Weka Decision tree"));
        doc.add(new Paragraph(System.lineSeparator()));
        doc.add(new Paragraph(tree));
        doc.newPage();
    }

    /**
     * Private method used in order to write the Watson's values into the file.
     * @param doc The document where the values will be placed in.
     * @throws DocumentException
     */
    private void pdfAddWatsonValues(Document doc) throws DocumentException {
        if (wResults.getLstNativeValues() == null ) {
            doc.add(new Paragraph("Watson Values not available"));
            doc.newPage();
            return;
        }

        doc.add(new Paragraph("Watson Values"));
        doc.add(new Paragraph(System.lineSeparator()));

        for (int i = 0; i < wResults.getLstNativeValues().size(); i++ ){
            doc.add(new Paragraph("Trait Category: " + wResults.getLstNativeValues().get(i).getCategory() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Id: " + wResults.getLstNativeValues().get(i).getId() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Name: " + wResults.getLstNativeValues().get(i).getName() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Percentile: " + wResults.getLstNativeValues().get(i).getPercentile() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Raw Score: " + wResults.getLstNativeValues().get(i).getRawScore() + System.lineSeparator()));
        }
        doc.newPage();
    }

    /**
     * Method used in order to add Watson's Needs to the document.
     *
     * @param doc The Document object where Watson's needs will be written.
     * @throws DocumentException
     */
    private void pdfAddWatsonNeeds(Document doc) throws DocumentException {
        if(wResults.getLstNativeNeeds() == null) {
            doc.add(new Paragraph("Watson Needs not available"));
            doc.newPage();
            return;
        }

        ITrait trt = null;
        doc.add(new Paragraph("Watson Needs" + System.lineSeparator()));
        for (int i = 0; i < wResults.getLstNativeNeeds().size(); i++) {
            trt = wResults.getLstNativeNeeds().get(i);
            doc.add(new Paragraph("Trait Category: " + trt.getCategory() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Id: "+ trt.getId() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Name: " + trt.getName() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Percentile: " + trt.getPercentile() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Raw Score: " + trt.getRawScore() + System.lineSeparator()));
        }
        doc.newPage();
    }

    /**
     * Private method used in order to write Watson's Big 5 into the provided document.
     *
     * @param doc The Document object where Big 5 are to be written.
     * @throws DocumentException
     */
    private void pdfAddWatsonBig5(Document doc) throws DocumentException {
        if (wResults.getLstB5() == null) {
            doc.add(new Paragraph("Watson Big 5 not available"));
            doc.newPage();
            return;
        }

        doc.add(new Paragraph("Watson Big 5"));
        List<ITrait> chLst = null;
        ITrait trt;
        for (int i = 0; i < wResults.getLstB5().size(); i++) {
            trt = wResults.getLstB5().get(i);
            doc.add(new Paragraph("Trait Category: " + trt.getCategory() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Id: " + trt.getId() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Name: " + trt.getName() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Percentile: " + trt.getPercentile() + System.lineSeparator()));
            doc.add(new Paragraph("Trait Raw Score: " + trt.getRawScore() + System.lineSeparator()));
            doc.add(new Paragraph("Facets are: " + System.lineSeparator()));
            doc.add(new Paragraph(System.lineSeparator()));

            chLst = trt.getChildren();

            for (int j = 0; j < chLst.size(); j++) {
                trt = chLst.get(j);
                doc.add(new Paragraph("\t\t" + "Facet Name: " + trt.getName() + System.lineSeparator()));
                doc.add(new Paragraph("\t" + "Facet Category: " + trt.getCategory() + System.lineSeparator()));
                doc.add(new Paragraph("\t" + "Facet Id: " + trt.getId() + System.lineSeparator()));
                doc.add(new Paragraph("\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator()));
                doc.add(new Paragraph("\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator() + System.lineSeparator()));
            }
        }
    }

    /**
     * Private method used to add an empty line.
     *
     * @param paragraph The paragraph where we are gonna add the empty line
     * @param number The number of empty lines.
     */
    private void pdfAddEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }


}
