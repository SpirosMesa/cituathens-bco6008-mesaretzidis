package com.mesaretzidis.spiros.Export.Implementations;

import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Exceptions.ExporterNotSupportedException;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import com.sun.istack.internal.NotNull;

/**
 * Factory class used to get the appropriate exporter.
 * @Author S. Mesaretzidis
 * @since 30/12/2016.
 */
public class ExporterFactory {
    public static final int PDFExporter = 1;
    public static final int XMLExporter = 2;
    public static final int TXTExporter = 3;

    /**
     * This method is used in order to provide a class that implements/inherits from the abstract class AExporter that will be used in order to
     * export the file to the proper format.
     *
     * @param exporterType A static variable indicating the type of the exporter that is to be returned.
     * @return An implementation/child class of the abstract class AExporter. The class type of the object depends on the input.
     * @throws ExporterNotSupportedException Thrown if an exporter is requested, that is not yet supported.
     */
    public AExporter getExporter(int exporterType) throws ExporterNotSupportedException {
        if (exporterType == PDFExporter) return new PDFExporter();
        else if (exporterType == XMLExporter) return  new XMLExporter();
        else if (exporterType == TXTExporter) return new TXTExporter();
        else throw new ExporterNotSupportedException();
    }

    /**
     *
     * @param exporterType A static variable indicating the type of the exporter that is to be returned.
     * @param watsonResults A WatsonResults object that is to be exported.
     * @param wekaTree A String object, containing the Weka-generated decision tree that is to be exported.
     * @param filePath The file path that is to be used.
     * @param log Boolean variable indicating whether logging should be used.
     * @param client A Client object that is to be exported.
     * @return An implementation/child class of the abstract class AExporter. The class type of the object depends on the input.
     * @throws ExporterNotSupportedException Thrown if an exporter is requested, that is not yet supported.
     */
    public AExporter getExporter(int exporterType, @NotNull WatsonResults watsonResults,@NotNull String wekaTree, @NotNull String filePath,@NotNull boolean log, @NotNull Client client) throws ExporterNotSupportedException {
        if (exporterType == PDFExporter) return new PDFExporter(watsonResults, wekaTree, filePath, log, client);
        else if (exporterType == XMLExporter) return  new XMLExporter(watsonResults, wekaTree, filePath, log, client);
        else if (exporterType == TXTExporter) return new TXTExporter(watsonResults, wekaTree, filePath, log, client);
        else throw new ExporterNotSupportedException();
    }
}

