package com.mesaretzidis.spiros.Export.Implementations;

import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import java.util.List;

/** Protected class used to export all the needed data to a PDF file.
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
class XMLExporter extends AExporter {
    private static XMLStreamWriter myXmlStreamWriter = null;
    private static final Logger log = Logger.getLogger(XMLExporter.class);

    /**
     * Class constructor receiving all possible arguments.
     *
     * @param wResults A WatsonResults object that is to be exported.
     * @param wekaTree A String containing the Weka-generated Decision Tree that is to be exported.
     * @param filePath The file that the file should be exported to.
     * @param log Stating whether the methods should use logging.
     * @param client A Client object that is to be exported.
     */
    public XMLExporter(WatsonResults wResults, String wekaTree, String filePath, boolean log, Client client) {
        super(wResults, wekaTree, filePath, log, client);
    }

    /**
     * Default class constructor accepting no arguments.
     */
    public XMLExporter() {
        super();
    }

    /**
     * Method used to export the needed data to a file.
     */
    @Override
    public void exportFile() throws XMLStreamException, IOException {
        if(logging == true) exportFileLg();
        else exportFileWoLg();
    }

    /**
     * Method used to export the needed data to the file
     * provided by the file path.
     * @param filePath The file path that the data will be exported to.
     */
    @Override
    public void exportFile(String filePath) throws IOException, XMLStreamException {
        if(logging == true) exportFileLg(filePath);
        else exportFileWoLg(filePath);
    }

    /**
     * Method used to export the file, while logging operations are enabled.
     */
    private void exportFileLg() throws XMLStreamException, FileNotFoundException {

    }

    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are enabled.
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileLg(String filePath) throws IOException, XMLStreamException {
        File file = new File(filePath);
        log.info("Class XMLExporter");
        log.info("Created file object.");
        try {
            file.createNewFile();
            log.info("createNewFile method called");
        } catch (IOException e) {
            log.error("IO Exception on trying to create/open a file.");
            log.error("Exception message: " + e.getMessage());
            log.error("Exception cause: " + e.getCause());
            log.error("StackTrace: " + e.getStackTrace());
            throw e;
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            log.info("Created FileOutputStream");
            XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            log.info("Created XMLOutputFactory");
            if (this.myXmlStreamWriter == null) this.myXmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(fileOutputStream);
            log.info("Created XMLStreamWriter");

            myXmlStreamWriter.writeStartDocument();
            writeNewLine();
            //Client
            myXmlStreamWriter.writeStartElement("Client");
            writeNewLine();
            writeTab(1);
            log.info("Writing client.");
            //First name element.
            myXmlStreamWriter.writeStartElement("fn");
            myXmlStreamWriter.writeAttribute("type", "first name");
            myXmlStreamWriter.writeCharacters(client.getFn());
            myXmlStreamWriter.writeEndElement();
            log.info("Wrote fn.");

            //New line
            writeNewLine();
            writeTab(1);
            //Last name element/
            myXmlStreamWriter.writeStartElement("ln");
            myXmlStreamWriter.writeAttribute("type", "last name");
            myXmlStreamWriter.writeCharacters(client.getLn());
            myXmlStreamWriter.writeEndElement();
            log.info("Wrote ln.");

            //End client
            writeNewLine();
            myXmlStreamWriter.writeEndElement();

            //Watson results
            log.info("Writing WatsonResults.s");
            exportWatsonResultsWoLg();
            log.info("Wrote Watson Results");

            //Weka Tree
            log.info("Writing Weka Tree.");
            exportWekaTreeWoLg();
            log.info("Wrote weka tree.");

            fileOutputStream.flush();
            myXmlStreamWriter.flush();
            log.info("flushed streams.");
            fileOutputStream.close();
            myXmlStreamWriter.close();
            log.info("Closed streams");
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException thrown");
            log.error("Exception message: " + e.getMessage());
            log.error("Exception cause: " + e.getCause());
            log.error("StackTrace: " + e.getStackTrace());
            throw e;
        } catch (IOException e) {
            log.error("IOException thrown");
            log.error("Exception message: " + e.getMessage());
            log.error("Exception cause: " + e.getCause());
            log.error("StackTrace: " + e.getStackTrace());
            throw e;
        } catch (XMLStreamException e) {
            log.error("XMLStreamException thrown");
            log.error("Exception message: " + e.getMessage());
            log.error("Exception cause: " + e.getCause());
            log.error("StackTrace: " + e.getStackTrace());
            throw e;
        }
    }

    /**
     * Method used to export data to a file,
     * while logging operations are disabled.
     */
    private void exportFileWoLg() throws XMLStreamException, IOException {
        File file = new File(this.filePath);
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        if (this.myXmlStreamWriter == null) this.myXmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(fileOutputStream);

        myXmlStreamWriter.writeStartDocument();
        writeNewLine();
        //Client
        myXmlStreamWriter.writeStartElement("Client");
        writeNewLine();
        writeTab(1);
            //First name element.
            myXmlStreamWriter.writeStartElement("fn");
            myXmlStreamWriter.writeAttribute("type", "first name");
            myXmlStreamWriter.writeCharacters(client.getFn());
            myXmlStreamWriter.writeEndElement();

            //New line
            writeNewLine();
            writeTab(1);
            //Last name element/
            myXmlStreamWriter.writeStartElement("ln");
            myXmlStreamWriter.writeAttribute("type", "last name");
            myXmlStreamWriter.writeCharacters(client.getLn());
            myXmlStreamWriter.writeEndElement();
        //End client
        writeNewLine();
        myXmlStreamWriter.writeEndElement();

        //Watson results
        exportWatsonResultsWoLg();

        //Weka Tree
        exportWekaTreeWoLg();

        fileOutputStream.flush();
        myXmlStreamWriter.flush();
        fileOutputStream.close();
        myXmlStreamWriter.close();
    }

    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are disabled.
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileWoLg(String filePath) throws IOException, XMLStreamException {
        File file = new File(filePath);
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        if (this.myXmlStreamWriter == null) this.myXmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(fileOutputStream);

        myXmlStreamWriter.writeStartDocument();
        writeNewLine();
        //Client
        myXmlStreamWriter.writeStartElement("Client");
        writeNewLine();
        writeTab(1);
        //First name element.
        myXmlStreamWriter.writeStartElement("fn");
        myXmlStreamWriter.writeAttribute("type", "first name");
        myXmlStreamWriter.writeCharacters(client.getFn());
        myXmlStreamWriter.writeEndElement();

        //New line
        writeNewLine();
        writeTab(1);
        //Last name element/
        myXmlStreamWriter.writeStartElement("ln");
        myXmlStreamWriter.writeAttribute("type", "last name");
        myXmlStreamWriter.writeCharacters(client.getLn());
        myXmlStreamWriter.writeEndElement();
        //End client
        writeNewLine();
        myXmlStreamWriter.writeEndElement();

        //Watson results
        exportWatsonResultsWoLg();

        //Weka Tree
        exportWekaTreeWoLg();

        fileOutputStream.flush();
        myXmlStreamWriter.flush();
        fileOutputStream.close();
        myXmlStreamWriter.close();
    }

    /**
     * Private method used to write Watson' Results to the provided file.
     *
     * @throws XMLStreamException When there is a XMLStream error.
     * @throws IOException When there is an IO Exception
     */
    private void exportWatsonResultsWoLg() throws XMLStreamException, IOException {
        if (wResults == null) return;
       writeNewLine();
        myXmlStreamWriter.writeStartElement("Watson Results");
        myXmlStreamWriter.writeAttribute("data_type" , "com.mesaretzidis.spiros.Entities.WatsonResults");

            exportWatsonNeedsWoLg();
            exportWatsonValuesWoLg();
            exportWatsonB5WoLg();

       writeNewLine();
       myXmlStreamWriter.writeEndElement();
    }

    /**
     * Private method used to write Watson's Needs to the provided file.
     *
     * @throws XMLStreamException XMLStreamException When there is a XMLStream error.
     * @throws IOException IOException When there is an IO Exception
     */
    private void exportWatsonNeedsWoLg() throws XMLStreamException, IOException {
        List<ITrait> lst = wResults.getLstNativeNeeds();
        if (lst == null) return;

        writeNewLine(); writeTab(1);
        myXmlStreamWriter.writeStartElement("Needs");
        myXmlStreamWriter.writeAttribute("personality model", "needs");

        for (int i = 0; i < lst.size(); i++) {
            writeNewLine(); writeTab(2);
            ITrait trt = lst.get(i);
            myXmlStreamWriter.writeStartElement(trt.getName());
            myXmlStreamWriter.writeAttribute("data_type", "com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait");
            myXmlStreamWriter.writeAttribute("category", trt.getCategory());
            myXmlStreamWriter.writeAttribute("id", trt.getId());

                //Percentile
                writeNewLine(); writeTab(3);
                myXmlStreamWriter.writeStartElement("Percentile");
                myXmlStreamWriter.writeAttribute("type", "percentile");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters(trt.getPercentile().toString());
                myXmlStreamWriter.writeEndElement();

                //Raw score
                writeNewLine(); writeTab(3);
                myXmlStreamWriter.writeStartElement("Raw_Score");
                myXmlStreamWriter.writeAttribute("type", "raw score");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters( (trt.getRawScore() == null ? "0.0" : trt.getRawScore().toString()));
                myXmlStreamWriter.writeEndElement();

            writeNewLine();writeTab(2);
            myXmlStreamWriter.writeEndElement();
        }
        writeNewLine();writeTab(1);
        myXmlStreamWriter.writeEndElement();
    }

    /**
     * Method used to write Watson's Values to the provided file.
     *
     * @throws XMLStreamException XMLStreamException XMLStreamException When there is a XMLStream error.
     * @throws IOException IOException IOException When there is an IO Exception
     */
    private void exportWatsonValuesWoLg() throws XMLStreamException, IOException {
        List<ITrait> lst = wResults.getLstNativeValues();
        if (lst == null) return;

        writeNewLine(); writeTab(1);
        myXmlStreamWriter.writeStartElement("Values");
        myXmlStreamWriter.writeAttribute("personality model", "values");

        for (int i = 0; i < lst.size(); i++) {
            writeNewLine();writeTab(2);
            ITrait trt = lst.get(i);
            myXmlStreamWriter.writeStartElement(trt.getName());
            myXmlStreamWriter.writeAttribute("category", trt.getCategory());
            myXmlStreamWriter.writeAttribute("id", trt.getId());
            myXmlStreamWriter.writeAttribute("data_type", "com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait");

                //Percentile
                writeNewLine(); writeTab(3);
                myXmlStreamWriter.writeStartElement("Percentile");
                myXmlStreamWriter.writeAttribute("type", "percentile");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters(trt.getPercentile().toString());
                myXmlStreamWriter.writeEndElement();

                //Raw_Score
                writeNewLine();writeTab(3);
                myXmlStreamWriter.writeStartElement("Raw_Score");
                myXmlStreamWriter.writeAttribute("type", "raw score");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters((trt.getRawScore() == null ? "0.0" : trt.getRawScore().toString()));
                myXmlStreamWriter.writeEndElement();

            writeNewLine();writeTab(2);
            myXmlStreamWriter.writeEndElement();
        }
        writeNewLine(); writeTab(1);
        myXmlStreamWriter.writeEndElement();
    }

    /**
     * Method used to export Watson's B5 traits and facets.
     *
     * @throws XMLStreamException XMLStreamException XMLStreamException When there is a XMLStream error.
     * @throws IOException IOException IOException When there is an IO Exception.
     */
    private void exportWatsonB5WoLg() throws XMLStreamException, IOException {
        List<ITrait> lst = wResults.getLstB5(), chList;
        ITrait trt = null, chTrt = null;
        if (lst == null) return;

        writeNewLine(); writeTab(1);
        //Opening big 5 element.
        myXmlStreamWriter.writeStartElement("Watson Big 5");

        for (int i = 0; i < lst.size(); i++) {
            trt = lst.get(i);
            chList = trt.getChildren();

            //Opening first facet element.
            writeNewLine();writeTab(2);
            myXmlStreamWriter.writeStartElement(trt.getName().replace(" ", "_"));
            myXmlStreamWriter.writeAttribute("category", trt.getCategory());
            myXmlStreamWriter.writeAttribute("id", trt.getId());
            writeNewLine();writeTab(3);

                //Opening percentile element
                myXmlStreamWriter.writeStartElement("Percentile");
                myXmlStreamWriter.writeAttribute("type", "percentile");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters(trt.getPercentile().toString());

                //Closing percentile element
                myXmlStreamWriter.writeEndElement();
                writeNewLine();writeTab(3);

                //Opening raw score element
                myXmlStreamWriter.writeStartElement("Raw_Score");
                myXmlStreamWriter.writeAttribute("type", "raw score");
                myXmlStreamWriter.writeAttribute("data_type", "Double");
                myXmlStreamWriter.writeCharacters((trt.getRawScore() == null ? "0.0" : trt.getRawScore().toString()));

                //Closing raw score element
                myXmlStreamWriter.writeEndElement();

                //Opening children element.
                writeNewLine();writeTab(3);
                myXmlStreamWriter.writeStartElement("Children");
                myXmlStreamWriter.writeAttribute("type", "children");
                myXmlStreamWriter.writeAttribute("data_type", "List<com.mesaretzidis.spiros.DataStructures,Interfaces.ITrait>");
                myXmlStreamWriter.writeAttribute("number of children", String.valueOf(chList.size()));

                for (int j = 0; j < chList.size(); j++) {
                    chTrt = chList.get(j);
                    writeNewLine();writeTab(4);
                    //Opening child element
                    myXmlStreamWriter.writeStartElement(chTrt.getName().replace(" ", "_"));
                    myXmlStreamWriter.writeAttribute("category", chTrt.getCategory());
                    myXmlStreamWriter.writeAttribute("id", chTrt.getId());
                    myXmlStreamWriter.writeAttribute("data_type", "com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait");
                    writeNewLine();writeTab(5);

                        //Opening percentile element
                        myXmlStreamWriter.writeStartElement("Percentile");
                        myXmlStreamWriter.writeAttribute("name", "percentile");
                        myXmlStreamWriter.writeAttribute("data_type", "Double");
                        myXmlStreamWriter.writeCharacters(chTrt.getPercentile().toString());

                        //Closing percentile element.
                        myXmlStreamWriter.writeEndElement();
                        writeNewLine();writeTab(5);

                        //Opening raw score element
                        myXmlStreamWriter.writeStartElement("Raw_Score");
                        myXmlStreamWriter.writeAttribute("name", "raw score");
                        myXmlStreamWriter.writeAttribute("data_type", "Double");
                        myXmlStreamWriter.writeCharacters((chTrt.getRawScore() == null ? "0.0" : chTrt.getRawScore().toString()));

                        //Closing raw score element.
                        myXmlStreamWriter.writeEndElement();
                        writeNewLine();

                    writeTab(4);
                    //Closing child element
                    myXmlStreamWriter.writeEndElement();
                }
                //Closing children element
                writeNewLine(); writeTab(3);
                myXmlStreamWriter.writeEndElement();
            //Closing facet element
            writeNewLine();writeTab(2);
            myXmlStreamWriter.writeEndElement();
        }
        //Closing watson element
        writeNewLine();writeTab(1);
        myXmlStreamWriter.writeEndElement();
    }

    /**
     * Method used in order to export the decision tree provided by Weka.
     *
     * @throws XMLStreamException XMLStreamException XMLStreamException XMLStreamException When there is a XMLStream error.
     * @throws IOException IOException IOException IOException When there is an IO Exception.
     */
    private void exportWekaTreeWoLg() throws XMLStreamException, IOException {
        if ((wekaTree == null) || wekaTree.isEmpty()) return;

        writeNewLine();
        myXmlStreamWriter.writeStartElement("Weka Tree");
        myXmlStreamWriter.writeAttribute("name", "Weka tree");
        writeNewLine();writeTab(1);

            myXmlStreamWriter.writeStartElement("blob");
            myXmlStreamWriter.writeAttribute("byte[]", "java.lang.String");

            myXmlStreamWriter.writeCharacters(String.valueOf(wekaTree.getBytes("UTF-8")));
            myXmlStreamWriter.writeEndElement();

        writeNewLine();
        myXmlStreamWriter.writeEndElement();
    }

    /**
     * Method used to write a new line. This method was used due to the frequency that the developer wanted to insert a new line.
     *
     * @throws XMLStreamException XMLStreamException XMLStreamException XMLStreamException XMLStreamException When there is a XMLStream error.
     */
    private void writeNewLine() throws XMLStreamException {
        this.myXmlStreamWriter.writeCharacters(System.lineSeparator());
    }

    /**
     * Method used in order write a tab character, the request number of times.
     *
     * @param count The number of tab characters that are to be written.
     * @throws XMLStreamException XMLStreamException XMLStreamException XMLStreamException XMLStreamException When there is a XMLStream error.
     */
    private void writeTab(int count) throws XMLStreamException {
        for (int i = 0; i < count; i++)
            this.myXmlStreamWriter.writeCharacters("\t");
    }
}
