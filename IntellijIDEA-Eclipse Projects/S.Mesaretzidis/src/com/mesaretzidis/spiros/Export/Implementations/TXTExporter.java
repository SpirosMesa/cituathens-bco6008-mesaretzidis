package com.mesaretzidis.spiros.Export.Implementations;

import com.mesaretzidis.spiros.DataStructures.Interfaces.ITrait;
import com.mesaretzidis.spiros.Entities.Client;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Export.AbstractClasses.AExporter;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/** Protected class used to export all the needed data to a PDF file.
 *
 * @author S. Mesaretzidis
 * @since 31/12/2016.
 */
class TXTExporter extends AExporter {
   private static final Logger log = Logger.getLogger(TXTExporter.class);

    /**
     * Class constructor receiving all possible arguments.
     *
     * @param wResults A WatsonResults object that is to be exported.
     * @param wekaTree A String containing the Weka-generated Decision Tree that is to be exported.
     * @param filePath The file that the file should be exported to.
     * @param log Stating whether the methods should use logging.
     * @param client A Client object that is to be exported.
     */
    public TXTExporter(WatsonResults wResults, String wekaTree, String filePath, boolean log, Client client) {
        super(wResults, wekaTree, filePath, log, client);
    }

    /**
     * Default class exported accepting no arguments.
     */
    public TXTExporter() {
        super();
    }

    /**
     * Method used to export the needed data to a file.
     */
    @Override
    public void exportFile() throws IOException {
        if(logging == true) exportLg();
        else exportWoLg();
    }

    /**
     * Method used to export the file, while logging operations are enabled.
     */
    private void exportLg()  {
        log.info("Exporting from class TXTExporter.");
        File file = new File(this.filePath);
        log.info("Created File object.");
        FileWriter fw = null;
        try {
            fw = new FileWriter(file);
            log.info("Got FileWriterObject.");
            fw.write("First Name: " + client.getFn() + System.lineSeparator());
            fw.write("Last Name: " + client.getLn() + System.lineSeparator());
            log.info("Wrote first and last name.");
            fw.write("Weka tree: " + System.lineSeparator());
            fw.write(wekaTree);
            log.info("Wrote Weka Tree.");
            fw.write("Watson Results: " + System.lineSeparator());
            fw.write(System.lineSeparator());
            fw.write("Needs: " + System.lineSeparator());

            ITrait trt = null;
            log.info("Native needs Lst size is: " + wResults.getLstNativeNeeds().size());
            log.info("Writting needs. i is: ");

            //Needs. We use .size method to avoid any IndexOutOfRange exceptions.
            for (int i = 0; i < wResults.getLstNativeNeeds().size(); i++) {
                log.info(i);
                trt = wResults.getLstNativeNeeds().get(i);
                fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
                fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
            }

            fw.write(System.lineSeparator());
            fw.write("Values:");
            fw.write(System.lineSeparator());

            log.info("Writing values");
            log.info("Native Values List size: " + wResults.getLstNativeValues().size());
            //Values
            for (int i = 0; i < wResults.getLstNativeValues().size(); i++) {
                log.info(i);
                trt = wResults.getLstNativeValues().get(i);
                fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
                fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
            }

            fw.write(System.lineSeparator());
            fw.write("B5:");
            fw.write(System.lineSeparator());
            //B5
            List<ITrait> chLst;
            log.info("Writing values");
            log.info("Native Values List size: " + wResults.getLstNativeValues().size());
            log.info("i is: ");
            for (int i = 0; i < wResults.getLstB5().size(); i++) {
                log.info(i);
                trt = wResults.getLstB5().get(i);
                fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                fw.write("\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
                fw.write("\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
                fw.write( "Facets:" + System.lineSeparator());

                //Writing the children.
                chLst = wResults.getLstB5().get(i).getChildren();
                log.info("Children size is: ");
                log.info("Children lst size is: " + chLst.size());
                for (int j = 0; j < chLst.size(); j++) {
                    trt = chLst.get(j);
                    fw.write("\t\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                    fw.write("\t\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                    fw.write("\t\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                    fw.write("\t\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
                    fw.write("\t\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
                }
                fw.write(System.lineSeparator());
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method used to export data to a file,
     * while logging operations are disabled.
     */
    private void exportWoLg() throws IOException {
        File file = new File(this.filePath);
        file.createNewFile();
        FileWriter fw = new FileWriter(file);
        fw.write("First Name: " + client.getFn() + System.lineSeparator());
        fw.write("Last Name: " + client.getLn() + System.lineSeparator());
        fw.write("Weka tree: " + System.lineSeparator());
        fw.write(wekaTree);
        fw.write("Watson Results: " + System.lineSeparator());
        fw.write(System.lineSeparator());
        fw.write("Needs: " + System.lineSeparator());

        ITrait trt = null;
        //Needs. We use .size method to avoid any IndexOutOfRange exceptions.
        for (int i = 0; i < wResults.getLstNativeNeeds().size(); i++) {
            trt = wResults.getLstNativeNeeds().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
        }

        fw.write(System.lineSeparator());
        fw.write("Values:");
        fw.write(System.lineSeparator());

        //Values
        for (int i = 0; i < wResults.getLstNativeValues().size(); i++) {
            trt = wResults.getLstNativeValues().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
        }

        fw.write(System.lineSeparator());
        fw.write("B5:");
        fw.write(System.lineSeparator());
        //B5
        List<ITrait> chLst;
        for (int i = 0; i < wResults.getLstB5().size(); i++) {
            trt = wResults.getLstB5().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
            fw.write( "Facets:" + System.lineSeparator());

            //Writing the children.
            chLst = wResults.getLstB5().get(i).getChildren();
            for (int j = 0; j < chLst.size(); j++) {
                trt = chLst.get(j);
                fw.write("\t\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                fw.write("\t\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                fw.write("\t\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                fw.write("\t\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
                fw.write("\t\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
            }
            fw.write(System.lineSeparator());
        }
        fw.flush();
        fw.close();
    }

    /**
     * Method used to export the needed data to the file
     * provided by the file path.
     * @param filePath The file path that the data will be exported to.
     */
    @Override
    public void exportFile(String filePath) throws IOException {
        if(logging == true) exportFileLg(filePath);
        else exportFileWoLg(filePath);
    }



    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are enabled.
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileLg(String filePath) {

    }



    /**
     * Method used to export data to a file,
     * indicated by the file path,
     * while logging operations are disabled.
     * @param filePath The file path that the file will be exported to.
     */
    private void exportFileWoLg(String filePath) throws IOException {
        File file = new File(filePath);
        file.createNewFile();
        FileWriter fw = new FileWriter(file);
        fw.write("First Name: " + client.getFn() + System.lineSeparator());
        fw.write("Last Name: " + client.getLn() + System.lineSeparator());
        fw.write("Weka tree: " + System.lineSeparator());
        fw.write(wekaTree);
        fw.write("Watson Results: " + System.lineSeparator());
        fw.write(System.lineSeparator());
        fw.write("Needs: " + System.lineSeparator());

        ITrait trt = null;
        //Needs. We use .size method to avoid any IndexOutOfRange exceptions.
        for (int i = 0; i < wResults.getLstNativeNeeds().size(); i++) {
            trt = wResults.getLstNativeNeeds().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
        }

        fw.write(System.lineSeparator());
        fw.write("Values:");
        fw.write(System.lineSeparator());

        //Values
        for (int i = 0; i < wResults.getLstNativeValues().size(); i++) {
            trt = wResults.getLstNativeValues().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Trait Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Trait Raw Score: " + trt.getRawScore() + System.lineSeparator());
        }

        fw.write(System.lineSeparator());
        fw.write("B5:");
        fw.write(System.lineSeparator());
        //B5
        List<ITrait> chLst;
        for (int i = 0; i < wResults.getLstB5().size(); i++) {
            trt = wResults.getLstB5().get(i);
            fw.write("\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
            fw.write("\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
            fw.write("\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
            fw.write("\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
            fw.write("\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
            fw.write( "Facets:" + System.lineSeparator());

            //Writing the children.
            chLst = wResults.getLstB5().get(i).getChildren();
            for (int j = 0; j < chLst.size(); j++) {
                trt = chLst.get(j);
                fw.write("\t\t" + "Trait Category: " + trt.getCategory() + System.lineSeparator());
                fw.write("\t\t" + "Trait Id: " + trt.getId() + System.lineSeparator());
                fw.write("\t\t" + "Trait Name: " + trt.getName() + System.lineSeparator());
                fw.write("\t\t" + "Percentile: " + trt.getPercentile() + System.lineSeparator());
                fw.write("\t\t" + "Raw Score: " + trt.getRawScore() + System.lineSeparator());
            }
            fw.write(System.lineSeparator());
        }
        fw.flush();
        fw.close();
    }
}
