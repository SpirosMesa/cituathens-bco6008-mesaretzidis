package com.mesaretzidis.spiros.WatsonAnalysis;

import com.ibm.watson.developer_cloud.personality_insights.v3.PersonalityInsights;
import com.ibm.watson.developer_cloud.service.exception.*;
import com.mesaretzidis.spiros.Entities.WatsonResults;
import com.mesaretzidis.spiros.Exceptions.LibraryException;
import com.mesaretzidis.spiros.WatsonAnalysis.AbstractClasses.AAnalysis;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;


/**
 * Class used in order to get the results of Watson's Personality Insights service.
 *
 * @author S. Mesaretzidis
 * @since 30/12/2016.
 */
public class TextAnalysis extends AAnalysis {
    private String defaultPst = "5h8FRC3BeYDU", defaultUsrNm = "428e2476-d441-43b0-bdd4-44390ad2ca59",
            psw = null, usr = null, userText = null;
    private Boolean log = false, useDefaultCredentials = false;
    private String warnings;


    /**
     * Class constructor, accepting all possible arguments.
     *
     * @param userName Personality Insights Username
     * @param password Personality Insights Password
     * @param userText User text meant to be analyzed.
     * @param enableLogging Variable indicating if there should be any logging.
     */
    public TextAnalysis(String userName, String password, String userText, boolean enableLogging){
        psw = password;
        usr = userName;
        this.userText = userText;
        log = enableLogging;
    }

    /**
     * Method used in order to get the results of Watson's Personality Insights service.
     *
     * @return A WatsonResults object, containing all the necessary results.
     * @throws LibraryException
     */
    public WatsonResults getResults() throws LibraryException {

        return getResultsWOLg();

       /* try {
            if (log == true) return getResultsWOLg();
            else return getResultsLg();
        }
        catch (LibraryException ex) {
            throw ex;
        }*/
    }

    /**
     * Private method used to get the results of Watson's Personality Insights with the logging functionality
     * disabled.
     *
     * @return A WatsonResults object containing the results of the analysis.
     * @throws LibraryException
     */
    private WatsonResults getResultsWOLg() throws LibraryException {
        PersonalityInsights pi = new PersonalityInsights(LocalDateTime.now().toString());

        WatsonResults wr = null;

        //Checking credentials
        try {
            if ((usr == null) || usr.isEmpty() || (psw == null) || psw.isEmpty()) {
                pi.setUsernameAndPassword(defaultUsrNm, defaultPst);
            }
            else {
                pi.setUsernameAndPassword(usr, psw);
            }

            //Setting tracking user usage off.
            Map<String, String> header = new HashMap<String, String>();
            header.put("X-Watson-Learning-Opt-Out", "true");
            pi.setDefaultHeaders(header);

            wr = new WatsonResults(pi.getProfile(userText).execute());
            return  wr;
        }
        catch (IllegalArgumentException ex) {
            throw ex;
        }
        catch(BadRequestException ex){
            throw new LibraryException(ex);
        }
        catch (UnauthorizedException ex){
            throw new LibraryException(ex);
        }
        catch (TooManyRequestsException ex){
            throw new LibraryException(ex);
        }
        catch (InternalServerErrorException ex) {
            throw new LibraryException(ex);
        }
        catch (ServiceResponseException ex) {
            throw new LibraryException(ex);
        }
    }

    /**
     * Private method used to get the results of Watson's Personality Insights with the logging functionality
     * enabled.
     *
     * @return A WatsonResults object containing the results of the analysis.
     * @throws LibraryException
     */
    private WatsonResults getResultsLg() throws LibraryException{
        PersonalityInsights pi = new PersonalityInsights(LocalDateTime.now().toString());

        //Checking credentials
        try {
            if (usr.equals(null) || usr.isEmpty() || psw.equals(null) || psw.isEmpty()) {
                pi.setUsernameAndPassword(defaultUsrNm, defaultPst);
            }
            else {
                pi.setUsernameAndPassword(usr, psw);
            }

            //Setting tracking user usage off.
            Map<String, String> header = new HashMap<String, String>();
            header.put("X-Watson-Learning-Opt-Out", "true");
            pi.setDefaultHeaders(header);

            return new WatsonResults(pi.getProfile(userText).execute());
        }
        catch (IllegalArgumentException ex) {
            throw ex;
        }
        catch(BadRequestException ex){
            throw new LibraryException(ex);
        }
        catch (UnauthorizedException ex){
            throw new LibraryException(ex);
        }
        catch (TooManyRequestsException ex){
            throw new LibraryException(ex);
        }
        catch (InternalServerErrorException ex) {
            throw new LibraryException(ex);
        }
        catch (ServiceResponseException ex) {
            throw new LibraryException(ex);
        }
        finally{
            return null;
        }
    }
}
