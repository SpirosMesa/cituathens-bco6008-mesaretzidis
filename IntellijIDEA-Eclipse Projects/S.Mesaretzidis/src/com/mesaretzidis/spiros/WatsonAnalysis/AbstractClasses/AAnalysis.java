package com.mesaretzidis.spiros.WatsonAnalysis.AbstractClasses;

import com.mesaretzidis.spiros.Exceptions.PasswordNotSetException;
import com.mesaretzidis.spiros.Exceptions.UsernameNotSetException;
import com.sun.istack.internal.Nullable;

/**
 * Abstract Class, used in order to define and implement common methods, used by a number of subclasses. The purpose of these
 * subclasses is to make use the corresponding Watson Service.
 *
 * @since 1/2/2017.
 * @author S. Mesaretzidis
 */
public abstract class AAnalysis {
    protected String defaultPst, defaultUsrNm, psw = null, usr = null, userText = null;
    protected Boolean log = false, useDefaultCredentials = false;
    protected String warnings = null;

    /**
     * @param userName The desired service, username.
     * @param password The desired service password.
     * @param enableLogging Boolean variable determining whether the loggin operations should be enabled.
     */
    public AAnalysis(@Nullable String userName,@Nullable String password, boolean enableLogging) {
        psw = password;
        usr = userName;
        log = enableLogging;
    }

    /**
     * Default class constructor.
     */
    public AAnalysis(){}

    /**
     * Method used in order to get any warnings that might have been produced.
     *
     * @return A String containing all of the generated warnings.
     */
    public String getWarnings(){ return  warnings;}
    /**
     * Method used in order to define whether the default credentials should be used.
     * @param dft Variable indicating whether the default credentials should be used.
     */
    public void useDefaultCredentials(boolean dft){
        useDefaultCredentials = dft;
    }

    /**
     * Method used in order set the Personality Insights services credentials.
     * @param username The username to be used by the service.
     * @param password The password to be used by the service.
     * @throws com.mesaretzidis.spiros.Exceptions.PasswordNotSetException Thrown if the password is not set.
     * @throws com.mesaretzidis.spiros.Exceptions.UsernameNotSetException Thrown if the username is not set.
     */
    public void setCredentials(String username, String password) throws PasswordNotSetException, UsernameNotSetException {
        if (username.isEmpty() || username.equals(null)) throw new UsernameNotSetException();
        else usr = username;

        if (password.isEmpty() || password.equals(null)) throw new PasswordNotSetException();
        else psw = password;
    }

    /**
     * Method used in order to define whether the user would like logging to be enabled.
     * @param log
     */
    public void setLogging(boolean log) {this.log = log;}


}
