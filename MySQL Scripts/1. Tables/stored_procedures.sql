use bco_6008_tests;

drop procedure if exists getClientWatsonResults;
drop procedure if exists get_watson_values;
drop procedure if exists get_watson_needs;
drop procedure if exists getB5Open;
drop procedure if exists getB5Extra;
drop procedure if exists getB5ER;
drop procedure if exists getB5Con;
drop procedure if exists getB5Agree;


Delimiter //

Create definer = current_user procedure get_watson_values(in id int(10)) 
Begin		
		select watson_values_self_transcendence.percentile, watson_values_self_transcendence.raw_score,
		watson_values_hedonism.percentile, watson_values_hedonism.raw_score,
		watson_values_open_to_change.percentile, watson_values_open_to_change.raw_score,
		watson_values_conservation.percentile, watson_values_conservation.raw_score,
		watson_values_self_enhancement.percentile, watson_values_self_enhacement.raw_score 
		from watson_values 
		join watson_values_self_transcendence 
		join watson_values_hedonism
		join watson_values_open_to_change
		join watson_values_conservation
		join watson_values_self_enhancement
		where watson_values.id = id
		and watson_values.fk_self_transcendence_id = watson_values_self_transcendence.id
		and watson_values.fk_hedonism_id = watson_values_hedonism.id
		and watson_values.fk_open_to_change_id = watson_values_open_to_change.id
		and watson_values.fk_conservation_id = watson_values_conservation.id
		and watson_values.fk_self_enhancement_id = watson_values_self_enhancement.id;
End// 

Create definer = current_user procedure get_watson_needs(in id int(10))
Begin
	select watson_needs_excitement.percentile, watson_needs_excitement.raw_score,
	watson_needs_harmony.percentile, watson_needs_harmony.raw_score,
	watson_needs_curiosity.percentile, watson_needs_curiosity.raw_score,
	watson_needs_liberty.percentile, watson_needs_liberty.raw_score,
	watson_needs_practicality.percentile, watson_needs_practicality.raw_score,
	watson_needs_challenge.percentile, watson_needs_challenge.raw_score,
	watson_needs_ideal.percentile, watson_needs_ideal.raw_score,
	watson_needs_closeness.percentile, watson_needs_closeness.raw_score,
	watson_needs_self_expression.percentile, watson_needs_self_expression.raw_score,
	watson_needs_love.percentile, watson_needs_love.raw_score,
	watson_needs_stability.percentile, watson_needs_stability.raw_score,
	watson_needs_structure.percentile, watson_needs_structure.raw_score
	from watson_needs
	join watson_needs_excitement
	join watson_needs_harmony
	join watson_needs_curiosity
	join watson_needs_liberty
	join watson_needs_practicality
	join watson_needs_challenge
	join watson_needs_ideal
	join watson_needs_closeness
	join watson_needs_self_expression
	join watson_needs_love
	join watson_needs_stability
	join watson_needs_structure
	where watson_needs.id = id 
	and watson_needs.fk_excitement_id = watson_needs_excitement.id
	and watson_needs.fk_harmony_id = watson_needs_harmony.id
	and watson_needs.fk_curiosity_id = watson_needs_curiosity.id
	and watson_needs.fk_liberty_id = watson_needs_liberty.id
	and watson_needs.fk_practicality_id = watson_needs_practicality.id
	and watson_needs.fk_challenge_id = watson_needs_challenge.id
	and watson_needs.fk_ideal_id = watson_needs_ideal.id
	and watson_needs.fk_closeness_id = watson_needs_closeness.id
	and watson_needs.fk_self_expression_id = watson_needs_self_expression.id
	and watson_needs.fk_love_id = watson_needs_love.id
	and watson_needs.fk_stability_id = watson_needs_stability.id
	and watson_needs.fk_structure_id = watson_needs_structure.id;
End//

Create definer = current_user procedure getB5Open(in id int(10)) 
Begin
	select big_5_openness_adventurousness.percentile, big_5_openness_adventurousness.raw_score,
	big_5_openness_artistic_interests.percentile, big_5_openness_artistic_interests.raw_score,
	big_5_openness_emotionality.percentile, big_5_openness_emotionality.raw_score,
	big_5_openness_imagination.percentile, big_5_openness_imagination.raw_score,
	big_5_openness_intellect.percentile, big_5_openness_intellect.raw_score,
	big_5_openness_liberalism.percentile, big_5_openness_liberalism.raw_score
	from big_5_openness
	join big_5_openness_adventurousness
	join big_5_openness_artistic_interests
	join big_5_openness_emotionality
	join big_5_openness_imagination
	join big_5_openness_intellect
	join big_5_openness_liberalism
	where big_5_openness.id = id
	And big_5_openness.fk_adventurousness_id = big_5_openness_adventurousness.id
	And big_5_openness.fk_artistic_interests_id = big_5_openness_artistic_interests.id
	and big_5_openness.fk_emotionality_id = big_5_openness_emotionality.id
	and big_5_openness.fk_imagination_id = big_5_openness_imagination.id
	and big_5_openness.fk_intellect_id = big_5_openness_intellect.id
	and big_5_openness.fk_liberalism_id = big_5_openness_liberalism.id;
End//

Create definer = current_user procedure getB5Extra(in id int(10))
Begin
	select big_5_extraversion_activity_level.percentile, big_5_extraversion_activity_level.raw_score,
	big_5_extraversion_assertiveness.percentile, big_5_extraversion_assertiveness.raw_score,
	big_5_extraversion_cheerfulness.percentile, big_5_extraversion_cheerfulness.raw_score,
	big_5_extraversion_excitement_seeking.percentile, big_5_extraversion_excitement_seeking.raw_score,
	big_5_extraversion_friendliness.percentile, big_5_extraversion_friendliness.raw_score,
	big_5_extraversion_gregariousness.percentile, big_5_extraversion_gregariousness.raw_score
	from big_5_extraversion
	join big_5_extraversion_activity_level
	join big_5_extraversion_assertiveness
	join big_5_extraversion_cheerfulness
	join big_5_extraversion_excitement_seeking
	join big_5_extraversion_friendliness
	join big_5_extraversion_gregariousness
	where big_5_extraversion.id = id
	And big_5_extraversion.fk_activity_level_id = big_5_extraversion_activity_level.id
	And big_5_extraversion.fk_assertiveness_id = big_5_extraversion_assertiveness.id
	and big_5_extraversion.fk_cheerfulness_id = big_5_extraversion_cheerfulness.id
	and big_5_extraversion.fk_excitement_seeking_id = big_5_extraversion_excitement_seeking.id
	and big_5_extraversion.fk_friendliness_id = big_5_extraversion_friendliness.id
	and big_5_extraversion.fk_gregariousness_id = big_5_extraversion_gregariousness.id;
End//

Create definer = current_user procedure getB5ER (in id int(10))
Begin
	select big_5_emotional_range_anger.percentile, big_5_emotional_range_anger.raw_score,
	big_5_emotional_range_anxiety.percentile, big_5_emotional_range_anxiety.raw_score,
	big_5_emotional_range_depression.percentile, big_5_emotional_range_depression.raw_score,
	big_5_emotional_range_self_consciousness.percentile, big_5_emotional_range_self_consciousness.raw_score,
	big_5_emotional_range_vulnerability.percentile, big_5_emotional_range_vulnerability.raw_score,
	big_5_emotional_range_immoderation.percentile, big_5_emotional_range_immoderation.raw_score
	from big_5_emotional_range
	join big_5_emotional_range_anger
	join big_5_emotional_range_anxiety
	join big_5_emotional_range_depression
	join big_5_emotional_range_self_consciousness
	join big_5_emotional_range_vulnerability
	join big_5_emotional_range_immoderation
	where big_5_emotional_range.id = id
	And big_5_emotional_range.fk_anger_id = big_5_emotional_range_anger.id
	And big_5_emotional_range.fk_anxiety_id = big_5_emotional_range_anxiety.id
	and big_5_emotional_range.fk_depression_id = big_5_emotional_range_depression.id
	and big_5_emotional_range.fk_self_consciousness_id = big_5_emotional_range_self_consciousness.id
	and big_5_emotional_range.fk_vulnerability_id = big_5_emotional_range_vulnerability.id
	and big_5_emotional_range.fk_immoderation_id = big_5_emotional_range_immoderation.id;
End//

Create definer = current_user procedure getB5Con(in id int(10))
Begin
	select big_5_conscientiousness_dutifulness.percentile, big_5_conscientiousness_dutifulness.raw_score,
	big_5_conscientiousness_cautiousness.percentile, big_5_conscientiousness_cautiousness.raw_score,
	big_5_conscientiousness_achievement_striving.percentile, big_5_conscientiousness_achievement_striving.raw_score,
	big_5_conscientiousness_self_efficacy.percentile, big_5_conscientiousness_self_efficacy.raw_score,
	big_5_conscientiousness_self_discipline.percentile, big_5_conscientiousness_self_discipline.raw_score,
	big_5_conscientiousness_orderliness.percentile, big_5_conscientiousness_orderliness.raw_score
	from big_5_conscientiousness
	join big_5_conscientiousness_dutifulness
	join big_5_conscientiousness_cautiousness
	join big_5_conscientiousness_achievement_striving
	join big_5_conscientiousness_self_efficacy
	join big_5_conscientiousness_self_discipline
	join big_5_conscientiousness_orderliness
	where big_5_conscientiousness.id = id
	And big_5_conscientiousness.fk_dutifulness_id = big_5_conscientiousness_dutifulness.id
	And big_5_conscientiousness.fk_cautiousness_id = big_5_conscientiousness_cautiousness.id
	and big_5_conscientiousness.fk_achievement_striving_id = big_5_conscientiousness_achievement_striving.id
	and big_5_conscientiousness.fk_self_efficacy_id = big_5_conscientiousness_self_efficacy.id
	and big_5_conscientiousness.fk_self_discipline_id = big_5_conscientiousness_self_discipline.id
	and big_5_conscientiousness.fk_orderliness_id = big_5_conscientiousness_orderliness.id;
End//

Create definer = current_user procedure getB5Agree (in id int(10))
Begin
	select big_5_agreeableness_cooperation.percentile, big_5_agreeableness_cooperation.raw_score,
	big_5_agreeableness_morality.percentile, big_5_agreeableness_morality.raw_score,
	big_5_agreeableness_trust.percentile, big_5_agreeableness_trust.raw_score,
	big_5_agreeableness_altruism.percentile, big_5_agreeableness_altruism.raw_score,
	big_5_agreeableness_modesty.percentile, big_5_agreeableness_modesty.raw_score,
	big_5_agreeableness_sympathy.percentile, big_5_agreeableness_sympathy.raw_score
	from big_5_agreeableness
	join big_5_agreeableness_cooperation
	join big_5_agreeableness_morality
	join big_5_agreeableness_trust
	join big_5_agreeableness_altruism
	join big_5_agreeableness_modesty
	join big_5_agreeableness_sympathy
	where big_5_agreeableness.id = id
	And big_5_agreeableness.fk_cooperation_id = big_5_agreeableness_cooperation.id
	And big_5_agreeableness.fk_morality_id = big_5_agreeableness_morality.id
	and big_5_agreeableness.fk_trust_id = big_5_agreeableness_trust.id
	and big_5_agreeableness.fk_altruism_id = big_5_agreeableness_altruism.id
	and big_5_agreeableness.fk_modesty_id = big_5_agreeableness_modesty.id
	and big_5_agreeableness.fk_sympathy_id = big_5_agreeableness_sympathy.id;
End//

Create definer = current_user procedure getClientWatsonResults(in id int(10)) 
Begin
	Declare watson_results_id, watson_values_id, watson_needs_id, big_5 int;
    
    /* get watson results id. */
    select fk_watson_results_id from client_texts where fk_app_client_id = id into watson_results_id;


	select fk_big_five_id from watson_results where id = watson_results_id into big_5;
    
    select fk_needs_id from watson_results where id = watson_reuslts_id into watson_needs_id;
    
    
    
    
    
	

End//