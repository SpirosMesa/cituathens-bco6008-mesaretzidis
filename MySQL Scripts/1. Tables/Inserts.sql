/***
 *    ____    ____  ___       __       __    __   _______     _______.
 *    \   \  /   / /   \     |  |     |  |  |  | |   ____|   /       |
 *     \   \/   / /  ^  \    |  |     |  |  |  | |  |__     |   (----`
 *      \      / /  /_\  \   |  |     |  |  |  | |   __|     \   \    
 *       \    / /  _____  \  |  `----.|  `--'  | |  |____.----)   |   
 *        \__/ /__/     \__\ |_______| \______/  |_______|_______/    
 *                                                                    
 */
 use bco_6008_tests;
 
 insert into watson_values_self_transcendence(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_values_hedonism(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_values_open_to_change(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_values_conservation(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_values_self_enhancement(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_values(fk_self_transcendence_id, fk_conservation_id, fk_hedonism_id, fk_self_enhancement_id, fk_open_to_change_id)
 values (1, 1, 1, 1, 1) , (2, 2, 2, 2, 2);
 
 /***
 *    .__   __.  _______  _______  _______       _______.
 *    |  \ |  | |   ____||   ____||       \     /       |
 *    |   \|  | |  |__   |  |__   |  .--.  |   |   (----`
 *    |  . `  | |   __|  |   __|  |  |  |  |    \   \    
 *    |  |\   | |  |____ |  |____ |  '--'  |.----)   |   
 *    |__| \__| |_______||_______||_______/ |_______/    
 *                                                       
 */
 
 insert into watson_needs_excitement(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_harmony(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_curiosity(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_liberty(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_ideal(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_closeness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_self_expression(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_love(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_practicality(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_stability(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_challenge(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs_structure(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into watson_needs(fk_excitement_id, fk_harmony_id, fk_curiosity_id, fk_liberty_id, fk_ideal_id, fk_closeness_id, fk_self_expression_id, fk_love_id, fk_practicality_id,
	fk_stability_id, fk_challenge_id, fk_structure_id)
 values (1,1,1,1,1,1,1,1,1,1,1,1), (2,2,2,2,2,2,2,2,2,2,2,2), (3,3,3,3,3,3,3,3,3,3,3,3);
 
 /***
 *    .______    _____      ______   .______    _______ .__   __. 
 *    |   _  \  | ____|    /  __  \  |   _  \  |   ____||  \ |  | 
 *    |  |_)  | | |__     |  |  |  | |  |_)  | |  |__   |   \|  | 
 *    |   _  <  |___ \    |  |  |  | |   ___/  |   __|  |  . `  | 
 *    |  |_)  |  ___) |   |  `--'  | |  |      |  |____ |  |\   | 
 *    |______/  |____/     \______/  | _|      |_______||__| \__| 
 *                                                                
 */
 
 insert into big_5_openness_adventurousness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_openness_artistic_interests(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
  insert into big_5_openness_emotionality(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
  insert into big_5_openness_artistic_interests(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
  insert into big_5_openness_imagination(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_openness_intellect(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_openness_liberalism(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_openness(percentile, raw_score, fk_adventurousness_id, fk_artistic_interests_id, fk_emotionality_id, fk_imagination_id, fk_intellect_id, fk_liberalism_id)
 values (0.123456789123, 0.123456789123, 1 ,1, 1, 1, 1, 1) , (0.123456789123, 0.123456789123, 2, 2, 2, 2, 2, 2), (0.123456789123, 0.123456789123, 3, 3, 3, 3, 3, 3);
 
 /***
 *    .______    _____     __________   ___ .___________..______          ___      
 *    |   _  \  | ____|   |   ____\  \ /  / |           ||   _  \        /   \     
 *    |  |_)  | | |__     |  |__   \  V  /  `---|  |----`|  |_)  |      /  ^  \    
 *    |   _  <  |___ \    |   __|   >   <       |  |     |      /      /  /_\  \   
 *    |  |_)  |  ___) |   |  |____ /  .  \      |  |     |  |\  \----./  _____  \  
 *    |______/  |____/    |_______/__/ \__\     |__|     | _| `._____/__/     \__\ 
 *                                                                                 
 */
 
 insert into big_5_extraversion_activity_level(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion_assertiveness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion_cheerfulness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion_excitement_seeking(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion_friendliness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion_gregariousness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_extraversion(percentile, raw_score, fk_activity_level_id, fk_assertiveness_id, fk_cheerfulness_id, fk_excitement_seeking_id, fk_friendliness_id, fk_gregariousness_id)
 values (0.123456789123, 0.123456789123, 1, 1, 1, 1, 1, 1), (0.123456789123, 0.123456789123, 2, 2, 2, 2, 2, 2), (0.123456789123, 0.123456789123, 3, 3, 3, 3, 3, 3);
 
 /***
 *    .______    _____     _______ .______      
 *    |   _  \  | ____|   |   ____||   _  \     
 *    |  |_)  | | |__     |  |__   |  |_)  |    
 *    |   _  <  |___ \    |   __|  |      /     
 *    |  |_)  |  ___) |   |  |____ |  |\  \----.
 *    |______/  |____/    |_______|| _| `._____|
 *                                              
 */
 
 insert into big_5_emotional_range_anger(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range_anxiety(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range_depression(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range_self_consciousness(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range_vulnerability(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range_immoderation(percentile, raw_score)
 values (0.123456789123, 0.123456789123) , (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_emotional_range(percentile, raw_score, fk_anger_id, fk_anxiety_id, fk_depression_id, fk_self_consciousness_id, fk_vulnerability_id, fk_immoderation_id)
 values (0.123456789123, 0.123456789123, 1, 1, 1, 1, 1, 1), (0.123456789123, 0.123456789123, 2, 2, 2, 2, 2, 2) , (0.123456789123, 0.123456789123, 3,3,3,3,3,3);
 
 /***
 *    .______    _____      ______   ______   .__   __.      _______.  ______ 
 *    |   _  \  | ____|    /      | /  __  \  |  \ |  |     /       | /      |
 *    |  |_)  | | |__     |  ,----'|  |  |  | |   \|  |    |   (----`|  ,----'
 *    |   _  <  |___ \    |  |     |  |  |  | |  . `  |     \   \    |  |     
 *    |  |_)  |  ___) |   |  `----.|  `--'  | |  |\   | .----)   |   |  `----.
 *    |______/  |____/     \______| \______/  |__| \__| |_______/     \______|
 *                                                                            
 */
 
 insert into big_5_conscientiousness_dutifulness(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness_cautiousness(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness_achievement_striving(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness_self_efficacy(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness_self_discipline(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness_orderliness(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_conscientiousness(percentile, raw_score, fk_self_efficacy_id, fk_self_discipline_id, fk_orderliness_id, fk_dutifulness_id, fk_cautiousness_id, fk_achievement_striving_id)
 values (0.123456789123, 0.123456789123, 1, 1, 1, 1, 1, 1), (0.123456789123, 0.123456789123, 2, 2, 2, 2, 2, 2), (0.123456789123, 0.123456789123,3 ,3 ,3 ,3 ,3 ,3);
 
 /***
 *    .______    _____         ___       _______ .______       _______ 
 *    |   _  \  | ____|       /   \     /  _____||   _  \     |   ____|
 *    |  |_)  | | |__        /  ^  \   |  |  __  |  |_)  |    |  |__   
 *    |   _  <  |___ \      /  /_\  \  |  | |_ | |      /     |   __|  
 *    |  |_)  |  ___) |    /  _____  \ |  |__| | |  |\  \----.|  |____ 
 *    |______/  |____/    /__/     \__\ \______| | _| `._____||_______|
 *                                                                     
 */
 
 insert into big_5_agreeableness_cooperation(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness_morality(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness_trust(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness_altruism(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness_modesty(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness_sympathy(percentile, raw_score)
 values (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123), (0.123456789123, 0.123456789123);
 
 insert into big_5_agreeableness(percentile, raw_score, fk_altruism_id, fk_cooperation_id, fk_modesty_id, fk_morality_id, fk_sympathy_id, fk_trust_id)
 values (0.123456789123, 0.123456789123, 1, 1, 1, 1, 1, 1), (0.123456789123, 0.123456789123, 2, 2, 2, 2, 2, 2), (0.123456789123, 0.123456789123, 3, 3, 3, 3, 3, 3);
 
 /***
 *    .______    _____    
 *    |   _  \  | ____|   
 *    |  |_)  | | |__     
 *    |   _  <  |___ \    
 *    |  |_)  |  ___) |   
 *    |______/  |____/    
 *                        
 */
 
 insert into big_five(fk_big_5_openness_id, fk_big_5_agreeableness_id, fk_big_5_extraversion_id, fk_big_5_emotional_range_id, fk_big_5_conscientiousness_id)
 values (1, 1, 1, 1, 1), (2, 2, 2, 2, 2);
 
 /***
 *    ____    __    ____  ___   .___________.    _______.  ______   .__   __.   .______       _______     _______. __    __   __      .___________.    _______.
 *    \   \  /  \  /   / /   \  |           |   /       | /  __  \  |  \ |  |   |   _  \     |   ____|   /       ||  |  |  | |  |     |           |   /       |
 *     \   \/    \/   / /  ^  \ `---|  |----`  |   (----`|  |  |  | |   \|  |   |  |_)  |    |  |__     |   (----`|  |  |  | |  |     `---|  |----`  |   (----`
 *      \            / /  /_\  \    |  |        \   \    |  |  |  | |  . `  |   |      /     |   __|     \   \    |  |  |  | |  |         |  |        \   \    
 *       \    /\    / /  _____  \   |  |    .----)   |   |  `--'  | |  |\   |   |  |\  \----.|  |____.----)   |   |  `--'  | |  `----.    |  |    .----)   |   
 *        \__/  \__/ /__/     \__\  |__|    |_______/     \______/  |__| \__|   | _| `._____||_______|_______/     \______/  |_______|    |__|    |_______/    
 *                                                                                                                                                             
 */
 
 insert into watson_results(fk_big_five_id, fk_needs_id, fk_values_id)
 values(1, 1, 1), (2, 2, 2);
 
 insert into app_client(fn, ln, active)
 values ('George', 'Lucas', 1), ('Walt', 'Disney', 1);
 
 insert into weka_results(tree)
 values ('001010110010100101010010010010101'), ('10010100111010010101001010101011'), ('10010101100101110100101010010101010010100101');
 
 insert into client_texts(fk_app_client_id, fk_watson_results_id, fk_weka_results_id, client_text)
 values (1, 1, 1, 'Star Wars was awesome'), (2,2,2, 'My company destroyed Star Wars');