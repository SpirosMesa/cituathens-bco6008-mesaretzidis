
/***
 *      ______ .______     .___________.    _______  .______   
 *     /      ||   _  \    |           |   |       \ |   _  \  
 *    |  ,----'|  |_)  |   `---|  |----`   |  .--.  ||  |_)  | 
 *    |  |     |      /        |  |        |  |  |  ||   _  <  
 *    |  `----.|  |\  \----.   |  |        |  '--'  ||  |_)  | 
 *     \______|| _| `._____|   |__|        |_______/ |______/  
 *                                                             
 */
drop database if exists bco_6008_tests;

create database bco_6008_tests;

use bco_6008_tests;

/***
 *    ____    ____  ___       __       __    __   _______     _______.
 *    \   \  /   / /   \     |  |     |  |  |  | |   ____|   /       |
 *     \   \/   / /  ^  \    |  |     |  |  |  | |  |__     |   (----`
 *      \      / /  /_\  \   |  |     |  |  |  | |   __|     \   \    
 *       \    / /  _____  \  |  `----.|  `--'  | |  |____.----)   |   
 *        \__/ /__/     \__\ |_______| \______/  |_______|_______/    
 *                                                                    
 */
create table watson_values_self_transcendence (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_values_hedonism (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_values_open_to_change (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_values_conservation (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_values_self_enhancement (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_values (
  id int not null auto_increment primary key,
  fk_self_transcendence_id int ,
  fk_conservation_id int not null,
  fk_hedonism_id int not null,
  fk_self_enhancement_id int not null,
  fk_open_to_change_id int not null,
  
  index(id),
  
  foreign key (fk_self_transcendence_id)
	references watson_values_self_transcendence(id)
    on delete cascade,

  foreign key (fk_conservation_id)
	references watson_values_conservation(id)
     on delete cascade,
     
  foreign key(fk_hedonism_id)
	references watson_values_hedonism(id)
    on delete cascade,
    
  foreign key(fk_self_enhancement_id)
	references watson_values_self_enhancement(id)
    on delete cascade,
    
  foreign key(fk_open_to_change_id)
	references watson_values_open_to_change(id)
    on delete cascade
);

/***
 *    .__   __.  _______  _______  _______       _______.
 *    |  \ |  | |   ____||   ____||       \     /       |
 *    |   \|  | |  |__   |  |__   |  .--.  |   |   (----`
 *    |  . `  | |   __|  |   __|  |  |  |  |    \   \    
 *    |  |\   | |  |____ |  |____ |  '--'  |.----)   |   
 *    |__| \__| |_______||_______||_______/ |_______/    
 *                                                       
 */

create table watson_needs_excitement (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_harmony (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_curiosity (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_liberty (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_ideal (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_closeness (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_self_expression (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_love (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_practicality (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_stability (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_challenge (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs_structure (
  id int not null auto_increment Primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
);

create table watson_needs(
 id int not null auto_increment primary key,
 fk_excitement_id int not null,
 fk_harmony_id int not null,
 fk_curiosity_id int not null,
 fk_liberty_id int not null,
 fk_ideal_id int not null,
 fk_closeness_id int not null,
 fk_self_expression_id int not null,
 fk_love_id int not null,
 fk_practicality_id int not null,
 fk_stability_id int not null,
 fk_challenge_id int not null,
 fk_structure_id int not null,
 
 foreign key (fk_excitement_id)
	references watson_needs_excitement(id)
    on delete cascade,

 foreign key (fk_harmony_id)
	references watson_needs_harmony(id)
    on delete cascade,
    
 foreign key (fk_curiosity_id)
	references watson_needs_curiosity(id)
    on delete cascade,
    
 foreign key (fk_liberty_id)
	references watson_needs_liberty(id)
    on delete cascade,
    
 foreign key (fk_ideal_id)
	references watson_needs_ideal(id)
    on delete cascade,

 foreign key (fk_closeness_id)
	references watson_needs_closeness(id)
    on delete cascade,
    
 foreign key (fk_self_expression_id)
	references watson_needs_self_expression(id)
    on delete cascade,
    
 foreign key (fk_love_id)
	references watson_needs_love(id)
    on delete cascade,
    
 foreign key (fk_practicality_id)
	references watson_needs_practicality(id)
    on delete cascade,
    
 foreign key (fk_stability_id)
	references watson_needs_stability(id)
    on delete cascade,
    
 foreign key (fk_challenge_id)
	references watson_needs_challenge(id)
    on delete cascade,
    
 foreign key (fk_structure_id)
	references watson_needs_structure(id)
    on delete cascade, 
    
 index(id)
);

/***
 *    .______    __    _______     _____  
 *    |   _  \  |  |  /  _____|   | ____| 
 *    |  |_)  | |  | |  |  __     | |__   
 *    |   _  <  |  | |  | |_ |    |___ \  
 *    |  |_)  | |  | |  |__| |     ___) | 
 *    |______/  |__|  \______|    |____/  
 *                                        
 */
 
/***
 *    .______    _____       ______   .______    _______ .__   __. .__   __.  _______     _______.     _______.
 *    |   _  \  | ____|     /  __  \  |   _  \  |   ____||  \ |  | |  \ |  | |   ____|   /       |    /       |
 *    |  |_)  | | |__      |  |  |  | |  |_)  | |  |__   |   \|  | |   \|  | |  |__     |   (----`   |   (----`
 *    |   _  <  |___ \     |  |  |  | |   ___/  |   __|  |  . `  | |  . `  | |   __|     \   \        \   \    
 *    |  |_)  |  ___) |    |  `--'  | |  |      |  |____ |  |\   | |  |\   | |  |____.----)   |   .----)   |   
 *    |______/  |____/      \______/  | _|      |_______||__| \__| |__| \__| |_______|_______/    |_______/    
 *                                                                                                             
 */
 
 create table big_5_openness_adventurousness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness_artistic_interests (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness_emotionality (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness_imagination (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness_intellect (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness_liberalism (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_openness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  fk_adventurousness_id int not null,
  fk_artistic_interests_id int not null,
  fk_emotionality_id int not null,
  fk_imagination_id int not null,
  fk_intellect_id int not null,
  fk_liberalism_id int not null,
  
  foreign key (fk_adventurousness_id)
	references big_5_openness_adventurousness(id)
    on delete cascade,
    
  foreign key (fk_artistic_interests_id) 
	references big_5_openness_artistic_interests(id)
    on delete cascade,
        
  foreign key (fk_emotionality_id)
	references big_5_openness_emotionality(id)
    on delete cascade,
    
  foreign key (fk_imagination_id)
	 references big_5_openness_imagination(id)
     on delete cascade,
     
  foreign key (fk_intellect_id)
	references big_5_openness_intellect(id)
    on delete cascade,
    
  foreign key (fk_liberalism_id)
	references big_5_openness_liberalism(id)
    on delete cascade,
    
  index(id)
 );
 
/***
 *    .______    _____     __________   ___ .___________..______          ___   ____    ____  _______ .______          _______. __    ______   .__   __. 
 *    |   _  \  | ____|   |   ____\  \ /  / |           ||   _  \        /   \  \   \  /   / |   ____||   _  \        /       ||  |  /  __  \  |  \ |  | 
 *    |  |_)  | | |__     |  |__   \  V  /  `---|  |----`|  |_)  |      /  ^  \  \   \/   /  |  |__   |  |_)  |      |   (----`|  | |  |  |  | |   \|  | 
 *    |   _  <  |___ \    |   __|   >   <       |  |     |      /      /  /_\  \  \      /   |   __|  |      /        \   \    |  | |  |  |  | |  . `  | 
 *    |  |_)  |  ___) |   |  |____ /  .  \      |  |     |  |\  \----./  _____  \  \    /    |  |____ |  |\  \----.----)   |   |  | |  `--'  | |  |\   | 
 *    |______/  |____/    |_______/__/ \__\     |__|     | _| `._____/__/     \__\  \__/     |_______|| _| `._____|_______/    |__|  \______/  |__| \__| 
 *                                                                                                                                                       
 */
 
 create table big_5_extraversion_activity_level(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion_assertiveness(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion_cheerfulness(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion_excitement_seeking(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion_friendliness(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion_gregariousness(
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_extraversion (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null, 
  fk_activity_level_id int not null,
  fk_assertiveness_id int not null,
  fk_cheerfulness_id int not null,
  fk_excitement_seeking_id int not null,
  fk_gregariousness_id int not null,
  fk_friendliness_id int not null,
  
  foreign key (fk_activity_level_id) 
	references big_5_extraversion_activity_level(id)
    on delete cascade,
    
  foreign key (fk_assertiveness_id)
	references big_5_extraversion_assertiveness(id)
    on delete cascade,
    
  foreign key (fk_cheerfulness_id)
	references big_5_extraversion_cheerfulness(id)
    on delete cascade,
    
  foreign key (fk_excitement_seeking_id)
	references big_5_extraversion_excitement_seeking(id)
    on delete cascade,

  foreign key (fk_gregariousness_id)
	references big_5_extraversion_gregariousness(id)
    on delete cascade,
    
  foreign key (fk_friendliness_id)
	references big_5_extraversion_friendliness(id)
	on delete cascade,
    
  index(id)
 );
 
 /***
 *    .______    _____     _______ .___  ___.   ______   .___________. __    ______   .__   __.      ___       __         .______          ___      .__   __.   _______  _______ 
 *    |   _  \  | ____|   |   ____||   \/   |  /  __  \  |           ||  |  /  __  \  |  \ |  |     /   \     |  |        |   _  \        /   \     |  \ |  |  /  _____||   ____|
 *    |  |_)  | | |__     |  |__   |  \  /  | |  |  |  | `---|  |----`|  | |  |  |  | |   \|  |    /  ^  \    |  |        |  |_)  |      /  ^  \    |   \|  | |  |  __  |  |__   
 *    |   _  <  |___ \    |   __|  |  |\/|  | |  |  |  |     |  |     |  | |  |  |  | |  . `  |   /  /_\  \   |  |        |      /      /  /_\  \   |  . `  | |  | |_ | |   __|  
 *    |  |_)  |  ___) |   |  |____ |  |  |  | |  `--'  |     |  |     |  | |  `--'  | |  |\   |  /  _____  \  |  `----.   |  |\  \----./  _____  \  |  |\   | |  |__| | |  |____ 
 *    |______/  |____/    |_______||__|  |__|  \______/      |__|     |__|  \______/  |__| \__| /__/     \__\ |_______|   | _| `._____/__/     \__\ |__| \__|  \______| |_______|
 *                                                                                                                                                                               
 */
 
 create table big_5_emotional_range_anger (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range_anxiety (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range_depression (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range_self_consciousness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range_vulnerability (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range_immoderation (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id)
 );
 
 create table big_5_emotional_range (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  fk_anger_id int not null,
  fk_anxiety_id int not null,
  fk_depression_id int not null,
  fk_self_consciousness_id int not null,
  fk_vulnerability_id int not null,
  fk_immoderation_id int not null,
  
  foreign key (fk_anger_id)
	references big_5_emotional_range_anger(id)
    on delete cascade,
    
  foreign key (fk_anxiety_id)
    references big_5_emotional_range_anxiety(id)
    on delete cascade,
    
  foreign key (fk_depression_id)
    references big_5_emotional_range_depression(id)
    on delete cascade,
    
  foreign key (fk_self_consciousness_id)
    references big_5_emotional_range_self_consciousness(id)
    on delete cascade,
    
  foreign key (fk_vulnerability_id)
    references big_5_emotional_range_vulnerability(id)
    on delete cascade,
    
  foreign key (fk_immoderation_id)
    references big_5_emotional_range_immoderation(id)
    on delete cascade,
    
  index (id)
 );
 
 /***
 *    .______    _____      ______   ______   .__   __.      _______.  ______  __   _______ .__   __. .___________. __    ______    __    __       _______..__   __.  _______     _______.     _______.
 *    |   _  \  | ____|    /      | /  __  \  |  \ |  |     /       | /      ||  | |   ____||  \ |  | |           ||  |  /  __  \  |  |  |  |     /       ||  \ |  | |   ____|   /       |    /       |
 *    |  |_)  | | |__     |  ,----'|  |  |  | |   \|  |    |   (----`|  ,----'|  | |  |__   |   \|  | `---|  |----`|  | |  |  |  | |  |  |  |    |   (----`|   \|  | |  |__     |   (----`   |   (----`
 *    |   _  <  |___ \    |  |     |  |  |  | |  . `  |     \   \    |  |     |  | |   __|  |  . `  |     |  |     |  | |  |  |  | |  |  |  |     \   \    |  . `  | |   __|     \   \        \   \    
 *    |  |_)  |  ___) |   |  `----.|  `--'  | |  |\   | .----)   |   |  `----.|  | |  |____ |  |\   |     |  |     |  | |  `--'  | |  `--'  | .----)   |   |  |\   | |  |____.----)   |   .----)   |   
 *    |______/  |____/     \______| \______/  |__| \__| |_______/     \______||__| |_______||__| \__|     |__|     |__|  \______/   \______/  |_______/    |__| \__| |_______|_______/    |_______/    
 *                                                                                                                                                                                                     
 */
 
 create table big_5_conscientiousness_dutifulness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
  
 create table big_5_conscientiousness_cautiousness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
  
 create table big_5_conscientiousness_achievement_striving (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
 
 create table big_5_conscientiousness_self_efficacy (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
 
 create table big_5_conscientiousness_self_discipline (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
 create table big_5_conscientiousness_orderliness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  index(id) 
 );
  
  create table big_5_conscientiousness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  fk_self_efficacy_id int not null,
  fk_self_discipline_id int not null,
  fk_orderliness_id int not null,
  fk_dutifulness_id int not null,
  fk_cautiousness_id int not null,
  fk_achievement_striving_id int not null,
  
  foreign key (fk_self_efficacy_id)
	references big_5_conscientiousness_self_efficacy(id)
	on delete cascade,
  
  foreign key (fk_self_discipline_id)
	references big_5_conscientiousness_self_discipline(id)
	on delete cascade,
  
  foreign key (fk_orderliness_id)
	references big_5_conscientiousness_orderliness(id)
	on delete cascade,
  
  foreign key (fk_dutifulness_id)
	references big_5_conscientiousness_dutifulness(id)
    on delete cascade,
    
  foreign key (fk_cautiousness_id)
	references big_5_conscientiousness_cautiousness(id)
    on delete cascade,
    
  foreign key (fk_achievement_striving_id)
	references big_5_conscientiousness_achievement_striving(id)
    on delete cascade,
 
  index(id) 
 );
 
/***
 *    .______    _____         ___       _______ .______       _______  _______     ___      .______    __       _______ .__   __.  _______     _______.     _______.
 *    |   _  \  | ____|       /   \     /  _____||   _  \     |   ____||   ____|   /   \     |   _  \  |  |     |   ____||  \ |  | |   ____|   /       |    /       |
 *    |  |_)  | | |__        /  ^  \   |  |  __  |  |_)  |    |  |__   |  |__     /  ^  \    |  |_)  | |  |     |  |__   |   \|  | |  |__     |   (----`   |   (----`
 *    |   _  <  |___ \      /  /_\  \  |  | |_ | |      /     |   __|  |   __|   /  /_\  \   |   _  <  |  |     |   __|  |  . `  | |   __|     \   \        \   \    
 *    |  |_)  |  ___) |    /  _____  \ |  |__| | |  |\  \----.|  |____ |  |____ /  _____  \  |  |_)  | |  `----.|  |____ |  |\   | |  |____.----)   |   .----)   |   
 *    |______/  |____/    /__/     \__\ \______| | _| `._____||_______||_______/__/     \__\ |______/  |_______||_______||__| \__| |_______|_______/    |_______/    
 *                                                                                                                                                                   
 */
 
 create table big_5_agreeableness_cooperation (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness_morality (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness_trust (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness_altruism (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness_modesty (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness_sympathy (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  
  index(id)
 );
 
 create table big_5_agreeableness (
  id int not null auto_increment primary key,
  percentile decimal(17,16) null,
  raw_score decimal(17,16) null,
  fk_altruism_id int not null,
  fk_cooperation_id int not null,
  fk_modesty_id int not null,
  fk_morality_id int not null,
  fk_sympathy_id int not null,
  fk_trust_id int not null,
  
  foreign key (fk_altruism_id)
	references big_5_agreeableness_altruism(id)
    on delete cascade,
    
  foreign key (fk_cooperation_id)
	references big_5_agreeableness_cooperation(id)
    on delete cascade,
    
  foreign key (fk_modesty_id)
	references big_5_agreeableness_modesty(id)
    on delete cascade,

  foreign key (fk_morality_id)
	references big_5_agreeableness_morality(id)
    on delete cascade,
    
  foreign key (fk_sympathy_id)
	references big_5_agreeableness_sympathy(id)
    on delete cascade,
    
  foreign key (fk_trust_id)
	references big_5_agreeableness_trust(id)
    on delete cascade,

  index(id)
 );
 
 /***
 *    .______    __    _______     _____  
 *    |   _  \  |  |  /  _____|   | ____| 
 *    |  |_)  | |  | |  |  __     | |__   
 *    |   _  <  |  | |  | |_ |    |___ \  
 *    |  |_)  | |  | |  |__| |     ___) | 
 *    |______/  |__|  \______|    |____/  
 *                                        
 */
 
 create table big_five (
 id int not null auto_increment primary key,
 fk_big_5_openness_id int not null,
 fk_big_5_extraversion_id int not null,
 fk_big_5_emotional_range_id int not null,
 fk_big_5_conscientiousness_id int not null,
 fk_big_5_agreeableness_id int not null,
 
 foreign key (fk_big_5_openness_id) 
	references big_5_openness(id)
    on delete cascade,
    
  foreign key (fk_big_5_extraversion_id)
	references big_5_extraversion(id)
    on delete cascade,
 
  foreign key (fk_big_5_emotional_range_id)
	references big_5_emotional_range(id)
    on delete cascade,

  foreign key (fk_big_5_conscientiousness_id)
	references big_5_conscientiousness(id)
    on delete cascade,

  foreign key (fk_big_5_agreeableness_id)
	references big_5_agreeableness(id)
    on delete cascade,
    
 index(id)
 );
 
 /***
 *    ____    __    ____  ___   .___________.    _______.  ______   .__   __.   .______       _______     _______. __    __   __      .___________.    _______.
 *    \   \  /  \  /   / /   \  |           |   /       | /  __  \  |  \ |  |   |   _  \     |   ____|   /       ||  |  |  | |  |     |           |   /       |
 *     \   \/    \/   / /  ^  \ `---|  |----`  |   (----`|  |  |  | |   \|  |   |  |_)  |    |  |__     |   (----`|  |  |  | |  |     `---|  |----`  |   (----`
 *      \            / /  /_\  \    |  |        \   \    |  |  |  | |  . `  |   |      /     |   __|     \   \    |  |  |  | |  |         |  |        \   \    
 *       \    /\    / /  _____  \   |  |    .----)   |   |  `--'  | |  |\   |   |  |\  \----.|  |____.----)   |   |  `--'  | |  `----.    |  |    .----)   |   
 *        \__/  \__/ /__/     \__\  |__|    |_______/     \______/  |__| \__|   | _| `._____||_______|_______/     \______/  |_______|    |__|    |_______/    
 *                                                                                                                                                             
 */
 
 create table watson_results (
  id int not null auto_increment primary key,
  fk_big_five_id int not null,
  fk_needs_id int not null,
  fk_values_id int not null,
  
 foreign key (fk_big_five_id)
	references big_five(id)
    on delete cascade,

 foreign key (fk_needs_id)
	references watson_needs(id)
    on delete cascade,
 
 foreign key (fk_values_id)
	references watson_values(id)
    on delete cascade,
    
 index(id)
 );
 
 /***
 *    ____    __    ____  _______  __  ___      ___        .______       _______     _______. __    __   __      .___________.    _______.
 *    \   \  /  \  /   / |   ____||  |/  /     /   \       |   _  \     |   ____|   /       ||  |  |  | |  |     |           |   /       |
 *     \   \/    \/   /  |  |__   |  '  /     /  ^  \      |  |_)  |    |  |__     |   (----`|  |  |  | |  |     `---|  |----`  |   (----`
 *      \            /   |   __|  |    <     /  /_\  \     |      /     |   __|     \   \    |  |  |  | |  |         |  |        \   \    
 *       \    /\    /    |  |____ |  .  \   /  _____  \    |  |\  \----.|  |____.----)   |   |  `--'  | |  `----.    |  |    .----)   |   
 *        \__/  \__/     |_______||__|\__\ /__/     \__\   | _| `._____||_______|_______/     \______/  |_______|    |__|    |_______/    
 *                                                                                                                                        
 */
 
 create table weka_results(
  id int not null auto_increment primary key,
  tree text null,
  
  index(id)
 );
 
 /***
 *    .___________. __________   ___ .___________.    _______.
 *    |           ||   ____\  \ /  / |           |   /       |
 *    `---|  |----`|  |__   \  V  /  `---|  |----`  |   (----`
 *        |  |     |   __|   >   <       |  |        \   \    
 *        |  |     |  |____ /  .  \      |  |    .----)   |   
 *        |__|     |_______/__/ \__\     |__|    |_______/    
 *                                                            
 */
 
 create table texts (
  id int not null auto_increment primary key,
  fk_watson_results_id int not null,
  fk_weka_results_id int not null,
  tree text null,
  
  foreign key (fk_watson_results_id)
	references watson_results(id)
    on delete cascade,
    
  foreign key (fk_weka_results_id)
	references weka_results(id)
    on delete cascade
 );
 
 /***
 *      ______  __       __   _______ .__   __. .___________.
 *     /      ||  |     |  | |   ____||  \ |  | |           |
 *    |  ,----'|  |     |  | |  |__   |   \|  | `---|  |----`
 *    |  |     |  |     |  | |   __|  |  . `  |     |  |     
 *    |  `----.|  `----.|  | |  |____ |  |\   |     |  |     
 *     \______||_______||__| |_______||__| \__|     |__|     
 *                                                           
 */
 
 create table app_client (
  id int not null auto_increment primary key,
  fn nvarchar(35) null,
  ln nvarchar(35) null,
  active bit default 0,
  
  index(id)
 );
 
/***
 *      ______  __       __   _______ .__   __. .___________.   .___________. __________   ___ .___________.    _______.
 *     /      ||  |     |  | |   ____||  \ |  | |           |   |           ||   ____\  \ /  / |           |   /       |
 *    |  ,----'|  |     |  | |  |__   |   \|  | `---|  |----`   `---|  |----`|  |__   \  V  /  `---|  |----`  |   (----`
 *    |  |     |  |     |  | |   __|  |  . `  |     |  |            |  |     |   __|   >   <       |  |        \   \    
 *    |  `----.|  `----.|  | |  |____ |  |\   |     |  |            |  |     |  |____ /  .  \      |  |    .----)   |   
 *     \______||_______||__| |_______||__| \__|     |__|            |__|     |_______/__/ \__\     |__|    |_______/    
 *                                                                                                                      
 */
 
 create table client_texts (
  id int not null auto_increment primary key,
  fk_app_client_id int not null,
  fk_watson_results_id int unique,
  fk_weka_results_id int,
  client_text text null,
  
  foreign key (fk_app_client_id)
	references app_client(id)
    on delete cascade,
    
  foreign key (fk_watson_results_id)
	references watson_results(id)
    on delete cascade,
    
  foreign key (fk_weka_results_id)
	references weka_results(id)
    on delete cascade,
  index(id)
 );