/***
 *         _______..______   
 *        /       ||   _  \  
 *       |   (----`|  |_)  | 
 *        \   \    |   ___/  
 *    .----)   |   |  |      
 *    |_______/    | _|      
 *                           
 */
 
 use bco_6008_tests;
 
 drop function if exists f_ins_wv_self_transcendence;
 drop function if exists f_ins_wv_hedonism;
 drop function if exists f_ins_wv_open_to_change;
 drop function if exists f_ins_wv_conservation;
 drop function if exists f_ins_wv_self_enhancement;
 drop function if exists f_ins_watson_values;
 
 
 drop function if exists f_ins_wn_excitement;
 drop function if exists f_ins_wn_harmony;
 drop function if exists f_ins_wn_curiosity;
 drop function if exists f_ins_wn_liberty;
 drop function if exists f_ins_wn_ideal;
 drop function if exists f_ins_wn_closeness;
 drop function if exists f_ins_wn_self_expression;
 drop function if exists f_ins_wn_love;
 drop function if exists f_ins_wn_practicality;
 drop function if exists f_ins_wn_stability;
 drop function if exists f_ins_wn_challenge;
 drop function if exists f_ins_wn_structure;
 drop function if exists f_ins_watson_needs;
 
 
 drop function if exists f_ins_b5_openness_adventurousness;
 drop function if exists f_ins_b5_openness_artistic_interests;
 drop function if exists f_ins_b5_openness_emotionality;
 drop function if exists f_ins_b5_openness_imagination;
 drop function if exists f_ins_b5_openness_intellect;
 drop function if exists f_ins_b5_openness_liberalism;
 drop function if exists f_ins_b5_openness;
 
 
 drop function if exists f_ins_b5_extraversion_activity_level;
 drop function if exists f_ins_b5_extraversion_assertiveness;
 drop function if exists f_ins_b5_extraversion_cheerfulness;
 drop function if exists f_ins_b5_extraversion_excitement_seeking;
 drop function if exists f_ins_b5_extraversion_friendliness;
 drop function if exists f_ins_b5_extraversion_gregariousness;
 drop function if exists f_ins_b5_extraversion;
 
 
 drop function if exists f_ins_b5_emotional_range;
 drop function if exists f_ins_b5_emotional_range_anger;
 drop function if exists f_ins_b5_emotional_range_anxiety;
 drop function if exists f_ins_b5_emotional_range_depression;
 drop function if exists f_ins_b5_emotional_range_self_consciousness;
 drop function if exists f_ins_b5_emotional_range_vulnerability;
 drop function if exists f_ins_b5_emotional_range_immoderation;
 
 
 drop function if exists f_ins_b5_conscientiousness_dutifulness;
 drop function if exists f_ins_b5_conscientiousness_cautiousness;
 drop function if exists f_ins_b5_conscientiousness_achievement_striving;
 drop function if exists f_ins_b5_conscientiousness_self_efficacy;
 drop function if exists f_ins_b5_conscientiousness_self_discipline;
 drop function if exists f_ins_b5_conscientiousness_orderliness ;
 drop function if exists f_ins_b5_conscientiousness;
 
 
 drop function if exists f_ins_b5_agreeableness_cooperation;
 drop function if exists f_ins_b5_agreeableness_morality;
 drop function if exists f_ins_b5_agreeableness_trust;
 drop function if exists f_ins_b5_agreeableness_altruism;
 drop function if exists f_ins_b5_agreeableness_modesty;
 drop function if exists f_ins_b5_agreeableness_sympathy;
 drop function if exists f_ins_b5_agreeableness;
 
 
 drop function if exists f_ins_big_five;
 
 
 drop function if exists f_ins_watson_results;
 
 drop function if exists f_ins_client_texts;
 
 delimiter // 
 
 /***
 *    ____    ____  ___       __       __    __   _______     _______.   .______   .______       _______ .______   
 *    \   \  /   / /   \     |  |     |  |  |  | |   ____|   /       |   |   _  \  |   _  \     |   ____||   _  \  
 *     \   \/   / /  ^  \    |  |     |  |  |  | |  |__     |   (----`   |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *      \      / /  /_\  \   |  |     |  |  |  | |   __|     \   \       |   ___/  |      /     |   __|  |   ___/  
 *       \    / /  _____  \  |  `----.|  `--'  | |  |____.----)   |      |  |      |  |\  \----.|  |____ |  |      
 *        \__/ /__/     \__\ |_______| \______/  |_______|_______/       | _|      | _| `._____||_______|| _|      
 *                                                                                                                 
 */
 
 create definer = current_user function f_ins_wv_self_transcendence(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin 
		Insert into watson_values_self_transcendence(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id() ;
	End//
 
 create definer = current_user function f_ins_wv_hedonism(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	begin 
		insert into watson_values_hedonism(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
 
 create definer = current_user function f_ins_wv_open_to_change(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	begin 
		insert into watson_values_open_to_change(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
 
 create definer = current_user function f_ins_wv_conservation(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_values_conservation(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
    
 create definer = current_user function f_ins_wv_self_enhancement(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_values_self_enhacement(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
    
/***
 *    ____    ____  ___       __       __    __   _______     _______.
 *    \   \  /   / /   \     |  |     |  |  |  | |   ____|   /       |
 *     \   \/   / /  ^  \    |  |     |  |  |  | |  |__     |   (----`
 *      \      / /  /_\  \   |  |     |  |  |  | |   __|     \   \    
 *       \    / /  _____  \  |  `----.|  `--'  | |  |____.----)   |   
 *        \__/ /__/     \__\ |_______| \______/  |_______|_______/    
 *                                                                    
 */
 
 create definer = current_user function f_ins_watson_values (st_percentile decimal(17,16), st_raw_score decimal(17,16), h_percentile decimal(17,16), 
	h_raw_score decimal(17,16), otc_percentile decimal(17,16), otc_raw_score decimal(17,16), c_percentile decimal(17,16), c_raw_score decimal(17,16),
    se_percentile decimal(17,16), se_raw_score decimal(17,16)) returns int
    
    Begin
		Declare st_id, h_id, otc_id, c_id, se_id int;
        
        /*Getting user variables. */
        select f_ins_wv_self_transcendence(st_percentile, st_raw_score) into st_id;
        select f_ins_wv_hedonism(h_percentile, h_raw_score) into h_id;
        select f_ins_wv_open_to_change(otc_percentile, otc_percentile) into otc_id;
        select f_ins_wv_conservation(c_percentile, c_raw_score) into c_id;
        select f_ins_wv_self_enhancement(se_percentile, se_raw_score) into se_id;
        
        insert into watson_values(fk_self_transcendence_id, fk_conservation_id, fk_hedonism_id, fk_self_enhancement_id, fk_open_to_change_id)
        values(st_id, c_id, h_id, se_id, otc_id);
        
        return last_insert_id();
    End//
    
/***
 *    .__   __.  _______  _______  _______       _______.   .______   .______       _______ .______   
 *    |  \ |  | |   ____||   ____||       \     /       |   |   _  \  |   _  \     |   ____||   _  \  
 *    |   \|  | |  |__   |  |__   |  .--.  |   |   (----`   |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *    |  . `  | |   __|  |   __|  |  |  |  |    \   \       |   ___/  |      /     |   __|  |   ___/  
 *    |  |\   | |  |____ |  |____ |  '--'  |.----)   |      |  |      |  |\  \----.|  |____ |  |      
 *    |__| \__| |_______||_______||_______/ |_______/       | _|      | _| `._____||_______|| _|      
 *                                                                                                    
 */
    
 create definer = current_user function f_ins_wn_excitement(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_excitement(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//   
    
create definer = current_user function f_ins_wn_harmony(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_harmony(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
    
create definer = current_user function f_ins_wn_curiosity(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_curiosity(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_liberty(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_liberty(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_ideal(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_ideal(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_closeness(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_closeness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_self_expression(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_self_expression(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_love(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_love(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//  
    
create definer = current_user function f_ins_wn_practicality(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_practicality(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end// 
    
create definer = current_user function f_ins_wn_stability(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_stability(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
    
create definer = current_user function f_ins_wn_challenge(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_challenge(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
    
 create definer = current_user function f_ins_wn_structure(percentile decimal(17,16),raw_score decimal(17,16)) returns int
	begin 
		insert into watson_needs_structure(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
	end//
    
/***
 *    .__   __.  _______  _______  _______       _______.
 *    |  \ |  | |   ____||   ____||       \     /       |
 *    |   \|  | |  |__   |  |__   |  .--.  |   |   (----`
 *    |  . `  | |   __|  |   __|  |  |  |  |    \   \    
 *    |  |\   | |  |____ |  |____ |  '--'  |.----)   |   
 *    |__| \__| |_______||_______||_______/ |_______/    
 *                                                       
 */
        
create definer = current_user function f_ins_watson_needs (e_percentile decimal(17,16), e_raw_score decimal(17,16), h_percentile decimal(17,16), 
	h_raw_score decimal(17,16), c_percentile decimal(17,16), c_raw_score decimal(17,16), l_percentile decimal(17,16), l_raw_score decimal(17,16),
    i_percentile decimal(17,16), i_raw_score decimal(17,16), cl_percentile decimal(17,16), cl_raw_score decimal(17,16), se_percentile decimal(17,16),
    se_raw_score decimal(17,16), lv_percentile decimal(17,16), lv_raw_score decimal(17,16), prc_percentile decimal(17,16), prc_raw_score decimal(17,16),
    sblt_percentile decimal(17,16), sblt_raw_score decimal(17,16), chlg_percentile decimal(17,16), chlg_raw_score decimal(17,16), 
    stcr_percentile decimal(17,16), stcr_raw_score decimal(17,16)) returns int
    
    Begin
		Declare e_id, h_id, c_id, l_id, i_id, cl_id, se_id, lv_id, prc_id, sblt_id, chlg_id, stcr_id int;
        
        /*Getting user variables. */
        select f_ins_wn_excitement(e_percentile, e_raw_score) into e_id;
        select f_ins_wn_harmony(h_percentile, h_raw_score) into h_id;
        select f_ins_wn_curiosity(c_percentile, c_raw_score) into c_id;
        select f_ins_wn_liberty(l_percentile, l_raw_score) into l_id;
        select f_ins_wn_ideal(i_percentile, i_raw_score) into i_id;
        select f_ins_wn_closeness(cl_percentile, cl_raw_score) into cl_id;
        select f_ins_wn_self_expression(se_percentile, se_raw_score) into se_id;
        select f_ins_wn_love(lv_percentile, lv_raw_score) into lv_id;
        select f_ins_wn_practicality(prc_percentile, prc_raw_score) into prc_id;
        select f_ins_wn_stability(sblt_percentile, sblt_raw_score) into sblt_id;
        select f_ins_wn_challenge(chlg_percentile, chlg_raw_score) into chlg_id;
        select f_ins_wn_structure(stcr_percentile, stcr_raw_score) into stcr_id;
        
        insert into watson_needs(fk_excitement_id, fk_harmony_id, fk_curiosity_id, fk_liberty_id, fk_ideal_id, fk_closeness_id, fk_self_expression_id, fk_love_id,
			fk_practicality_id, fk_stability_id, fk_challenge_id , fk_structure_id)
        values (e_id, h_id, c_id, l_id, i_id, cl_id, se_id, lv_id, prc_id, sblt_id, chlg_id, stcr_id);
        
        return last_insert_id();
    End//
    
/***
 *    .______    _____      ______   .______    _______ .__   __. .__   __.  _______     _______.     _______.   .______   .______       _______ .______   
 *    |   _  \  | ____|    /  __  \  |   _  \  |   ____||  \ |  | |  \ |  | |   ____|   /       |    /       |   |   _  \  |   _  \     |   ____||   _  \  
 *    |  |_)  | | |__     |  |  |  | |  |_)  | |  |__   |   \|  | |   \|  | |  |__     |   (----`   |   (----`   |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *    |   _  <  |___ \    |  |  |  | |   ___/  |   __|  |  . `  | |  . `  | |   __|     \   \        \   \       |   ___/  |      /     |   __|  |   ___/  
 *    |  |_)  |  ___) |   |  `--'  | |  |      |  |____ |  |\   | |  |\   | |  |____.----)   |   .----)   |      |  |      |  |\  \----.|  |____ |  |      
 *    |______/  |____/     \______/  | _|      |_______||__| \__| |__| \__| |_______|_______/    |_______/       | _|      | _| `._____||_______|| _|      
 *                                                                                                                                                         
 */    
    
 create definer =  current_user function f_ins_b5_openness_adventurousness (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_adventurousness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer =  current_user function f_ins_b5_openness_artistic_interests (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_artistic_interests(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer =  current_user function f_ins_b5_openness_emotionality (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_emotionality(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer =  current_user function f_ins_b5_openness_imagination (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_imagination(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//

 create definer =  current_user function f_ins_b5_openness_intellect (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_intellect(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer =  current_user function f_ins_b5_openness_liberalism (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_openness_liberalism(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
/***
 *    .______    _____      ______   .______    _______ .__   __. .__   __.  _______     _______.     _______.
 *    |   _  \  | ____|    /  __  \  |   _  \  |   ____||  \ |  | |  \ |  | |   ____|   /       |    /       |
 *    |  |_)  | | |__     |  |  |  | |  |_)  | |  |__   |   \|  | |   \|  | |  |__     |   (----`   |   (----`
 *    |   _  <  |___ \    |  |  |  | |   ___/  |   __|  |  . `  | |  . `  | |   __|     \   \        \   \    
 *    |  |_)  |  ___) |   |  `--'  | |  |      |  |____ |  |\   | |  |\   | |  |____.----)   |   .----)   |   
 *    |______/  |____/     \______/  | _|      |_______||__| \__| |__| \__| |_______|_______/    |_______/    
 *                                                                                                            
 */
    
create definer = current_user function f_ins_b5_openness(o_percentile decimal(17,16), o_raw_score decimal(17,16), a_percentile decimal(17,16), a_raw_score decimal(17,16), ai_percentile decimal(17,16), ai_raw_score decimal(17,16),
 e_percentile decimal(17,16), e_raw_score decimal(17,16), im_percentile decimal(17,16), im_raw_score decimal(17,16), in_percentile decimal(17,16), in_raw_score decimal(17,16),
 l_percentile decimal(17,16), l_raw_score decimal(17,16)) returns int
	begin
		declare a_id, ai_id, e_id, im_id, in_id, l_id int;
        
        select f_ins_b5_openness_adventurousness(a_percentile, a_raw_score) into a_id;
		select f_ins_b5_openness_artistic_interests(ai_percentile, ai_raw_score) into ai_id;
		select f_ins_b5_openness_emotionality(e_percentile, e_raw_score) into e_id;
        select f_ins_b5_openness_imagination(im_percentile, im_raw_score) into im_id;
        select f_ins_b5_openness_intellect(in_percentile, in_raw_score) into in_id;
        select f_ins_b5_openness_liberalism(l_percentile, l_raw_score) into l_id;
        
        insert into big_5_openness(percentile, raw_score,fk_adventurousness_id, fk_artistic_interests_id, fk_emotionality_id, fk_imagination_id, fk_intellect_id, fk_liberalism_id)
        values (o_percentile, o_raw_score, a_id, ai_id, e_id, im_id, in_id, l_id);
        
        return last_insert_id();
    end//
    
/***
 *    .______    _____   __________   ___ .___________..______         .______   .______       _______ .______   
 *    |   _  \  | ____| |   ____\  \ /  / |           ||   _  \        |   _  \  |   _  \     |   ____||   _  \  
 *    |  |_)  | | |__   |  |__   \  V  /  `---|  |----`|  |_)  |       |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *    |   _  <  |___ \  |   __|   >   <       |  |     |      /        |   ___/  |      /     |   __|  |   ___/  
 *    |  |_)  |  ___) | |  |____ /  .  \      |  |     |  |\  \----.   |  |      |  |\  \----.|  |____ |  |      
 *    |______/  |____/  |_______/__/ \__\     |__|     | _| `._____|   | _|      | _| `._____||_______|| _|      
 *                                                                                                               
 */
    
 create definer = current_user function f_ins_b5_extraversion_activity_level (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_activity_level(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_extraversion_assertiveness (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_assertiveness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_extraversion_cheerfulness (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_cheerfulness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_extraversion_excitement_seeking (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_excitement_seeking(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_extraversion_friendliness (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_friendliness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
 
 create definer = current_user function f_ins_b5_extraversion_gregariousness (percentile decimal(17,16),raw_score decimal(17,16)) returns int
	Begin      
        Insert Into big_5_extraversion_gregariousness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_extraversion (percentile decimal(17,16), raw_score decimal(17,16), al_percentile decimal(17,16), al_raw_score decimal(17,16),
	a_percentile decimal(17,16), a_raw_score decimal(17,16), c_percentile decimal(17,16), c_raw_score decimal(17,16), es_percentile decimal(17,16), es_raw_score decimal(17,16),
	f_percentile decimal(17,16), f_raw_score decimal(17,16), g_percentile decimal(17,16), g_raw_score decimal(17,16)) returns int
		Begin
			Declare al_id, a_id, c_id, es_id, f_id, g_id int;
            
            select f_ins_b5_extraversion_activity_level(al_percentile, al_raw_score) into al_id;
            select f_ins_b5_extraversion_assertiveness(a_percentile, a_raw_score) into a_id;
            select f_ins_b5_extraversion_cheerfulness(c_percentile, c_raw_score) into c_id;
            select f_ins_b5_extraversion_excitement_seeking(es_percentile, es_raw_score) into es_id;
            select f_ins_b5_extraversion_friendliness(f_percentile, f_raw_score) into f_id;
            select f_ins_b5_extraversion_gregariousness(g_percentile, g_raw_score) into g_id;
            
            Insert Into big_5_extraversion(percentile, raw_score, fk_activity_level_id, fk_assertiveness_id, fk_cheerfulness_id, fk_excitement_seeking_id, fk_friendliness_id, fk_gregariousness_id)
            Values (percentile, raw_score, al_id, a_id, c_id, es_id, f_id, g_id);
        
			Return last_insert_id();
        End//
 
 /***
 *    .______    _____   _______ .______         .______   .______       _______ .______   
 *    |   _  \  | ____| |   ____||   _  \        |   _  \  |   _  \     |   ____||   _  \  
 *    |  |_)  | | |__   |  |__   |  |_)  |       |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *    |   _  <  |___ \  |   __|  |      /        |   ___/  |      /     |   __|  |   ___/  
 *    |  |_)  |  ___) | |  |____ |  |\  \----.   |  |      |  |\  \----.|  |____ |  |      
 *    |______/  |____/  |_______|| _| `._____|   | _|      | _| `._____||_______|| _|      
 *                                                                                         
 */
 
 create definer = current_user function f_ins_b5_emotional_range_anger(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_anger(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
 
 create definer = current_user function f_ins_b5_emotional_range_anxiety(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_anxiety(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_emotional_range_depression(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_depression(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_emotional_range_self_consciousness(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_self_consciousness(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
    
 create definer = current_user function f_ins_b5_emotional_range_vulnerability(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_vulnerability(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//

 create definer = current_user function f_ins_b5_emotional_range_immoderation(percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_emotional_range_immoderation(percentile, raw_score)
        values (percentile, raw_score);
        
        return last_insert_id();
    End//
 
 /***
 *    .______    _____     _______ .______          ___      .__   __.   _______  _______    
 *    |   _  \  | ____|   |   ____||   _  \        /   \     |  \ |  |  /  _____||   ____|   
 *    |  |_)  | | |__     |  |__   |  |_)  |      /  ^  \    |   \|  | |  |  __  |  |__      
 *    |   _  <  |___ \    |   __|  |      /      /  /_\  \   |  . `  | |  | |_ | |   __|     
 *    |  |_)  |  ___) |   |  |____ |  |\  \----./  _____  \  |  |\   | |  |__| | |  |____    
 *    |______/  |____/    |_______|| _| `._____/__/     \__\ |__| \__|  \______| |_______|   
 *                                                                                           
 */
 
 create definer = current_user function f_ins_b5_emotional_range (percentile decimal(17,16), raw_score decimal(17,16), ag_percentile decimal(17,16), ag_raw_score decimal(17,16),
	ax_percentile decimal(17,16), ax_raw_score decimal(17,16), d_percentile decimal(17,16), d_raw_score decimal(17,16), sc_percentile decimal(17,16), sc_raw_score decimal(17,16),
    v_percentile decimal(17,16), v_raw_score decimal(17,16), i_percentile decimal(17,16), i_raw_score decimal(17,16)) returns int
	Begin
		Declare ag_id, ax_id, d_id, sc_id,v_id, i_id Int;
		
        Select f_ins_b5_emotional_range_anger(ag_percentile, ag_raw_score) into ag_id;
        Select f_ins_b5_emotional_range_anxiety(ax_percentile, ax_raw_score) into ax_id;
        Select f_ins_b5_emotional_range_depression(d_percentile, d_raw_score) into d_id;
        Select f_ins_b5_emotional_range_self_consciousness(sc_percentile, sc_raw_score) into sc_id;
        Select f_ins_b5_emotional_range_vulnerability(v_percentile, v_raw_score) into v_id;
        Select f_ins_b5_emotional_range_immoderation(i_percentile, i_raw_score) into i_id;
        
        Insert Into big_5_emotional_range(percentile, raw_score, fk_anger_id, fk_anxiety_id, fk_depression_id, fk_self_consciousness_id, fk_vulnerability_id, fk_immoderation_id)
        Values (percentile, raw_score, ag_id, ax_id, d_id, sc_id, v_id, i_id); 
        
        Return last_insert_id();
    
    End//
 
 /***
 *    .______    _____    ______   ______   .__   __.   .______   .______       _______ .______     
 *    |   _  \  | ____|  /      | /  __  \  |  \ |  |   |   _  \  |   _  \     |   ____||   _  \    
 *    |  |_)  | | |__   |  ,----'|  |  |  | |   \|  |   |  |_)  | |  |_)  |    |  |__   |  |_)  |   
 *    |   _  <  |___ \  |  |     |  |  |  | |  . `  |   |   ___/  |      /     |   __|  |   ___/    
 *    |  |_)  |  ___) | |  `----.|  `--'  | |  |\   |   |  |      |  |\  \----.|  |____ |  |        
 *    |______/  |____/   \______| \______/  |__| \__|   | _|      | _| `._____||_______|| _|        
 *                                                                                                  
 */
 
 create definer = current_user function f_ins_b5_conscientiousness_dutifulness (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_dutifulness(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
    End//
 
 create definer = current_user function f_ins_b5_conscientiousness_cautiousness (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_cautiousness(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
    End// 
 
 create definer = current_user function f_ins_b5_conscientiousness_achievement_striving (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_achievement_striving(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
    End//
 
 create definer = current_user function f_ins_b5_conscientiousness_self_efficacy (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_self_efficacy(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
	End//
 
 create definer = current_user function f_ins_b5_conscientiousness_self_discipline (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_self_discipline(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
	End//
    
 create definer = current_user function f_ins_b5_conscientiousness_orderliness (percentile decimal(17,16), raw_score decimal(17,16)) returns int
	Begin
		Insert into big_5_conscientiousness_orderliness(percentile, raw_score)
        Values (percentile, raw_score);
        
        Return last_insert_id();
	End//
 
 /***
 *    .______    _____    ______   ______   .__   __. 
 *    |   _  \  | ____|  /      | /  __  \  |  \ |  | 
 *    |  |_)  | | |__   |  ,----'|  |  |  | |   \|  | 
 *    |   _  <  |___ \  |  |     |  |  |  | |  . `  | 
 *    |  |_)  |  ___) | |  `----.|  `--'  | |  |\   | 
 *    |______/  |____/   \______| \______/  |__| \__| 
 *                                                    
 */
 
 create definer = current_user function f_ins_b5_conscientiousness (percentile decimal(17,16), raw_score decimal(17,16), d_percentile decimal(17,16), d_raw_score decimal(17,16),
	c_percentile decimal(17,16), c_raw_score decimal(17,16), as_percentile decimal(17,16), as_raw_score decimal(17,16), se_percentile decimal(17,16), se_raw_score decimal(17,16),
    sd_percentile decimal(17,16), sd_raw_score decimal(17,16), o_percentile decimal(17,16), o_raw_score decimal(17,16)) returns int
    Begin
		Declare d_id, c_id, as_id, se_id, sd_id, o_id int;
        
        Select f_ins_b5_conscientiousness_dutifulness(d_percentile, d_raw_score) into d_id;
        Select f_ins_b5_conscientiousness_cautiousness(c_percentile, c_raw_score) into c_id;
        Select f_ins_b5_conscientiousness_achievement_striving(as_percentile, as_raw_score) into as_id;
        Select f_ins_b5_conscientiousness_self_efficacy(se_percentile, se_raw_score) into se_id;
        Select f_ins_b5_conscientiousness_self_discipline(sd_percentile, sd_raw_score) into sd_id;
        Select f_ins_b5_conscientiousness_orderliness(o_percentile, o_raw_score) into o_id;
        
        Insert Into big_5_conscientiousness(percentile, raw_score, fk_self_efficacy_id, fk_self_discipline_id, fk_orderliness_id, fk_dutifulness_id, fk_cautiousness_id, fk_achievement_striving_id)
        Values (percentile, raw_score, se_id, sd_id, o_id, d_id, c_id, as_id);
        
        Return last_insert_id();
    End//
 
/***
 *    .______    _____       ___       _______ .______         .______   .______       _______ .______   
 *    |   _  \  | ____|     /   \     /  _____||   _  \        |   _  \  |   _  \     |   ____||   _  \  
 *    |  |_)  | | |__      /  ^  \   |  |  __  |  |_)  |       |  |_)  | |  |_)  |    |  |__   |  |_)  | 
 *    |   _  <  |___ \    /  /_\  \  |  | |_ | |      /        |   ___/  |      /     |   __|  |   ___/  
 *    |  |_)  |  ___) |  /  _____  \ |  |__| | |  |\  \----.   |  |      |  |\  \----.|  |____ |  |      
 *    |______/  |____/  /__/     \__\ \______| | _| `._____|   | _|      | _| `._____||_______|| _|      
 *                                                                                                       
 */
 
 Create definer = current_user function f_ins_b5_agreeableness_cooperation(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
		Insert into big_5_agreeableness_cooperation(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//

 Create definer = current_user function f_ins_b5_agreeableness_morality(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
		Insert into big_5_agreeableness_morality(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//
  
 Create definer = current_user function f_ins_b5_agreeableness_trust(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
		Insert into big_5_agreeableness_trust(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//
    
 Create definer = current_user function f_ins_b5_agreeableness_altruism(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
		Insert into big_5_agreeableness_altruism(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//
    
 Create definer = current_user function f_ins_b5_agreeableness_modesty(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
	Insert into big_5_agreeableness_modesty(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//
    
 Create definer = current_user function f_ins_b5_agreeableness_sympathy(percentile decimal(17,16), raw_score decimal(17,16)) returns int
    Begin 
		Insert into big_5_agreeableness_sympathy(percentile, raw_score)
        values(percentile, raw_score);
        
        Return last_insert_id();
	End//
    
 Create definer = current_user function f_ins_b5_agreeableness(percentile decimal(17,16), raw_score decimal(17,16), a_percentile decimal(17,16), a_raw_score decimal(17,16), 
	c_percentile decimal(17,16), c_raw_score decimal(17,16), md_percentile decimal(17,16), md_raw_score decimal(17,16), mr_percentile decimal(17,16), mr_raw_score decimal(17,16),
    s_percentile decimal(17,16), s_raw_score decimal(17,16), t_percentile decimal(17,16), t_raw_score decimal(17,16)) returns int
    Begin
		Declare c_id, md_id, t_id, a_id, mr_id, s_id int;
        
        Select f_ins_b5_agreeableness_altruism(a_percentile, a_raw_score) into a_id;
        Select f_ins_b5_agreeableness_cooperation(c_percentile, c_raw_score) into c_id;
        Select f_ins_b5_agreeableness_modesty(md_percentile, md_raw_score) into md_id;
        Select f_ins_b5_agreeableness_morality(mr_percentile, mr_raw_score) into mr_id;
        Select f_ins_b5_agreeableness_sympathy(s_percentile, s_raw_score) into s_id;
        Select f_ins_b5_agreeableness_trust(t_percentile, t_raw_score) into t_id;
        
        Insert Into big_5_agreeableness(percentile, raw_score, fk_altruism_id, fk_cooperation_id, fk_modesty_id, fk_morality_id, fk_sympathy_id, fk_trust_id)
        Values (percentile, raw_score, a_id, c_id, md_id, mr_id, s_id, t_id);
        
        Return last_insert_id();    
    End//
    
 /***
 *    .______    _____  
 *    |   _  \  | ____| 
 *    |  |_)  | | |__   
 *    |   _  <  |___ \  
 *    |  |_)  |  ___) | 
 *    |______/  |____/  
 *                      
 */
 create definer = current_user function f_ins_big_five (openness_id int, agreeableness_id int, extraversion_id int, emotional_range_id int, conscientiousness_id int) returns int
    Begin
		Insert into big_five(fk_big_5_openness_id, fk_big_5_agreeableness_id, fk_big_5_extraversion_id, fk_big_5_emotional_range_id, fk_big_5_conscientiousness_id)
        values (openness_id, agreeableness_id, extraversion_id, emotional_range_id, conscientiousness_id);
        
		Return last_insert_id();
    End//
    
    
 /***
 *    ____    __    ____ .______       _______     _______. __    __   __      .___________.    _______.
 *    \   \  /  \  /   / |   _  \     |   ____|   /       ||  |  |  | |  |     |           |   /       |
 *     \   \/    \/   /  |  |_)  |    |  |__     |   (----`|  |  |  | |  |     `---|  |----`  |   (----`
 *      \            /   |      /     |   __|     \   \    |  |  |  | |  |         |  |        \   \    
 *       \    /\    /    |  |\  \----.|  |____.----)   |   |  `--'  | |  `----.    |  |    .----)   |   
 *        \__/  \__/     | _| `._____||_______|_______/     \______/  |_______|    |__|    |_______/    
 *                                                                                                      
 */
 
 create definer = current_user function f_ins_watson_results(tree longblob, big_five_id int, needs_id int, values_id int) returns int
	Begin
		Insert into watson_results(tree, fk_big_five_id, fk_needs_id, fk_values_id)
        values(tree, big_five_id, needs_id, values_id);
        
        return last_insert_id();
    
    End//
    
    
 create definer = current_user function f_ins_client_texts(client_id int, watson_results_id int, weka_results_id int, client_text longblob) returns int
	Begin
		Insert into client_texts(fk_app_client_id, fk_watson_results_id, fk_weka_results_id, client_text)
        values (client_id, watson_results_id, weka_results_id, client_text);
        
        return last_insert_id();
    End//
    
    
 Delimiter ;
 